@component('mail::message')
Hello {{$user->firstName}}

Thanks to your efforts {{$sponsoredUser['firstName']}} has used your referral link to sign up. If you would like to contact them, their email is {{$sponsoredUser['email']}}

@component('mail::button', ['url' => 'https://triotrader.com/login'])
Login Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
