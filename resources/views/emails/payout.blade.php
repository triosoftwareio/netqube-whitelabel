@component('mail::message')
@if($admin)
Hello Admins,
<br>
Latest commission run info can be found below.
@component('mail::table')
<?php $totalPaidOut = 0.00000; ?>
<table>
<tr>
<th>ID</th>
<th>Email</th>
<th>Direct</th>
<th>Team</th>
<th>Bonus</th>
<th>24h Max</th>
<th>Fee</th>
<th>Total</th>
</tr>
@foreach($payoutData as $key => $payout)
<tr>
<td>{!! $payout['user_id'] !!}</td>
<td>{!! $key !!}</td>
<td>{!! $payout['direct_commission_paid'] !!} BTC</td>
<td>{!! $payout['team_commission_paid'] !!} BTC</td>
<td>{!! $payout['bonus_amount_paid'] !!} BTC</td>
<td>{!! $payout['daily_max'] !!}</td>
<td>{!! $payout['network_fee'] !!} BTC</td>
<td>{!! $payout['final_amount_sent'] !!} BTC</td>
</tr>
<?php $totalPaidOut += $payout['team_commission_paid'] ?>
<?php $totalPaidOut += $payout['bonus_amount_paid'] ?>
<?php $totalPaidOut += $payout['direct_commission_paid'] ?>
@endforeach
</table>
@endcomponent

Total Paid Out: {{ $totalPaidOut }} BTC
@else
Hello,

You have just been paid out on your daily commissions! Please check your wallet and commission report section on the back-office.

@endif

@component('mail::button', ['url' => 'http://triotrader.com/login'])
Login Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
