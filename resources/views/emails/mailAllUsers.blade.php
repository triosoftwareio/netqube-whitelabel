@component('mail::message')

Hello {{$user->name}},
<p>
  {{ $content }}
</p>

Cheers!<br>
{{ config('app.name') }}
@endcomponent
