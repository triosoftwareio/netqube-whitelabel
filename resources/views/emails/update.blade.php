@component('mail::message')

Hello {{$user->name}},
<p>Our newest update is here! Find it attached to this email</p>

Enjoy!<br>
{{ config('app.name') }}
@endcomponent
