@component('mail::message')

Dear {{$name}},
<p>Are you ready to join the Trio Trader family?</p>

<p>Here at Trio Trader, we've invested endless hours studying, researching, observing and trading.</p>
<p>The Trio Trader family is one in which each member will learn anything and everything about cryptocurrencies to make the best, most educated decisions going forward with the goal of increasing everybody's wealth.
  We have an experienced and driven team of traders actively researching for the news, updates, anything and everything about altcoins and the teams that lead them. These traders will have live-stream feeds available in the near future so you can see what they're trading into, at what price and most importantly WHY.</p>
<p>We have reserved a position in our company specifically for you. In order to get the knowledge and portfolio growth process started, visit our site using the sponsor link below and you'll be well on your way to learning and earning!</p>

@component('mail::button', ['url' => 'http://www.triotrader.com/'])
Join Now!
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
