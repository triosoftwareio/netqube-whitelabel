@component('mail::message')
Hello {{$user['firstName']}}

Welcome to Trio Trader!

On behalf of the entire staff, thank you for your purchase of the Trio Trader Platform. This is a revolutionary, simple to use Cryptocurrency Trading Platform right at your fingertips!  We're confident that you're going to enjoy our tools as you utilize & learn our simple to use platform.

The enclosed information is designed to serve as an introduction with the getting started & first things first process. Please take a few minutes to review the information below as you will find it is very helpful in getting started the right way with your Trio Trader platform.

Ensure that you first review all of our videos and set up your API Keys before trading!

We are excited to be on this journey with you and we're here to support you each step of the way. We believe you'll love our newly enhanced customer support system found inside your back office, so please reach out to us when assistance is needed.

Our Customers are the most important part of our business, and we work tirelessly to ensure your complete satisfaction, both now and in the future. Thank you for joining our Team and we look forward to Your Success Welcome to Trio Trader!

The Trio Trader Team
"Our Passion is People!"






Getting Started & Steps to Success!



The videos in the Trio Trader Backoffice will assist you further, but here are the essential steps necessary before you can begin trading.



1.      Get a Cryptocurrency Wallet  Some of the easiest wallets to obtain you can find here:

A.      JAXX - WWW.JAXX.IO

B.    EXODUS -WWW.EXODUS.COM

C.    GEMINI WWW.GEMINI.COM

D.   BLOCKCHAIN - www.blockchain.info

Every exchange will require information in the setup process. It is very important that the pictures you submit are VERY clear and legible. Anything less could disrupt the setup process.



2. Get at least one of the Cryptocurrency Exchanges that we use.

A.      HitBTC    WWW.HITBTC.COM

B.   Bittrex.com    WWW.BITTREX.COM

C.    Binance.com   WWW.BINANCE.COM



Every exchange will require information in the setup process. It is very important that the pictures you submit are VERY clear and legible. Anything less could disrupt the setup process.



3. Get Funded in order to Trade Cryptocurrency.

A.      Not every fiat/cryptocurrency is available everywhere but convenient place to start can be found here: https://coinlist.me/bitcoin/buy/canada/canadian-bitcoin-exchanges/

B.      This process can take a few minutes but can also take several days. It’s crucial to use clear and legible pictures and addresses that match your ID. Imagine opening a new bank account. The KYC (Know Your Customer) principles are becoming increasingly more important and will actually help protect your account security.


4.      Getting a 2FA Authentication
.      Two-Factor-Authentication (2FA) is an extremely useful tool for securing your trading accounts and wallets. “Google Authenticator” can be used on Android or iPhones.


5.     Be Sure to Join our "Trio Trader Facebook Group"

                            https://www.facebook.com/groups/2223471464551317/?ref=bookmarks


Be sure to "STAY PLUGGED IN" to our Daily & Weekly Updates and Webinars! Check your Trio Trader Back Office for Meeting Times!

@component('mail::button', ['url' => 'https://www.triotrader.com/login'])
Login Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
