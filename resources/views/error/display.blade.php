@extends('layouts.newApp')


@section('content')

<div class="content-w">
  <div class="container-fluid" style="min-height:1200px;">
    <div class="content-i">
      <div class="content-box">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="element-wrapper">
              <div class="element-header" data-background-color="{{$status == 'Error' ? 'orange' : 'green'}}">
                  <h4 class="title">{{$status}}</h4>
              </div>
              <div class="element-box">
                <div class="text-center">
                  <h5>{{ $message }}</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
