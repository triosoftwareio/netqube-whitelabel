
<div class="modal" id="notificationModal">
    <div class="modal-dialog ui-block window-popup update-header-photo">

        <div class="ui-block-title">
            <h6 class="title" id="notificationModalContent"></h6>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="swipe-modal">
    <div class="modal-dialog ui-block window-popup update-header-photo">
        <div class="ui-block-title">
            <h6 id="swipe-title">Execute Trade</h6>
        </div>
        <div class="ui-block-content">
            <p id="notification-content">Are you sure you would like to execute this trade?</p>
            <button class="btn btn-success" id="executeTrade">Execute Trade</button>
            <button class="btn btn-danger" id="cancelTrade">Cancel</button>
        </div>
    </div>
</div> --}}

<div id="swipe-modal" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          Execute Trade
        </h5>
        {{-- <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button> --}}
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Pair</label>
          <input class="form-control" disabled="disabled" value="Loading" id="currentPairToTrade"></input>
        </div>
        <div class="form-group">
          <label>Target %</label>
          <input class="form-control" value="4" id="percentageAmount"></input>
        </div>
        <p style="text-align:center;">Confirmation</p>
        <p id="notification-content">Are you sure you would like to execute this trade? <span style="color:red !important;">these signals are to be used at your own risk</span>, there are no guaranteed returns on any of the signals, The prices listed for entry and exit (buy and sell) are <b>REFERENCE</b> prices</p>
        <p style="color:red !important;">If executed, the trade will execute at current market price regardless of it moving up or down from the listed entry price and repost the order at the target percentage above the buy price, again this is not investment advice nor is it going to guarantee a return</p>
        {{-- <p style="color:green !important;">Trailing stop loss is currently implemented at a -15% stop loss and trailing up at 1% intervals.</p> --}}
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col">
            <button class="btn btn-success" id="executeTrade">Execute Trade</button>
          </div>
          <div class="col">
            <button class="btn btn-danger" id="cancelTrade">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="sell-modal" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          Sell At Market
        </h5>
        {{-- <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button> --}}
      </div>
      <div class="modal-body">
        <p style="text-align:center;">Confirmation</p>
        <p id="notification-content">Are you sure you would like to sell these coins at market price? <span style="color:red !important;">these signals are to be used at your own risk</span>, there are no guaranteed returns on any of the signals, The prices listed for entry and exit (buy and sell) are <b>REFERENCE</b> prices</p>
        <p style="color:red !important;">If executed, the trade will sell at current market price</p>
        {{-- <p style="color:green !important;">Trailing stop loss is currently implemented at a -15% stop loss and trailing up at 1% intervals.</p> --}}
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col">
            <button class="btn btn-success" id="sellAtMarket">Execute Trade</button>
          </div>
          <div class="col">
            <button class="btn btn-danger" id="cancelSell">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="notification-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="notification-title"></h5>
          </div>
          <div class="modal-body">
            <p id="notification-body-text"></p>
          </div>
        </div>
    </div>
</div>
