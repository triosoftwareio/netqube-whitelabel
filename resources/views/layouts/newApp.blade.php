<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trio Trader</title>

    <meta content="World Crypto Currency, Education, CryptoCurrency, Knowledge, Invest, Marketing, Trading, Summaries, Updates, Training, Trio Trader, Auto Trader" name="keywords">
    <meta content="Trio Software Inc." name="author">
    <meta content="Your all-in-one spot for crypto education, summaries and updates" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/img/ttradericon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="/css/main.css?version=3.3" rel="stylesheet">
    <link href="/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/css-toggle-switch@latest/dist/toggle-switch.css" />
  </head>
  <body class="menu-position-side menu-side-left full-screen" style="padding:0 !important;padding-right:0 !important;">
    @if(Auth::user()->status == 'Admin' || Auth::user()->status == 'Free' || (Auth::user()->plan != 8 && Auth::user()->status == 'Active'))
        {{-- @if(Auth::user()->status == 'Active') --}}
        {{-- @if(Auth::user()->plan != 8) --}}
          <ul class="notifications">
          </ul>
        {{-- @endif --}}
        {{-- @endif --}}
    @endif
    <div class="menu-side with-side-panel">
      <div class="layout-w">
        <!--------------------
        START - Mobile Menu
        -------------------->
        @if(Auth::check())
          <div class="menu-mobile menu-activated-on-click color-scheme-dark">
            <div class="mm-logo-buttons-w">
              <a class="mm-logo" href="/"><img src="/img/ttraderlogo.png" style="max-width:100%"></img></a>
              <div class="mm-buttons">
                <div class="content-panel-open">
                  <div class="os-icon os-icon-grid-circles"></div>
                </div>
                <div class="mobile-menu-trigger">
                  <div class="os-icon os-icon-hamburger-menu-1"></div>
                </div>
              </div>
            </div>
            <div class="menu-and-user">
              <div class="logged-user-w">
                <div class="avatar-w">
                  <img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}">
                </div>
                <div class="logged-user-info-w">
                  <div class="logged-user-name">
                    {{Auth::user()->name}}
                  </div>
                  <div class="logged-user-role">
                    {{Auth::user()->status}}
                  </div>
                </div>
              </div>
              <!--------------------
              START - Mobile Menu List
              -------------------->
              <ul class="main-menu">
                <li class="">
                  <a href="/home">
                    <div class="icon-w">
                      <div class="os-icon os-icon-window-content"></div>
                    </div>
                    <span>Dashboard</span></a>
                </li>
                <li class="">
                  <a href="/binary">
                    <div class="icon-w">
                      <div class="os-icon os-icon os-icon-grid-squares"></div>
                    </div>
                    <span>Binary Tree</span></a>
                </li>
                <li>
                  <a href="/webinars">
                    <div class="icon-w">
                      <div class="os-icon os-icon-others-43"></div>
                    </div>
                    <span>Webinars</span></a>
                </li>
                @if(Auth::user()->plan != 8)
                  <li class="has-sub-menu">
                    <a href="#">
                      <div class="icon-w">
                          <div class="os-icon os-icon-robot-1"></div>
                      </div>
                      <span>Signals</span>
                    </a>
                    <ul class="sub-menu">
                      <li>
                        <a href="/signals/Bittrex">
                          Bittrex
                        </a>
                      </li>
                      <li>
                        <a href="/signals/HitBTC">
                          HitBtc
                          {{-- <strong class="badge badge-danger">New</strong> --}}
                        </a>
                      </li>
                      <li>
                        <a href="/stats/Bittrex">
                          Stats
                          <strong class="badge badge-danger">New</strong>
                        </a>
                      </li>
                    </ul>
                  </li>
                @endif
                @if(Auth::user()->plan != 8)
                  <li>
                    <a href="/settings/api-keys">
                      <div class="icon-w">
                        <div class="os-icon os-icon-fingerprint"></div>
                      </div>
                      <span>API Key Setup</span></a>
                  </li>
                @endif
                <li>
                  <a href="/courses">
                    <div class="icon-w">
                      <div class="os-icon os-icon-newspaper"></div>
                    </div>
                    <span>Courses</span></a>
                </li>
                <li class="">
                  <a href="/settings">
                    <div class="icon-w">
                      <div class="os-icon os-icon-user-male-circle"></div>
                    </div>
                    <span>Profile</span></a>
                </li>
                @if(Auth::user()->status == 'Admin')
                 <li>
                   <a href="/admin">
                     <div class="icon-w">
                       <div class="os-icon os-icon-pencil-12"></div>
                     </div>
                     <span>Admin</span></a>
                 </li>
                @endif
                <li>
                  <a href="/referrals">
                    <div class="icon-w">
                      <i class="os-icon os-icon-grid-squares-22"></i>
                    </div>
                    <span>My Referrals</span></a>
                </li>
                @if(Auth::user()->status == 'Admin')
                <li>
                  <a href="/commissions">
                    <div class="icon-w">
                      <i class="os-icon os-icon-coins-4"></i>
                    </div>
                    <span>Commissions</span></a>
                </li>
                @endif
                @if(Auth::check())
                  <li class="text-center">
                    <a href="{{ route('logout') }}">
                      <span>Logout</span>
                    </a>
                  </li>
                @endif
              </ul>
              <!--------------------
              END - Mobile Menu List
              -------------------->
            </div>
          </div>
          <!--------------------
          END - Mobile Menu
          --------------------><!--------------------
          START - Menu side
          -------------------->
          <div class="desktop-menu menu-side-w menu-activated-on-click">
            <div class="col" style="max-height:100px;">
              <a href="/"><img src="/img/ttraderlogo.png" style="max-width:100%;margin-top:15px"></a>
            </div>
            <hr>
            @if(Auth::check())
            <div class="menu-and-user">
              <div class="logged-user-w">
                <div class="logged-user-i">
                  <div class="avatar-w">
                     <img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}">
                  </div>
                  <div class="logged-user-info-w">
                    <div class="logged-user-name">
                       {{ Auth::user()->name }}
                    </div>
                    <div class="logged-user-role">
                       {{ Auth::user()->status }}
                    </div>
                  </div>
                  <div class="logged-user-menu">
                    <div class="logged-user-avatar-info">
                      <div class="avatar-w">
                         <img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}">
                      </div>
                      <div class="logged-user-info-w">
                        <div class="logged-user-name">
                           {{ Auth::user()->name }}
                        </div>
                      </div>
                    </div>
                    <div class="bg-icon">
                      <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                      <li>
                        <a href="/settings"><i class="os-icon os-icon-user-male-circle"></i><span>Profile</span></a>
                      </li>
                      @if(Auth::user()->status == 'Admin')
                       <li>
                         <a href="/admin">
                             <i class="os-icon os-icon-pencil-12"></i>
                           <span>Admin</span>
                         </a>
                       </li>
                      @endif
                      <li>
                        <a href="/referrals"><i class="os-icon os-icon-grid-squares-22"></i><span>Referrals</span></a>
                      </li>
                      @if(Auth::user()->status == 'Admin')
                      <li>
                        <a href="/commissions">
                            <i class="os-icon os-icon-coins-4"></i>
                          <span>Commissions</span></a>
                      </li>
                      @endif
                      <li>
                        <a href="{{ url('logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            @endif
              <ul class="main-menu">
                <li class="">
                  <a href="/home">
                    <div class="icon-w">
                      <div class="os-icon os-icon-window-content"></div>
                    </div>
                    <span>Dashboard</span></a>
                </li>
                <li class="">
                  <a href="/binary">
                    <div class="icon-w">
                      <div class="os-icon os-icon os-icon-grid-squares"></div>
                    </div>
                    <span>Binary Tree</span></a>
                </li>
                <li>
                  <a href="/webinars">
                    <div class="icon-w">
                      <div class="os-icon os-icon-others-43"></div>
                    </div>
                    <span>Webinars</span></a>
                </li>
                @if(Auth::user()->plan != 8)
                  <li class="has-sub-menu">
                    <a href="#">
                      <div class="icon-w">
                          <div class="os-icon os-icon-robot-1"></div>
                      </div>
                      <span>Signals</span>
                    </a>
                    <ul class="sub-menu">
                      {{-- <li>
                        <a href="/signals/poloniex">
                          Poloniex
                        </a>
                      </li> --}}
                      <li>
                        <a href="/signals/Bittrex">
                          Bittrex
                        </a>
                      </li>
                      <li>
                        <a href="/signals/HitBTC">
                          HitBtc
                        </a>
                      </li>
                      <li>
                        <a href="/stats/Bittrex">
                          Stats
                          <strong class="badge badge-danger">New</strong>
                        </a>
                      </li>
                    </ul>
                  </li>
                @endif
                @if(Auth::user()->plan != 8)
                  <li>
                    <a href="/settings/api-keys">
                      <div class="icon-w">
                        <div class="os-icon os-icon-fingerprint"></div>
                      </div>
                      <span>API Key Setup</span></a>
                  </li>
                @endif
                <li>
                  <a href="/courses">
                    <div class="icon-w">
                      <div class="os-icon os-icon-newspaper"></div>
                    </div>
                    <span>Courses</span></a>
                </li>
                <li>
                  <a href="/settings">
                    <div class="icon-w">
                      <div class="os-icon os-icon-user-male-circle"></div>
                    </div>
                    <span>Profile</span></a>
                </li>
                @if(Auth::user()->status == 'Admin')
                 <li>
                   <a href="/admin">
                     <div class="icon-w">
                       <div class="os-icon os-icon-pencil-12"></div>
                     </div>
                     <span>Admin</span></a>
                 </li>
                @endif
                 <li>
                    <button class="btn btn-primary" onclick="setClipboard('{{ Auth::user()->username }}')" style="display:block; margin: 0 auto">Copy Affiliate Link</button>
                 </li>
              </ul>
            </div>
          </div>
          <!--------------------
          END - Menu side
          -------------------->
          @endif
          @yield('content')

          </div>
        </div>
      @includeWhen(Auth::check(), 'layouts.footers')

    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://js.authorize.net/v1/Accept.js" charset="utf-8"></script>
    <script src="/bower_components/moment/moment.js"></script>
    <script src="/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/bower_components/ckeditor/ckeditor.js"></script>
    <script src="/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="/js/main.js?version=3.3"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-XXXXXXXX-9', 'auto');
      ga('send', 'pageview');
    </script>

    <script>
        jQuery(document).ready(function(){
            if ($("#donutChart").length) {
                var donutChart = $("#donutChart");

                // donut chart data
                var data = {
                    labels: ["Bitcoins Percentage Earned", "Bitcoins Percentage Remaining"],
                    datasets: [{
                        data: [{{isset($percentage) ? $percentage : 0}}, {{isset($percentage) ? 100-$percentage : 100}}],
                        backgroundColor: ["#5797fc", "#7e6fff"],
                        hoverBackgroundColor: ["#5797fc", "#7e6fff"],
                        borderWidth: 0
                    }]
                };

                // -----------------
                // init donut chart
                // -----------------
                new Chart(donutChart, {
                    type: 'doughnut',
                    data: data,
                    options: {
                        legend: {
                            display: false
                        },
                        animation: {
                            animateScale: true
                        },
                        cutoutPercentage: 80
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#paymentModal').modal('show');
        });
    </script>
    <script>
      function setClipboard(value) {
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -1000px; top: -1000px";
        tempInput.value = "https://www.triotrader.com/?sponsor=" + value;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        alert("Link Copied to Clipboard");
      }
    </script>

    {{-- Google Translate --}}
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    {{-- Start intercom --}}

    <?php
      $hash = hash_hmac(
        'sha256', // hash function
        Auth::user()->email, // user's id
        'XSh7E4S-g3l4P3NJH3eX8f4nd2Roj7vMr19iAuPC' // secret key (keep safe!)
      );
    ?>
    <script>
      window.intercomSettings = {
        app_id: "fw83lbk4",
        name: "{{Auth::user()->name}}",
        email: "{{Auth::user()->email}}",
        user_hash: "{{ $hash }}"
      };
    </script>

<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/fw83lbk4';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

  </body>
</html>
