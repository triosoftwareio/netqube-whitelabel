<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="World Crypto Currency, Education, CryptoCurrency, Knowledge, Invest, Marketing, Trading, Summary, Summaries, Information, Education, Mentorship, Bitcoin, BTC, Litecoin, LTC, Trio Trader, Signals" name="keywords">
  <meta content="Trio Software Inc." name="author">
  <meta content="Your all-in-one spot for crypto education, summaries and updates" name="description">
  <meta content="width=device-width, initial-scale=1" name="viewport">

    <title>{{ config('app.name', 'Trio Trader') }}</title>
    <!-- Styles -->

    <!-- Scripts -->
    <link href="/img/ttradericon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="/css/front/main.css" rel="stylesheet">
    <script src="/js/js-cookie/src/js.cookie.js"></script>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('#header').vide('/videos/home.mp4');

            $('#test').scrollToFixed();
            $('.res-nav_click').click(function(){
                $('.main-nav').slideToggle();
                return false

            });

        });
    </script>

    <script>
        wow = new WOW(
            {
                animateClass: 'animated',
                offset:       100
            }
        );
        wow.init();
    </script>


    <script type="text/javascript">
        $(window).load(function(){

            $('.main-nav li a, .servicelink').bind('click',function(event){
                var $anchor = $(this);

                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top - 102
                }, 1500,'easeInOutExpo');
                /*
                 if you don't want to use the easing effects:
                 $('html, body').stop().animate({
                 scrollTop: $($anchor.attr('href')).offset().top
                 }, 1000);
                 */
                if ($(window).width() < 768 ) {
                    $('.main-nav').hide();
                }
                event.preventDefault();
            });
        })
    </script>

    <script type="text/javascript">

        $(window).load(function(){


            var $container = $('.portfolioContainer'),
                $body = $('body'),
                colW = 375,
                columns = null;


            $container.isotope({
                // disable window resizing
                resizable: true,
                masonry: {
                    columnWidth: colW
                }
            });

            $(window).smartresize(function(){
                // check if columns has changed
                var currentColumns = Math.floor( ( $body.width() -30 ) / colW );
                if ( currentColumns !== columns ) {
                    // set new column count
                    columns = currentColumns;
                    // apply width to container manually, then trigger relayout
                    $container.width( columns * colW )
                        .isotope('reLayout');
                }

            }).smartresize(); // trigger resize to set container width
            $('.portfolioFilter a').click(function(){
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({

                    filter: selector,
                });
                return false;
            });

        });

    </script>
    @if(\Request::path() == 'login' || \Request::path() == 'register' || \Request::path() == 'password/reset')
        <style>
            .main-panel {
                position: relative;
                z-index: 2;
                float: none !important;
                overflow: auto;
                width: auto !important;
                min-height: 100%;
                transform: translate3d(0,0,0);
                transition: all .33s cubic-bezier(.685,.0473,.346,1);
                max-height: 100%;
                height: 100%;
            }

            .sidebar-wrapper{
                display:none !important;
            }

            .logo{
                display:none !important;
            }
        </style>
    @endif
</head>
<body>
                @yield('content')


@if(\Request::path() == 'binary')
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Place User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" class="form-control" value="" id="userId">
                        <input type="text" class="form-control" placeholder="Placement Users Username" id="placementId">
                        <select type="text" class="form-control" id="selectSide">
                            <option value="" disabled selected>Choose a side to place this user under.</option>
                            <option value="Left">Left</option>
                            <option value="Right">Right</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submitPlacement">Place User</button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.placeUser').click(function(){
                var userId = $(this).data('userid');
                $('#userId').val(userId);
            });

            $('#submitPlacement').click(function(){
                if($('#selectSide').val() != "Left" && $('#selectSide').val() != "Right"){
                    console.log($('#selectSide').val());
                    alert("Please Select A Side (Left/Right)");
                }else{
                    $.ajax({
                        url: '/user/placement',
                        data: {
                            '_token': '{{csrf_token()}}',
                            'userId': $('#userId').val(),
                            'placementId': $('#placementId').val(),
                            'side': $('#selectSide').val()
                        },
                        dataType: 'json',
                        type: 'POST',
                        success: function(response){
                            if(confirm(response)){
                                window.location.reload();
                            }
                        },
                        error: function(error){
                            console.log(error.responseText);
                        }
                    });
                }
            });
        });
    </script>
@endif
</body>
</html>
