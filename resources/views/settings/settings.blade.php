@extends('layouts.newApp')

@section('content')
  <?php
    use Carbon\Carbon;
  ?>
  @if(Auth::check())
        <div class="content-w">
          <!--------------------
          START - Breadcrumbs
          -------------------->
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <span>Home</span>
            </li>
            <li class="breadcrumb-item">
              <span>Profile</span>
            </li>
          </ul>
          <!--------------------
          END - Breadcrumbs
          -------------------->
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="user-profile">
                  <div class="up-head-w" style="background-image:url(img/ttraderlogo.png); background-repeat: no-repeat; background-size: auto;">
                    {{-- <div class="up-social">
                      <a href="#"><i class="os-icon os-icon-twitter"></i></a><a href="#"><i class="os-icon os-icon-facebook"></i></a>
                    </div> --}}
                    <div class="up-main-info">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="/uploads/avatars/{{ $user->avatar }}">
                        </div>
                      </div>
                      <h1 class="up-header">
                        {{$user->name}}
                      </h1>
                      <h5 class="up-sub-header">
                        {{$user->email}}
                      </h5>
                    </div>
                    <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF"><path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path></g></svg>
                  </div>
                  <div class="up-controls">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="value-pair">
                          <div class="label">
                            Status:
                          </div>
                          @if($user->status != 'Inactive')
                            <div class="value badge badge-pill badge-success">
                          @else
                            <div class="value badge badge-pill badge-danger">
                          @endif
                            {{$user->status}}
                          </div>
                        </div>
                        <div class="value-pair">
                          <div class="label">
                            Member Since:
                          </div>
                          <div class="value">
                            <?php
                              $createdDate = new Carbon($user->created_at);
                              echo $createdDate->toFormattedDateString();
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="up-contents">
                    <h5 class="element-header">
                      Profile Statistics
                    </h5>
                    <div class="row m-b">
                      <div class="col">
                        <div class="row">
                          <div class="col-sm-6 b-r b-b">
                            <div class="el-tablo centered padded">
                              <div class="value">
                                {{$referralCount}}
                              </div>
                              <div class="label">
                                Users Recruited
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 b-b b-r">
                            <div class="el-tablo centered padded">
                              <div class="value">
                                {{$referralsActive}}
                              </div>
                              <div class="label">
                                Active Users Recruited
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 b-r">
                            <div class="el-tablo centered padded">
                              <div class="value">
                                {{ $completedVideoCount }}
                              </div>
                              <div class="label">
                                Videos Completed
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            START - Sidebar
            -------------------->
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Demo Mode
                </h6>
                <div class="element-box-tp">
                  <label class="switch-light switch-candy">
                    <input type="checkbox" id="demoSwitch" name="demo" {!! Auth::user()->trader_demo == 1 ? 'checked' : '' !!} />

                    <span class="float-left" style="width:50%">
                      <span>Off</span>
                      <span>On</span>
                      <a></a>
                    </span>
                  </label>
                </div>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Change Password
                </h6>
                <div class="element-box-tp">
                    <form method="post" action="/settings/updatepassword">
                      {!! csrf_field() !!}

                        <input type="password" name="oldPassword" placeholder="Old Password" class="form-control">

                        <input type="password" name="newPassword" placeholder="New Password" class="form-control">
                        <input type="password" name="newPassConfirm" placeholder="Confirm New Password" class="form-control">
                        <br>
                        <button class="btn btn-success" type="submit" id="updatePassword">Update Account Password</button>
                    </form>
                </div>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Update Avatar
                </h6>
                <div class="element-box-tp">
                  <form enctype="multipart/form-data" action="/settings/updateprofilepic" method="POST">
                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}"><br><br>
                    <input type="file" name="avatar" class="form-control">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></br>
                    <button class="btn btn-success" type="submit" id="updateProfilePicture">Update Profile Picture</button>
                  </form>
                </div>
              </div>

              <div class="element-wrapper">
                <h6 class="element-header">
                  Update Account Info
                </h6>
                <div class="element-box-tp">
                  <form action="/settings/updateuserprofile" method="POST">
                    {{-- <label>Username (This will change your login)</label>
                    <input type="text" name="username" value="{{ $user->username }}" class="form-control"> --}}
                    <label>Email</label>
                    <input type="text" name="email" value="{{$user->email}}" class="form-control">
                    <label>Phone #</label>
                    <input type="text" name="phone" value="{{$user->phone_number}}" class="form-control">
                    <label>Name</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></br>
                    <button class="btn btn-success" type="submit" id="updateProfile">Update Profile</button>
                  </form>
                </div>
              </div>

              <div class="element-wrapper">
                <h6 class="element-header">
                  Team Members
                </h6>
                <div class="element-box-tp">
                  {{-- <div class="input-search-w">
                    <input class="form-control rounded bright" placeholder="Search team members..." type="search">
                  </div> --}}
                  <div class="users-list-w">
                    @foreach($referralUsers as $ru)
                      @if($ru->status != 'Inactive')
                        <div class="user-w with-status status-green">
                      @else
                        <div class="user-w with-status status-red">
                      @endif
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="/uploads/avatars/{{ $ru->avatar }}">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          {{ $ru->name }}
                        </h6>
                        <div class="user-role">
                          @if($ru->status == 'Admin')Admin
                          @elseif($ru->status == 'Free')Free Account
                          @elseif($ru->plan == 1)Customer
                          @else Affiliate
                          @endif
                        </div>
                      </div>
                      {{-- <div class="user-action">
                        <div class="os-icon os-icon-email-forward"></div>
                      </div> --}}
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            END - Sidebar
            -------------------->
          </div>
        </div>
  @endif
@endsection
