<!DOCTYPE html>
<html>
  <head>
    <title>Trio Trader Login</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="World Crypto Currency Cryptocurrency Summary Summaries Information Education Mentorship Bitcoin BTC Litecoin LTC Trio Trader Signals" name="keywords">
    <meta content="Trio Software Inc." name="author">
    <meta content="Trio Trader" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="/img/ttradericon.png" rel="shortcut icon">
    <link href="/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="/css/main.css?version=3.3" rel="stylesheet">
  </head>

  <body class="auth-wrapper">

    {{-- <h1>Down for Maintenance</h1> --}}

    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">
          <a href="/"><img alt="" src="/img/ttraderlogo.png" style="max-width:100%"></a>
        </div>
        <h4 class="auth-header">
          Login Form
        </h4>
        <form role="form" method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }} ">
            <label for="">Username (Not Email)</label>
            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Enter Your Username" required autofocus>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
          </div>
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="">Password</label>
            <input id="password" type="password" class="form-control" name="password" placeholder="Enter Your Password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
          </div>
          <div class="buttons-w">
            <button class="btn btn-primary">Log me in</button>
            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>

          </div>
          {{-- <div class="row">
            <div class="col-md-12 offset-md-5">
              <a class="bnt btn-link" href="/register" style="">
                  Register Here
              </a>
            </div>
          </div> --}}
          <div class="buttons-w">

          </div>
        </form>
      </div>
    </div>
  </body>
</html>


{{-- <div class="form-check-inline">
  <label class="form-check-label"><input class="form-check-input" type="checkbox">Remember Me</label>
</div> --}}
