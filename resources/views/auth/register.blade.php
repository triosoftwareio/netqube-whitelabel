<html>
<head>
    <title>Trio Trader Registration</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="World Crypto Currency Cryptocurrency Summary Summaries Information Education Mentorship Bitcoin BTC Litecoin LTC Trio Trader Signals" name="keywords">
    <meta content="Trio Trader" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="/img/ttradericon.png" rel="shortcut icon">
    <link href="/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/main.css?version=3.3" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="/bower_components/vide/dist/jquery.vide.min.js"></script>
    <script src="/js/js-cookie/src/js.cookie.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div class="all-wrapper menu-side with-pattern">
    <div class="auth-box-w wider">
        <div class="logo-w">
            <a href="/"><img alt="" src="/img/ttraderlogo.png" style="max-width:100%"></a>
        </div>
        <h4 class="auth-header">
          @if(isset($_GET['name']) && isset($_GET['sponsor'])) Register with {{ $_GET['name'] }}
          @else Create new account
          @endif
        </h4>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}" id="registrationForm">
            {{ csrf_field() }}
            @if(!empty($_GET['package']))
                <div id="package" class="form-group has-error">
                    <div class="col-md-12">
                        <p style="color: #333;text-align:center;">Congratulations your are about to enroll with the
                            {{$_GET['package']}} Package priced at ${{ $_GET['price'] }}
                            {{ $costBtc }} BTC (${{ $btcPrice }} USD/BTC) <br>
                            and your sponsor is: <span id="usernameInfo">{{ $_GET['sponsor'] }}</span> </p>
                    </div>
                </div>
            @endif

            @if ($errors->has('package'))
                <div class="form-group has-error">
                    <div class="col-md-12">
                        <p style="color: #f44336;text-align:center;">Looks like that package doesn't exist.</p>
                    </div>
                </div>
              @else
                <input type="hidden" name="package" id="package" value="{{$_GET['package']}}">

            @endif

            @if ($errors->has('sponsorid'))
                <div class="form-group has-error">
                    <div class="col-md-12">
                        <p style="color: #f44336;text-align:center;">Looks like you don't have a sponsor. This is an invite only system</p>
                    </div>
                </div>
            @else
                <div class="form-group has-error" id="nosponsor" style="display:none;">
                    <div class="col-md-12">
                        <p style="color: #f44336;text-align:center;">Looks like you dont have a sponsor. This is an invite only system</p>
                    </div>
                </div>
            @endif

            <div class="row">
              <div class="col">
                <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                    <label for="firstName">First Name</label>
                    <input placeholder="Enter First Name" id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required autofocus>
                    <div class="col-md-6">
                        @if ($errors->has('firstName'))
                            <span class="help-block">
                          <strong>{{ $errors->first('firstName') }}</strong>
                      </span>
                        @endif
                    </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                    <label for="name">Last Name</label>
                    <input placeholder="Enter Last Name" id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required autofocus>
                    <div class="col-md-6">
                        @if ($errors->has('lastName'))
                            <span class="help-block">
                          <strong>{{ $errors->first('lastName') }}</strong>
                      </span>
                        @endif
                    </div>
                </div>
              </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username">Username (Used to Login)</label>
                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Enter Username" required autofocus>
                <div class="col-md-6">
                    @if ($errors->has('username'))
                        <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">E-Mail Address</label>
                <input placeholder="Enter Your E-Mail" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                <div class="col-md-6">
                    @if ($errors->has('email'))
                        <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                    @endif
                </div>
                <div class="pre-icon os-icon os-icon-email-2-at2"></div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                    <label for="email">Country</label>
                    <select name="country" id="country" class="form-control">
                      @foreach($countryCodes as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                    </select>
                    <div class="col-md-6">
                        @if ($errors->has('country'))
                            <span class="help-block">
                          <strong>{{ $errors->first('country') }}</strong>
                      </span>
                        @endif
                    </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="email">City</label>
                    <input placeholder="Enter City" id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required>
                    <div class="col-md-6">
                        @if ($errors->has('city'))
                            <span class="help-block">
                          <strong>{{ $errors->first('city') }}</strong>
                      </span>
                        @endif
                    </div>
                </div>
              </div>
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="email">Address</label>
                <input placeholder="Enter Address" id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                <div class="col-md-6">
                    @if ($errors->has('address'))
                        <span class="help-block">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('postalCode') ? ' has-error' : '' }}">
                <label for="email">Postal Code</label>
                <input placeholder="Enter Postal Code" id="postalCode" type="text" class="form-control" name="postalCode" value="{{ old('postalCode') }}" required>
                <div class="col-md-6">
                    @if ($errors->has('postalCode'))
                        <span class="help-block">
                      <strong>{{ $errors->first('postalCode') }}</strong>
                  </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="email">Phone Number</label>
                <input placeholder="Enter Phone Number" id="phoneNum" type="text" class="form-control" name="phoneNum" value="{{ old('phoneNum') }}" required>
                <div class="col-md-6">
                    @if ($errors->has('phoneNum'))
                        <span class="help-block">
                      <strong>{{ $errors->first('phoneNum') }}</strong>
                  </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('sponsorId') ? ' has-error' : '' }}">
                <label for="sponsorId">Sponsor Username</label>
                <input id="sponsorId" type="hidden" class="form-control" name="sponsorId" value="{{(isset($_GET['sponsor'])) ? $_GET['sponsor'] : 'triotrader'}}" required>
                <p>{{ (isset($_GET['sponsor'])) ? $_GET['sponsor'] : 'triotrader' }}</p>
                <div class="col-md-6">
                    @if ($errors->has('sponsorId'))
                        <span class="help-block">
                        <strong>{{ $errors->first('sponsorId') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Enter Password"required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                        @endif
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"required>
                    </div>
                </div>
            </div>
            <div class="buttons-w">
              <h6>By Registering, you agree to the following</h6>
              <a class="btn btn-link" href="/legal/TT_User_Agreement-Nov0718.pdf" download>User Agreement</a>
              <a class="btn btn-link" href="/legal/TS_Privacy_Policy-Nov0718.pdf" download>Privacy Policy</a>
              <a class="btn btn-link" href="/legal/TT_Affiliate_Agreement-Nov0718.pdf" download>Affiliate Agreement</a>
            </div>
            <div class="g-recaptcha" data-sitekey="6LddG3wUAAAAAHGua3iOXLR7t-MfmpxiqcBF1pTg"></div>
            <div class="buttons-w">
                <button class="btn btn-primary" type="submit" name="btc" value="{{ $costBtc }}" onclick="return buttonClick(this)">Register with BTC</button>
                <a class="btn btn-link" href="/login">
                    Login Here
                </a>
            </div>
            <input type="hidden" name="clickedButton" id="clickedButton" value="">
            <input type="hidden" name="costAmt" id="costAmt" value="">
            <input type="hidden" name="btcPrice" value="{{ $btcPrice }}">
            {{-- <input type="hidden" id="sponsorUsername" value="{{(isset($_GET['sponsor'])) ? $_GET['sponsor'] : ''}}"> --}}
        </form>
    </div>
</div>
{{-- <h1>Down for Maintenance</h1> --}}
<script>
    $(document).ready(function(){
        if($('#sponsorUsername').val() != null){
            Cookies.set('sponsor', $('#sponsorUsername').val());
            $('#sponsorid').val(Cookies.get('sponsor'));
            $('#usernameInfo').text(Cookies.get('sponsor'));
            console.log('No Cookie');
        }else if($('#sponsorUsername').val() == null && !Cookies.get('sponsor')){
            console.log('No Sponsor');
        }else{
            $('#sponsorid').val(Cookies.get('sponsor'));
            $('#usernameInfo').text(Cookies.get('sponsor'));
            // $("#sponsorid").prop('readonly', true);
            console.log('Cookie Exists');
        }
    });
</script>
<script>
  function buttonClick(btn){
    document.getElementById('clickedButton').value = btn.name;
    document.getElementById('costAmt').value = btn.value;
    return true;
  }
</script>
</body>
</html>
