<!DOCTYPE html>
<html>
  <head>
    <title>TrioTrader Forgot Password</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="triotrader TrioTrader Summary Summaries Information Education Mentorship Bitcoin BTC Litecoin LTC" name="keywords">
    <meta content="Trio Software Inc." name="author">
    <meta content="TrioTrader NetQube" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="/img/ttradericon.png" rel="shortcut icon">
    <link href="/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="/css/main.css?version=3.3" rel="stylesheet">
  </head>

  <body class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">
          <a href="/"><img alt="" src="/img/ttraderlogo.png" style="max-width:100%"></a>
        </div>
        <h4 class="auth-header">
          Reset Password
        </h4>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form role="form" method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div class="col">
                  <input id="email" type="email" placeholder="Email Address" class="form-control" name="email" value="{{ old('email') }}" required>

                  @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                      Send Password Reset Link
                  </button>
              </div>
          </div>

        </form>
      </div>
    </div>
  </body>
</html>
