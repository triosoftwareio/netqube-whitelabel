@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <div class="element-header">
                  <h6 class="title">Wallet for commissions</h6>
                </div>
                <div class="element-box">
                  @if(Auth::user()->wallet_address != NULL)
                    <p>Current Address: {{ Auth::user()->wallet_address }}</p>
                  @else
                    <p>No Address on file</p>
                  @endif
                  <div style="padding:20px !important;">
                    @if(Auth::user()->status == 'Active')
                      <form method="post" action="/wallet/address/update">
                        {!! csrf_field() !!}
                          <div class="input-group">
                            <input type="text" name="wallet_address" placeholder="Your Bitcoin Wallet Address" class="form-control">
                          </div>

                          <button class="btn btn-success" type="submit" id="submitWithdrawl">Update</button>
                      </form>
                    @endif
                  </div>
                </div>
              </div>
            </div>
    </div>
  </div>
@endsection
