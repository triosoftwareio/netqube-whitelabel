@extends('layouts.newApp')

@section('content')

    <?php
      use Carbon\Carbon;
    ?>

    <div class="content-w">
      <div class="container-fluid" style="min-height:1200px;">

        <div class="content-i">
          <div class="content-box">
            <div class="row">
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    My Commissions
                  </h6>
                  <div class="element-box el-tablo">
                    <div class="label">
                      Total Commissions Earned:
                    </div>
                    <div class="value">
                      {{$totalPaid}}<small>BTC</small>
                    </div>
                  </div>
                  <div class="element-box">
                    <div style="padding:20px !important;">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Date
                              </th>
                              @if(Auth::user()->status == 'Admin')
                                <th>
                                  Name
                                </th>
                                <th>
                                  Email
                                </th>
                                <th>
                                  Username
                                </th>
                              @endif
                              <th>
                                BTC Payment
                              </th>
                              <th>
                                Sent To Address
                              </th>
                              <th>
                                Status
                              </th>
                              <th>
                                Error Message
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($commissions as $commission)
                              <tr>
                                <td>
                                  <?php
                                    $thisDate = new Carbon($commission->created_at);
                                    echo $thisDate->toFormattedDateString();
                                  ?>
                                </td>
                                @if(Auth::user()->status == 'Admin')
                                  @foreach($commissionedUsers as $cu)
                                    @if($cu->id == $commission->user_id)
                                      <td>
                                        {{$cu->name}}
                                      </td>
                                      <td>
                                        {{$cu->email}}
                                      </td>
                                      <td>
                                        {{$cu->username}}
                                      </td>
                                      <?php break; ?>
                                    @endif
                                  @endforeach
                                @endif
                                <td>
                                  {{$commission->amount}}
                                </td>
                                <td>
                                  {{$commission->wallet_paid_to_address}}
                                </td>
                                <td>
                                  {{$commission->status}}
                                </td>
                                <td>
                                  @if($commission->status == 'Success')
                                    None
                                  @else
                                    {{ $commission->error_message }}
                                  @endif
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- End Row --}}
        </div>
      </div>
    </div>
  </div>

@endsection
