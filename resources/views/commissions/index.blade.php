@extends('layouts.newApp')

@section('content')

    <div class="content-w">
      <div class="container-fluid" style="min-height:1200px;">

        <div class="content-i">
          <div class="content-box">
            <div class="row">
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    My Commissions <b>(PAGE STILL UNDER DEVELOPMENT)</b>
                  </h6>
                  <div class="element-box el-tablo">
                    <div class="row">
                      <div class="col">
                        <div class="label">
                          Total Commissions Earned:
                        </div>
                        <div class="value">
                          {{$totalPaid}}<small>BTC</small>
                        </div>
                      </div>
                      <div class="col">
                        <div class="label">
                          Direct Commissions Earned
                        </div>
                        <div class="value">
                          {{$directCommissions}}<small>BTC</small>
                        </div>
                      </div>
                      <div class="col">
                        <div class="label">
                          Team Commissions Earned
                        </div>
                        <div class="value">
                          {{$teamCommissions}}<small>BTC</small>
                        </div>
                      </div>
                      <div class="col">
                        <div class="label">
                          Match Bonus Commissions Earned
                        </div>
                        <div class="value">
                          {{$matchCommissions}}<small>BTC</small>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="element-box">
                    {{-- <div style="padding:20px !important;"> --}}
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Date
                              </th>
                              <th>
                                BTC Payment
                              </th>
                              <th>
                                Sent To Address
                              </th>
                              <th>
                                Status
                              </th>
                              <th>
                                Error Message
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($commissions as $commission)
                              <tr>
                                <td>
                                  <?php
                                    $thisDate = new Carbon($commission->created_at);
                                    echo $thisDate->toFormattedDateString();
                                  ?>
                                </td>
                                <td>
                                  {{$commission->amount}}
                                </td>
                                <td>
                                  {{$commission->wallet_paid_to_address}}
                                </td>
                                <td>
                                  {{$commission->status}}
                                </td>
                                <td>
                                  @if($commission->status == 'Success')
                                    None
                                  @else
                                    {{ $commission->error_message }}
                                  @endif
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    {{-- </div> --}}
                  </div>
                </div>
              </div>
            </div>
            {{-- End Row --}}
            <div class="row">
              <div class="col-md-8">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Today's Sales
                  </h6>
                  <div class="element-box el-tablo">
                    @foreach($usersToPay as $key => $affiliate)
                      @if($affiliate['user_id'] == Auth::id())
                        {{-- {{ dd($affiliate) }} --}}
                      <div class="row">
                        <div class="col">
                          <div class="label">
                            Binary
                          </div>
                          <div class="value">
                            {{$affiliate['team_commission']}}<small>BTC</small>
                          </div>
                        </div>
                        <div class="col">
                          <div class="label">
                            Match
                          </div>
                          <div class="value">
                            {{$affiliate['match_bonus']}}<small>BTC</small>
                          </div>
                        </div>
                        <div class="col">
                          <div class="label">
                            Direct
                          </div>
                          <div class="value">
                            {{$affiliate['direct_commission']}}<small>BTC</small>
                          </div>
                        </div>
                      </div>
                      @endif
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Qualifications
                  </h6>
                  <div class="element-box-tp">
                    <div class="row">
                      <div class="col-md-10">
                        Direct Referral
                      </div>
                      <div class="col">
                        <i class="fa fa-check"></i>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        $3,000 Binary
                      </div>
                      <div class="col">
                        @if($qualified == true)
                          <i class="fa fa-check"></i>
                        @else
                          <i class="fa fa-times"></i>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        $4,500 Binary
                      </div>
                      <div class="col">
                        @if($qualified2 == true)
                          <i class="fa fa-check"></i>
                        @else
                          <i class="fa fa-times"></i>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        Generation 1 Match 20%
                      </div>
                      <div class="col">
                        @if($match1 == true)
                          <i class="fa fa-check"></i>
                        @else
                          <i class="fa fa-times"></i>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        Generation 2 Match 20%
                      </div>
                      <div class="col">
                        @if($match2 == true)
                          <i class="fa fa-check"></i>
                        @else
                          <i class="fa fa-times"></i>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        Generation 3 Match 10%
                      </div>
                      <div class="col">
                        @if($match3 == true)
                          <i class="fa fa-check"></i>
                        @else
                          <i class="fa fa-times"></i>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- End Row --}}
            <div class="row">
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Direct Commissions
                  </h6>
                  <div class="element-box el-tablo">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Date
                              </th>
                              <th>
                                User
                              </th>
                              <th>
                                Email
                              </th>
                              <th>
                                Amount
                              </th>
                              <th>
                                Side
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($directUsers as $du)
                              @foreach($users as $checkUser)
                                @if($checkUser->id == $du->child_id)
                                  @if($checkUser->status == 'Active' && $checkUser->plan == '1')
                                    <tr>
                                      <td>{{ $checkUser->last_renewal }}</td>
                                      <td>{{ $checkUser->username }}</td>
                                      <td>{{ $checkUser->email }}</td>
                                      <td>{{ round(100/$checkUser->btcPrice,5) }}</td>
                                      <td>{{ $du->side }}</td>
                                    </tr>
                                  @endif
                                @endif
                              @endforeach
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        {{-- End Row --}}
            <div class="row">
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Team Commissions (in development)
                  </h6>
                  <div class="element-box el-tablo">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Date
                              </th>
                              <th>
                                User
                              </th>
                              <th>
                                Email
                              </th>
                              <th>
                                Amount
                              </th>
                              {{-- <th>
                                Paid
                              </th> --}}
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              printDownline($downline, Auth::id(), 0, $users, null, true);
                            ?>
                          </tbody>
                        </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

@endsection



{{-- function printTree($downlines, $userId, $number, $users, $side, $first){
  $number++;
  if($number < 4){
    echo '<ul>';
    if(!empty($downlines['left_child'])){
        echo '<li>';
        echo '<a style="background-image: url(/uploads/avatars/'.$users->where('id', $downlines['left_child']['id'])->first()->avatar.')" href="/binary/get/children?parent='.base64_encode($downlines['left_child']['id']).'&side='.(isset($_GET['side']) ? $_GET['side'] : $side ).'"></a>';
        if($downlines['left_child']['status'] === 'Active'){
            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['left_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #6df723">Paid/';
            if($downlines['left_child']['plan_name'] == 'Sales_Rep'){
              echo 'Affiliate ';
            } else {
              echo 'Customer ';
            }
            echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['left_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_child']['left_volume']/150).' - '.'Right: '.($downlines['left_child']['right_volume']/150).'</p>';
        }else{
            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['left_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #eb2323">Not Paid/';
            if($downlines['left_child']['plan_name'] == 'Sales_Rep'){
              echo 'Affiliate ';
            } else {
              echo 'Customer ';
            }
            echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['left_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_child']['left_volume']/150).' - '.'Right: '.($downlines['left_child']['right_volume']/150).'</p>';
        }

        if($first == true){
            printTree($downlines['left_child'], $downlines['left_child']['id'], $number, $users, 'left', false);
        }else{
            printTree($downlines['left_child'], $downlines['left_child']['id'], $number, $users, $side, false);
        }
        echo '</li>';
    }else{
        echo '<li>';
        echo '<a style="background-image: url(/img/open3.png)" href="#"></a>';
        echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">Open Position</p>';
        // Removed:
        // .'<p style="margin: 0;font-size: 12px;line-height: 16px;"></p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">0 VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left:  - '.'Right: </p>'
//                              printTree($downline, $userId, $number);
        echo '</li>';
    }
    if(!empty($downlines['right_child'])){
        echo '<li>';
        echo '<a style="background-image: url(/uploads/avatars/'.$users->where('id', $downlines['right_child']['id'])->first()->avatar.')" href="/binary/get/children?parent='.base64_encode($downlines['right_child']['id']).'&side='.(isset($_GET['side']) ? $_GET['side']  : $side ).'"></a>';
        if($downlines['right_child']['status'] === 'Active'){
            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['right_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #6df723">Paid/';
            if($downlines['right_child']['plan_name'] == 'Sales_Rep'){
              echo 'Affiliate ';
            } else {
              echo 'Customer ';
            }
            echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['right_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['right_child']['left_volume']/150).' - '.'Right: '.($downlines['right_child']['right_volume']/150).'</p>';
        }else{
            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['right_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #eb2323">Not Paid/';
            if($downlines['right_child']['plan_name'] == 'Sales_Rep'){
              echo 'Affiliate ';
            } else {
              echo 'Customer ';
            }
            echo '</p><p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['right_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['right_child']['left_volume']/150).' - '.'Right: '.($downlines['right_child']['right_volume']/150).'</p>';
        }
        if($first == true){
            printTree($downlines['right_child'], $downlines['right_child']['id'], $number, $users, 'right', false);
        }else{
            printTree($downlines['right_child'], $downlines['right_child']['id'], $number, $users, $side, false);
        }
        echo '</li>';
    }else{
        echo '<li>';
        echo '<a style="background-image: url(/img/open3.png)" href="#"></a>';
        echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">Open Position</p>';
        // Removed
        // .'<p style="margin: 0;font-size: 12px;line-height: 16px;"></p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">0 VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left:  - '.'Right: </p>'
//                              printTree($downline, $userId, $number);
        echo '</li>';
    }
    echo '</ul>';

  }

} --}}
