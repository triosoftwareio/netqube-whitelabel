@extends('layouts.App')

@section('content')
<style>* {
  margin: 0;
  padding: 0;
}

*, *:before, *:after {
  box-sizing: border-box;
}

html, body {
  height: 100%;
  background: #f7f7f7;
  color: #5b5b5b;
  font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
}

body {
  margin: 20px 0;
}

.faq-wrapper {
  margin-bottom: 20px;
  border-top: 1px solid #f0f0f0;
  background: #fff;
  transform: translateZ(0);
}

input[type="checkbox"] {
  position: absolute;
  opacity: 0;
}

label img, label p {
  display: inline-block;
  vertical-align: top;
}

label img {
  margin-right: 10px;
  height: 1.4em;
}

label p {
  line-height: 1.4em;
}

label {
  position: relative;
  display: block;
  margin: 0 0 0 16px;
  padding: 16px 0;
  border-bottom: 1px solid #f0f0f0;
  background: #fff;
  font-size: 1em;
  line-height: 1.2em;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;
}

label:after {
  position: absolute;
  top: 50%;
  right: 20px;
  margin-top: -10px;
  height: 20px;
  content: '>';
  line-height: 20px;
  transform: rotate(90deg);
}

section {
  overflow: hidden;
  height: 0;
  transition: .3s all;
}

#question1:checked ~ label[for*='1']:after,
#question2:checked ~ label[for*='2']:after,
#question3:checked ~ label[for*='3']:after,
#question4:checked ~ label[for*='4']:after {
  transform: rotate(270deg);
}

#question1:checked ~ #answer1,
#question2:checked ~ #answer2,
#question3:checked ~ #answer3,
#question4:checked ~ #answer4 {
  height: auto;
}

section p {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid #f0f0f0;
  color: #999;
  font-size: 1em;
  line-height: 1.2em;
}
</style>
@if($inactive)
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  @if(Auth::user()->status != 'Active')
                    {{-- <h4 style="text-align:center;">Complete your deposit to gain access to your back office & earn on your investment.</h4>
                    <h5 style="text-align:center;">Payment accepted in Bitcoin.</h5>
                    <h6 style="text-align:center;">Payment could take up to 2 hours to process keep refreshing the page to check if it went through, increasing the transaction fee will allow payments to proccess within 15 minutes.</h6>
                    <img src="{{Auth::user()->payment_address}}" style="display:block;margin:auto;"/>
                    <h6 style="text-align:center;">QR Code Not Working?</h6>
                    <h6 style="text-align:center;text-transform:initial !important;">Send exactly {{substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "amount=") + 7)}} BTC  to {{strstr(substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "bitcoin:") + 8), '?amount=', true)}}</h6> --}}
                    <div style="text-align:center">
                    <p>
                    Dear CryptoBitHub Members,
                    </p>

                    <p>
                    We at CryptoBitHub believe that we have created the world’s best wealth creation platform.
                    </p>
                    <p>
                    During our Pre-Launch we are enabling you to get positioned prior to accepting the payment for your package.
                    </p>
                    <p>
                    We will notify you in advance when the payment for your package is due.
                    </p>
                    <p>
                    We look forward to working with you.
                    </p>

                    <p>
                    Thank you
                    </p>

                    <p>
                    All the best from the team at CryptoBitHub
                    </p>
                    </div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="green">
            <i class="mdi mdi-currency-btc"></i>
          </div>
          <div class="card-content">
            <p class="category">Bitcoin Balance</p>
            <h3 class="title">{{round($balance, 4)}}<small>BTC</small></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="mdi mdi-wallet"></i> <a href="/wallet">Transfer Funds To My Wallet</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="green">
            <i class="mdi mdi-chart-areaspline"></i>
          </div>
          <div class="card-content">
            <p class="category">Lesser Leg Volume</p>
            <h3 class="title">{{round($lesserLegVolume)}} BTC</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              Lesser Leg Volume
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="green">
            <i class="mdi mdi-account-multiple"></i>
          </div>
          <div class="card-content">
            <p class="category">Total Volume</p>
            <h3 class="title">{{round($totalVolume)}} BTC</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              Binary Downline
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="green">
            <i class="mdi mdi-cash-multiple"></i>
          </div>
          <div class="card-content">
            <p class="category">Total Profit</p>
            <h3 class="title">+{{round($profitCount, 4)}} BTC</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
             Just Updated
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header card-chart" data-background-color="green">
            <div class="ct-chart" id="dailySalesChart"></div>
          </div>
          <div class="card-content">
            <h4 class="title">Daily Sales</h4>
            <p class="category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 55%  </span> increase in today sales.</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> updated 4 minutes ago
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-header card-chart" data-background-color="orange">
            <div class="ct-chart" id="emailsSubscriptionChart"></div>
          </div>
          <div class="card-content">
            <h4 class="title">Email Subscriptions</h4>
            <p class="category">Last Campaign Performance</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> campaign sent 2 days ago
            </div>
          </div>

        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-header card-chart" data-background-color="red">
            <div class="ct-chart" id="completedTasksChart"></div>
          </div>
          <div class="card-content">
            <h4 class="title">Completed Tasks</h4>
            <p class="category">Last Campaign Performance</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> campaign sent 2 days ago
            </div>
          </div>
        </div>
      </div>
    </div> --}}
    <div class="row">
      <div class="col-md-12">
        <div class="card card-nav-tabs">
          <div class="card-header" data-background-color="green">
            <h4 class="title">Earnings Progress</h4>
            <a href="/upgrade" class="category">Upgrade Plan</a>
          </div>
          <div class="progress" style="height:50px;margin:20px;">
            <div class="progress-bar progress-bar-striped active" role="progressbar"
            aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%; padding-top:15px;">
            </div>
          </div>
          <div style="margin:20px;text-align:center;">
            <h4 class="title">{{$percentage}}%</h4>
            <h4 class="title">Daily Max: {{$dailyMax}}</h4>
            <a href="/upgrade" class="title">Click Here To Upgrade Plan</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-nav-tabs">
          <div class="card-header" data-background-color="green">
            <h4 class="title">Binary Affiliate Link</h4>
          </div>
          <div style="margin:20px;text-align:center;">
            <h5 style="text-align:center;">coinxl.com#{{Auth::user()->username}} </h5>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 col-md-12">
        <div class="card">
                        <div class="card-header" data-background-color="green">
                            <h4 class="title">Withdrawls</h4>
                            <p class="category">All Withdrawls</p>
                        </div>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead class="text-success">
                                  <th>Transaction Id</th>
                                  <th>Amount</th>
                                  <th>Date</th>
                                </thead>
                                <tbody>
                                  @foreach($withdrawls as $withdrawl)
                                    <tr>
                                      <td>{{$withdrawl->id}}</td>
                                      <td>{{$withdrawl->amount}}</td>
                                      <td>{{$withdrawl->created_at}}</td>
                                    </tr>
                                  @endforeach
                            </table>
                        </div>
                    </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="card">
                        <div class="card-header" data-background-color="green">
                            <h4 class="title">Deposits</h4>
                            <p class="category">All Deposits</p>
                        </div>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead class="text-warning">
                                  <th>Transaction ID</th>
                                  <th>Amount</th>
                                  <th>Date</th>
                                </thead>
                                <tbody>
                                  @foreach($payments as $payment)
                                    <tr>
                                      <td>{{$payment->id}}</td>
                                      <td>{{$payment->amount}} BTC</td>
                                      <td>{{$payment->created_at}}</td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header" data-background-color="green">
              <h4 class="title">FAQ</h4>
              <p class="category">Frequently Asked Questions</p>
          </div>
          <div class="card-content table-responsive body">
            <div class="faq-wrapper">
              <input id="question1" type="checkbox" name="toggle" class="question" />
              <label for="question1">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABMCAYAAAAx3tSzAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABCFJREFUeNrsm09oFFcYwH9JBjY0YGSFHSJEUkjooT0UAwo9BFo8CBEqNOQQkcVDIQWhhxYPW2LxkDlVUCwUejFz6KEFIcccRMFApQElHoKKK4pKlg00YGDFhQntYb6VIXnzdt8m48y074Nld9/73sz7zfv+vJk3r6e0ME8X4gCjwDRwAWgA28C4/N5StBkBvgIuAoGUHRfdDYX+kOjPAf1SNiG6tZZSvVwx7ng30gc8jPwfkO914LAC2BGAn3aUPwHGFMAOUAKu7ShfBT6NAptKb5ftBjV1JxRlB4A/Y/QnFWUFgVPJNHuQuBEuAR+IKTXkigYdHrN/Hy5en0b/gKrQ9b1R4SkAm0CjXq5sththRw44BzwTs30h5jhgYO775TodDZLre0XgL+nvqvT5vOt7BR3wkJjLa+D8Dr0XwEdkUFzfG5a+FXdUXQIuur53TgVckCj6m+bY42RTRjTxoQJUXN9zosAF4GNNo5a8zChwtU39KLDo+t5MFPhem0aXJXhlUbaAT9roTAo4DvB7G+UJYNmgA01FmS7Cqy7ktkb/dfRPvVxpAGtAj+t7nwO3Ytpdcn3vdq+Ysw52U1H+VtOmFgPsGbhKAHwfo/9Uc+5N4EdN/Z2e0sL8PzGVU8ANTWoYkZmSKg83Y6aK6zF5WDUVLQJ/K8oPRQdBNbV0fe9X4GuTicfVNn4dyHRwAjgpgIH4+rbG1z4T/e1IbGhqTH0cOBXJ7Tr9qCybAleB5x0Ei2UD/24Ad+XTaSy4Lx9TWTOdSzfJt7wyBQ5yDvx2v++WcisW+L8u3dy2fSk5ciilPrdS2oqkzq2kgUeAK1m5O3R97029XAmSMOlh4FiGYAHqrZuCJICbpubznqSRFPAG8Aj4OUOwYxg+wezGh/8Alggf9KUlTbn4Gyb+2y3wMjkWm4c7kKK060u571uR29JEgSvAdxkZsA9d33uVVB4ekEA1myELfUbMSsR+ABcl0Q9kzC1LSQFvED5/upsx4PWkfLgpnxuED+2HU4QMZC5wE3iTdNC6nKXhNV0Qtw8A7MRjt8xIxC4a+v8DwlWGtbwBTwp0NzKVNrCJSQ8RrkPN7OF8c3nzYScFi0oNuCZ+eGYP55vKY9CqAl8YBq0AeIx66TXzwCs2D1tgC2yBLbAFtsAW2AJbYAtsgS2wBbbAFtgCW2ALbIEtcNLATs65Bk2BCzkHLpoCHwOO5hj4hCnwWdJ9h2OvEvtKsUP4cshJRd0i8A3h/qWlPFC6vneUcDl3Vgf8IAYY4BfguvhEE/2ew7QgWz/7xQ3j3hKsAad6SgvzhSyCJCBevVz5oZdw08SR/wHw7ZZJBzLcY6h3i+ZdAuCguOS7CUYgwWlMItws4XadvMtpoCabqnfNqALC1f2qpKTnwLc5Ba0SvhN6MwoL8O8AwUvuYBGaCTgAAAAASUVORK5CYII=" alt="FAQ icon" />
                <p>About our time zone and location</p>
              </label>

              <section id="answer1">
                <p>
                  Our servers are located in the Netherlands, our timezone is UTC.
                </p>
              </section>

              <input id="question2" type="checkbox" name="toggle" class="question" />
              <label for="question2">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABMCAYAAAAx3tSzAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABCFJREFUeNrsm09oFFcYwH9JBjY0YGSFHSJEUkjooT0UAwo9BFo8CBEqNOQQkcVDIQWhhxYPW2LxkDlVUCwUejFz6KEFIcccRMFApQElHoKKK4pKlg00YGDFhQntYb6VIXnzdt8m48y074Nld9/73sz7zfv+vJk3r6e0ME8X4gCjwDRwAWgA28C4/N5StBkBvgIuAoGUHRfdDYX+kOjPAf1SNiG6tZZSvVwx7ng30gc8jPwfkO914LAC2BGAn3aUPwHGFMAOUAKu7ShfBT6NAptKb5ftBjV1JxRlB4A/Y/QnFWUFgVPJNHuQuBEuAR+IKTXkigYdHrN/Hy5en0b/gKrQ9b1R4SkAm0CjXq5sththRw44BzwTs30h5jhgYO775TodDZLre0XgL+nvqvT5vOt7BR3wkJjLa+D8Dr0XwEdkUFzfG5a+FXdUXQIuur53TgVckCj6m+bY42RTRjTxoQJUXN9zosAF4GNNo5a8zChwtU39KLDo+t5MFPhem0aXJXhlUbaAT9roTAo4DvB7G+UJYNmgA01FmS7Cqy7ktkb/dfRPvVxpAGtAj+t7nwO3Ytpdcn3vdq+Ysw52U1H+VtOmFgPsGbhKAHwfo/9Uc+5N4EdN/Z2e0sL8PzGVU8ANTWoYkZmSKg83Y6aK6zF5WDUVLQJ/K8oPRQdBNbV0fe9X4GuTicfVNn4dyHRwAjgpgIH4+rbG1z4T/e1IbGhqTH0cOBXJ7Tr9qCybAleB5x0Ei2UD/24Ad+XTaSy4Lx9TWTOdSzfJt7wyBQ5yDvx2v++WcisW+L8u3dy2fSk5ciilPrdS2oqkzq2kgUeAK1m5O3R97029XAmSMOlh4FiGYAHqrZuCJICbpubznqSRFPAG8Aj4OUOwYxg+wezGh/8Alggf9KUlTbn4Gyb+2y3wMjkWm4c7kKK060u571uR29JEgSvAdxkZsA9d33uVVB4ekEA1myELfUbMSsR+ABcl0Q9kzC1LSQFvED5/upsx4PWkfLgpnxuED+2HU4QMZC5wE3iTdNC6nKXhNV0Qtw8A7MRjt8xIxC4a+v8DwlWGtbwBTwp0NzKVNrCJSQ8RrkPN7OF8c3nzYScFi0oNuCZ+eGYP55vKY9CqAl8YBq0AeIx66TXzwCs2D1tgC2yBLbAFtsAW2AJbYAtsgS2wBbbAFtgCW2ALbIEtcNLATs65Bk2BCzkHLpoCHwOO5hj4hCnwWdJ9h2OvEvtKsUP4cshJRd0i8A3h/qWlPFC6vneUcDl3Vgf8IAYY4BfguvhEE/2ew7QgWz/7xQ3j3hKsAad6SgvzhSyCJCBevVz5oZdw08SR/wHw7ZZJBzLcY6h3i+ZdAuCguOS7CUYgwWlMItws4XadvMtpoCabqnfNqALC1f2qpKTnwLc5Ba0SvhN6MwoL8O8AwUvuYBGaCTgAAAAASUVORK5CYII=" alt="FAQ icon" />
                <p>About CryptoBitHub Bonus</p>
              </label>

              <section id="answer2">
                <p>
                  The CryptoBitHub Bonus is responsible for doubling your bitcoins in 30-60 days. It is processed daily between 1:00 AM and 1:15 AM (UTC); you can check it in your dashboard.
                </p>
              </section>

              <input id="question3" type="checkbox" name="toggle" class="question" />
              <label for="question3">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABMCAYAAAAx3tSzAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABCFJREFUeNrsm09oFFcYwH9JBjY0YGSFHSJEUkjooT0UAwo9BFo8CBEqNOQQkcVDIQWhhxYPW2LxkDlVUCwUejFz6KEFIcccRMFApQElHoKKK4pKlg00YGDFhQntYb6VIXnzdt8m48y074Nld9/73sz7zfv+vJk3r6e0ME8X4gCjwDRwAWgA28C4/N5StBkBvgIuAoGUHRfdDYX+kOjPAf1SNiG6tZZSvVwx7ng30gc8jPwfkO914LAC2BGAn3aUPwHGFMAOUAKu7ShfBT6NAptKb5ftBjV1JxRlB4A/Y/QnFWUFgVPJNHuQuBEuAR+IKTXkigYdHrN/Hy5en0b/gKrQ9b1R4SkAm0CjXq5sththRw44BzwTs30h5jhgYO775TodDZLre0XgL+nvqvT5vOt7BR3wkJjLa+D8Dr0XwEdkUFzfG5a+FXdUXQIuur53TgVckCj6m+bY42RTRjTxoQJUXN9zosAF4GNNo5a8zChwtU39KLDo+t5MFPhem0aXJXhlUbaAT9roTAo4DvB7G+UJYNmgA01FmS7Cqy7ktkb/dfRPvVxpAGtAj+t7nwO3Ytpdcn3vdq+Ysw52U1H+VtOmFgPsGbhKAHwfo/9Uc+5N4EdN/Z2e0sL8PzGVU8ANTWoYkZmSKg83Y6aK6zF5WDUVLQJ/K8oPRQdBNbV0fe9X4GuTicfVNn4dyHRwAjgpgIH4+rbG1z4T/e1IbGhqTH0cOBXJ7Tr9qCybAleB5x0Ei2UD/24Ad+XTaSy4Lx9TWTOdSzfJt7wyBQ5yDvx2v++WcisW+L8u3dy2fSk5ciilPrdS2oqkzq2kgUeAK1m5O3R97029XAmSMOlh4FiGYAHqrZuCJICbpubznqSRFPAG8Aj4OUOwYxg+wezGh/8Alggf9KUlTbn4Gyb+2y3wMjkWm4c7kKK060u571uR29JEgSvAdxkZsA9d33uVVB4ekEA1myELfUbMSsR+ABcl0Q9kzC1LSQFvED5/upsx4PWkfLgpnxuED+2HU4QMZC5wE3iTdNC6nKXhNV0Qtw8A7MRjt8xIxC4a+v8DwlWGtbwBTwp0NzKVNrCJSQ8RrkPN7OF8c3nzYScFi0oNuCZ+eGYP55vKY9CqAl8YBq0AeIx66TXzwCs2D1tgC2yBLbAFtsAW2AJbYAtsgS2wBbbAFtgCW2ALbIEtcNLATs65Bk2BCzkHLpoCHwOO5hj4hCnwWdJ9h2OvEvtKsUP4cshJRd0i8A3h/qWlPFC6vneUcDl3Vgf8IAYY4BfguvhEE/2ew7QgWz/7xQ3j3hKsAad6SgvzhSyCJCBevVz5oZdw08SR/wHw7ZZJBzLcY6h3i+ZdAuCguOS7CUYgwWlMItws4XadvMtpoCabqnfNqALC1f2qpKTnwLc5Ba0SvhN6MwoL8O8AwUvuYBGaCTgAAAAASUVORK5CYII=" alt="FAQ icon" />
                <p>About Binary Bonus & CryptoBitHub Holding Tank</p>
              </label>

              <section id="answer3">
                <p>
                  People of all plans are entitled to receive the Binary Bonus. This bonus is calculated by multiplying the points of your lower leg by the binary percentage from your contracted plan and the points paid are discounted from both legs. It is processed daily between 1:00 AM and 1:15 AM (UTC); you can check the entry in your account through the menu > Binary Tree. You can also manually place users from your CryptoBitHub Holding tank into your binary tree as needed.
                </p>
              </section>

              <input id="question4" type="checkbox" name="toggle" class="question" />
              <label for="question4">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABMCAYAAAAx3tSzAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABCFJREFUeNrsm09oFFcYwH9JBjY0YGSFHSJEUkjooT0UAwo9BFo8CBEqNOQQkcVDIQWhhxYPW2LxkDlVUCwUejFz6KEFIcccRMFApQElHoKKK4pKlg00YGDFhQntYb6VIXnzdt8m48y074Nld9/73sz7zfv+vJk3r6e0ME8X4gCjwDRwAWgA28C4/N5StBkBvgIuAoGUHRfdDYX+kOjPAf1SNiG6tZZSvVwx7ng30gc8jPwfkO914LAC2BGAn3aUPwHGFMAOUAKu7ShfBT6NAptKb5ftBjV1JxRlB4A/Y/QnFWUFgVPJNHuQuBEuAR+IKTXkigYdHrN/Hy5en0b/gKrQ9b1R4SkAm0CjXq5sththRw44BzwTs30h5jhgYO775TodDZLre0XgL+nvqvT5vOt7BR3wkJjLa+D8Dr0XwEdkUFzfG5a+FXdUXQIuur53TgVckCj6m+bY42RTRjTxoQJUXN9zosAF4GNNo5a8zChwtU39KLDo+t5MFPhem0aXJXhlUbaAT9roTAo4DvB7G+UJYNmgA01FmS7Cqy7ktkb/dfRPvVxpAGtAj+t7nwO3Ytpdcn3vdq+Ysw52U1H+VtOmFgPsGbhKAHwfo/9Uc+5N4EdN/Z2e0sL8PzGVU8ANTWoYkZmSKg83Y6aK6zF5WDUVLQJ/K8oPRQdBNbV0fe9X4GuTicfVNn4dyHRwAjgpgIH4+rbG1z4T/e1IbGhqTH0cOBXJ7Tr9qCybAleB5x0Ei2UD/24Ad+XTaSy4Lx9TWTOdSzfJt7wyBQ5yDvx2v++WcisW+L8u3dy2fSk5ciilPrdS2oqkzq2kgUeAK1m5O3R97029XAmSMOlh4FiGYAHqrZuCJICbpubznqSRFPAG8Aj4OUOwYxg+wezGh/8Alggf9KUlTbn4Gyb+2y3wMjkWm4c7kKK060u571uR29JEgSvAdxkZsA9d33uVVB4ekEA1myELfUbMSsR+ABcl0Q9kzC1LSQFvED5/upsx4PWkfLgpnxuED+2HU4QMZC5wE3iTdNC6nKXhNV0Qtw8A7MRjt8xIxC4a+v8DwlWGtbwBTwp0NzKVNrCJSQ8RrkPN7OF8c3nzYScFi0oNuCZ+eGYP55vKY9CqAl8YBq0AeIx66TXzwCs2D1tgC2yBLbAFtsAW2AJbYAtsgS2wBbbAFtgCW2ALbIEtcNLATs65Bk2BCzkHLpoCHwOO5hj4hCnwWdJ9h2OvEvtKsUP4cshJRd0i8A3h/qWlPFC6vneUcDl3Vgf8IAYY4BfguvhEE/2ew7QgWz/7xQ3j3hKsAad6SgvzhSyCJCBevVz5oZdw08SR/wHw7ZZJBzLcY6h3i+ZdAuCguOS7CUYgwWlMItws4XadvMtpoCabqnfNqALC1f2qpKTnwLc5Ba0SvhN6MwoL8O8AwUvuYBGaCTgAAAAASUVORK5CYII=" alt="FAQ icon" />
                <p>About the value and withdrawals processing</p>
              </label>

              <section id="answer4">
                <p>
                  Once you have at least 0.1 bitcoins in your balance you may make a withdrawl.
                </p>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@endsection
