@extends('layouts.newApp')

@section('content')
  <?php use Carbon\Carbon; ?>
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <div class="btn-group mr-1 mb-1">
                  <button aria-expanded="false" aria-haspopup="true" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton1" type="button">Exchanges</button>
                  <div aria-labelledby="dropdownMenuButton1" class="dropdown-menu">
                    @foreach($exchanges as $exchange)
                      <a class="dropdown-item" href="/stats/{{ $exchange->value }}">{{ $exchange->name }}</a>
                    @endforeach
                  </div>
                </div>
                {{-- <div class="row"> --}}
                  {{-- <div class="col"> --}}

                <br><br>


                <h6 class="element-header">
                  Traded Signals
                </h6>
                {{-- <div class="row"> --}}
                  {{-- <div class="col"> --}}
                    <div class="element-box el-tablo">
                      <table class="table">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Pair</th>
                        <th>Type</th>
                        <th>Buy Price</th>
                        <th>Target Sell Price</th>
                        <th>
                          Amount Wanted
                        </th>
                        <th>
                          Amount Purchased
                        </th>
                        <th>Final Sell Price</th>
                        <th>Profit</th>
                        <th>
                          Options
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 0; ?>
                    @foreach($trades as $trade)
                            <?php
                            $demo = $trade->demo;
                            $count++;
                            if(!$demo){
                              $currentProfit = round((($trade->signal->price/$trade->buy_price)-1)*100, 2);
                              $target = round((($trade->sell_price/$trade->buy_price)-1)*100, 2);
                              $percentageMilestone = round(($currentProfit/$target)*100);
                              if($percentageMilestone > 100){
                                  $percentageMilestone = 100;
                              }
                              if($trade->trade_open == false){
                                $buyPrice = number_format($trade->buy_price, 8);
                                $sellPrice = number_format($trade->final_sell_price, 8);
                                $actualProfit = round((($sellPrice/$buyPrice)-1)*100,2);
                                if($actualProfit < 0) $percentageMilestone = round(($actualProfit/15)*100)*-1;
                            }
                            } else {
                              $currentProfit = round((($trade->signal->price/$trade->signal->startingPrice)-1)*100, 2);
                              $target = 1;
                              $sell_price = $trade->signal->startingPrice * 1.01;
                              $percentageMilestone = round(($currentProfit/$target)*100);
                              if($percentageMilestone > 100){
                                  $percentageMilestone = 100;
                              }
                            }
                            ?>

                            <?php
                            $thisCarbon = Carbon::createFromTimestamp($trade->timestamp/1000);
                            $minutesElapsed = $thisCarbon->diffInMinutes(Carbon::now());
                            $thisCarbon->setTimeZone('America/Los_Angeles');
                            ?>
                            <tr>
                                <td class="work-status">
                                    @if($trade->trade_open == true)
                                        <span class="badge badge-warning">Pending Sell</span>
                                    @else
                                        <span class="badge badge-success">Cleared</span>
                                    @endif
                                </td>

                                <td class="subject">
                                    <div class="table-content">
                                        <p class="blue-grey-500">{{$trade->signal->pair}}</p>
                                        <span class="blue-grey-400">{{$thisCarbon->toDateTimeString() . ' PST'}}</span>
                                    </div>
                                </td>

                                <td class="work-status">
                                  @if($demo == true) <span class="badge badge-warning">Demo Trade</span>
                                  @else <span class="badge badge-success">Live Trade</span>
                                  @endif
                                </td>

                                <td>
                                    @if($trade->signal->startingPrice > 1)
                                      @if($demo) {{number_format($trade->signal->startingPrice, 2)}}
                                      @else {{number_format($trade->buy_price, 2)}}
                                      @endif
                                    @else
                                      @if($demo) {{number_format($trade->signal->startingPrice, 8)}}
                                      @else{{number_format($trade->buy_price, 8)}}
                                      @endif
                                    @endif
                                </td>

                                <td>
                                    @if($trade->signal->startingPrice > 1)
                                      @if($demo) {{ number_format($sell_price, 2) }}
                                      @else {{number_format($trade->sell_price, 2)}}
                                      @endif
                                    @else
                                      @if($demo) {{number_format($sell_price, 8)}}
                                      @else {{ number_format($trade->sell_price, 8) }}
                                      @endif
                                    @endif
                                </td>

                                <td>
                                  {{$trade->amount_ordered}}
                                </td>

                                <td>
                                  {{$trade->amount_fulfilled}}
                                </td>

                                <td>
                                    @if($trade->trade_open == false)
                                      {{ number_format($trade->final_sell_price, 8) }}
                                    @else
                                      Pending Sell
                                    @endif
                                </td>

                                @if(!$demo)
                                  <td class="work-progress">
                                      <div class="progress progress-xs table-content">
                                        @if($trade->trade_open == false && $actualProfit < 0)
                                          <div class="progress-bar progress-bar-indicating {{$trade->signal->exchange === 'Poloniex' && $currentProfit < $target ? $trade->signal->id : ''}} {{$currentProfit < $target ? $trade->signal->pair.' progress-bar-primary' : 'progress-bar bg-danger'}}" data-target="{{$target}}" data-starting-price="{{$trade->buy_price}}" style="width: {{ $percentageMilestone }}%" role="progressbar">
                                        @else
                                          <div class="progress-bar progress-bar-indicating {{$trade->signal->exchange === 'Poloniex' && $currentProfit < $target ? $trade->signal->id : ''}} {{$currentProfit < $target ? $trade->signal->pair.' progress-bar-primary' : 'progress-bar-success'}}" data-target="{{$target}}" data-starting-price="{{$trade->buy_price}}" style="width: {{ $percentageMilestone }}%" role="progressbar">
                                        @endif
                                              @if($trade->trade_open == false)
                                                {{-- <span class="sr-only">{{ $actualProfit }}%</span> --}}
                                              @else
                                                {{-- <span class="sr-only">{{ $currentProfit }}%</span> --}}
                                              @endif
                                          </div>
                                      </div>
                                      @if($trade->trade_open == true)
                                        {{-- <span class="text-{{$trade->signal->pair}} badge" style="color:black;">{{$currentProfit}}%</span> --}}
                                      @else
                                        <span class="text-{{$trade->signal->pair}}completed badge" style="color:black;">{{$actualProfit}}%</span>
                                      @endif
                                  </td>
                                @else
                                  <td class="work-progress">
                                      <div class="progress progress-xs table-content">
                                          <div class="progress-bar progress-bar-indicating {{$trade->signal->exchange === 'Poloniex' && $currentProfit < $target ? $trade->signal->id : ''}} {{$currentProfit < $target ? $trade->signal->pair.' progress-bar-primary' : 'progress-bar-success'}}" data-target="{{$target}}" data-starting-price="{{$trade->signal->startingPrice}}" style="width: {{ $percentageMilestone }}%" role="progressbar">
                                              <span class="sr-only">{{ $currentProfit }}%</span>
                                          </div>
                                      </div>
                                      @if($trade->trade_open == true)
                                        <span class="text-{{$trade->signal->pair}} badge" style="color:black;">{{$currentProfit}}%</span>
                                      @endif
                                  </td>
                                @endif

                                <td>
                                  @if($trade->trade_open)
                                    <a href="#" class="sell-trade btn btn-danger bg-green" data-sellorderid="{{ $trade->sell_order_id }}" data-exchange="{{$trade->exchange}}">
                                      Sell Now
                                        {{-- <i class="fa fa-thumbs-up" style="font-size:32px;color:#fff;"></i> --}}
                                    </a>
                                  @else
                                    Trade Closed
                                  @endif
                                </td>
                            </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="panel-footer">
                {{--<nav>--}}
                {{--<ul data-plugin="paginator" data-total="50" data-skin="pagination-gap" class="pagination pagination-gap"><li class="pagination-prev page-item disabled"><a class="page-link" href="javascript:void(0)" aria-label="Prev"><span class="icon md-chevron-left"></span></a></li><li class="pagination-items page-item active" data-value="1"><a class="page-link" href="javascript:void(0)">1</a></li><li class="pagination-items page-item" data-value="2"><a class="page-link" href="javascript:void(0)">2</a></li><li class="pagination-items page-item" data-value="3"><a class="page-link" href="javascript:void(0)">3</a></li><li class="pagination-items page-item" data-value="4"><a class="page-link" href="javascript:void(0)">4</a></li><li class="pagination-items page-item" data-value="5"><a class="page-link" href="javascript:void(0)">5</a></li><li class="pagination-next page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next"><span class="icon md-chevron-right"></span></a></li></ul>--}}
                {{--</nav>--}}
            </div>


                    </div>

                  {{-- </div> --}}
                {{-- </div> --}}
              </div>
            </div>
          </div>
          {{-- End Row --}}



        </div>
      </div>
    </div>
  </div>

@endsection
