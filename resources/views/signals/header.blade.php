@extends('layouts.app')

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
    @if(!$hasKey)
        <div class="col-xxl-12 col-lg-12">
            <div class="alert alert-danger" role="alert">
                <strong>Ops!</strong> You currently don't have a api key & secret set up for this exchange.
            </div>
        </div>
    @endif

    <div class="col-xxl-8 col-lg-12">
        <!-- Widget Statistic -->
        <div class="card card-shadow card-responsive" id="widgetOverallViews">
            <div class="card-block p-30">
                <div class="row pb-30" style="height:calc(100% - 250px);">
                    <div class="col-sm-4">
                        <div class="counter counter-md text-left">
                            <div class="counter-label">TOTAL SIGNALS</div>
                            <div class="counter-number-group text-truncate">
                                <span class="counter-number red-600">{{$totalSignals}}</span>
                            </div>
                            {{--<div class="counter-label">2% higher than last month</div>--}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="counter counter-sm text-left inline-block">
                            <div class="counter-label">SIGNALS TAKEN</div>
                            <div class="counter-number-group">
                                <span class="counter-number">{{$signalsTaken}}</span>
                            </div>
                        </div>
                        <div class="ct-chart inline-block small-bar-one"></div>
                    </div>
                    <div class="col-sm-4">
                        <div class="counter counter-sm text-left inline-block">
                            <div class="counter-label">SIGNALS COPIED</div>
                            <div class="counter-number-group">
                                <span class="counter-number">{{$signalsCopied}}</span>
                            </div>
                        </div>
                        <div class="ct-chart inline-block small-bar-two"></div>
                    </div>
                </div>
                <div class="ct-chart line-chart h-250" data-signals-copied="{{json_encode($signalsCopiedByDay)}}" data-signals-taken="{{json_encode($signalsTakenByDay)}}"></div>
            </div>
        </div>
        <!-- End Widget Statistic -->
    </div>

    <div class="col-xxl-4 col-lg-12">
        <div class="row h-full">
            <div class="col-xxl-12 col-lg-6 h-p50 h-only-lg-p100 h-only-xl-p100">
                <!-- Widget Linepoint -->
                <div class="card card-inverse card-shadow bg-blue-600 white" id="widgetLinepoint">
                    <div class="card-block p-0">
                        <div class="pt-25 px-30">
                            <div class="row no-space">
                                <div class="col-6">
                                    <p>Today's Profit</p>
                                    {{--<p class="blue-200">Last Signal 1.89 %</p>--}}
                                </div>
                                <div class="col-6 text-right">
                                    <p class="font-size-30 text-nowrap">{{$profitForTheDay}} %</p>
                                </div>
                            </div>
                        </div>
                        <div class="ct-chart h-120" id="dailyProfitChart" data-daily-profit="{{json_encode($profitForTheDayByHour)}}" ></div>
                    </div>
                </div>
                <!-- End Widget Linepoint -->
            </div>
            <div class="col-xxl-12 col-lg-6 h-p50 h-only-lg-p100 h-only-xl-p100">
                <!-- Widget Sale Bar -->
                <div class="card card-inverse card-shadow bg-purple-600 white" id="widgetSaleBar">
                    <div class="card-block p-0">
                        <div class="pt-25 px-30">
                            <div class="row no-space">
                                <div class="col-6">
                                    <p>Month Profit</p>
                                    {{--<p class="purple-200">2% higher than last month</p>--}}
                                </div>
                                <div class="col-6 text-right">
                                    <p class="font-size-30 text-nowrap">{{$profitForTheMonth}} %</p>
                                </div>
                            </div>
                        </div>
                        <div class="ct-chart h-120" id="monthProfitChart" data-month-profit="{{json_encode($profitForTheMonthByDay)}}"></div>
                    </div>
                </div>
                <!-- End Widget Sale Bar -->
            </div>
        </div>
    </div>
    @yield('traderContent')
        </div>
    </div>
@endsection
