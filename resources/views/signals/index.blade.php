@extends('layouts.newApp')

@section('content')

  <?php
    use Carbon\Carbon;
  ?>

  <div id="signalModal" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            Disclaimer
          </h5>
          {{-- <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button> --}}
        </div>
        <div class="modal-body">
          <h4 style="text-align:center;">These signals are to be used at your own risk</h4>
          <h5 style="text-align:center;">There are no guaranteed returns on any of the signals</h5>
          <h6 style="text-align:center;">The prices listed for entry and exit (buy and sell) are <b>REFERENCE</b> prices</h6>

            <h6 style="text-align:center;">For best results please only use the latest signals as the market does move quickly</h6>
            <h6 style="text-align:center;text-transform:initial;color:red !important;">
              ONLY USE THE SIGNALS FOR THE EXCHANGE THEY WERE CREATED FOR
            </h6>
            <h6 style="text-align:center;">Again this is not investment advice nor is it going to guarantee a return</h6>
            <h6 style="text-align:center;">All signals are back-tested on 5 years data (if it exists) on that trade pair for that exchange</h6>
            <h6 style="text-align:center;color:red !important;">Use the signals at your own risk.</h6>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" data-dismiss="modal" type="button">I Understand</button>
        </div>
      </div>
    </div>
  </div>

  <div id="signal_card_modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" name="title">
          </h5>
        </div>
      </div>
    </div>
  </div>

  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">

          <div class="row">
            <div class="col">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Signals ({{$exchange}} Only)
                </h6>
                @if(!$hasKey)
                    <div class="alert alert-danger" role="alert">
                        <strong>Oops!</strong> You currently don't have an Api Key & Secret set up for this exchange.
                    </div>
                @endif
                @if($isDemo)
                  <div class="alert alert-warning" role="alert">
                      <b>You are currently on demo mode.</b> No funds will be used if trades are executed. To change this, <a href="/settings"> click here </a>.</b>
                  </div>
                @else
                  <div class="alert alert-success" role="alert">
                      <b>Live trades ready to be taken! Demo mode is currently Off. Your funds will be used if trades are executed. To change this, <a href="/settings"> click here </a>.</b>
                  </div>
                @endif
                  <div class="row sorting-container" id="clients-grid-1" data-layout="masonry">
                      <?php $count = 0; ?>
                      @foreach($signals as $signal)
                          @if($signal->type === 'Buy' && $count < 240)

                            <?php
                            $count++;
                            $currentProfit = round((($signal->price/$signal->startingPrice)-1)*100, 2);
                            $target = round((($signal->longLow/$signal->startingPrice)-1)*100, 2);
                            $percentageMilestone = round(($currentProfit/$target)*100);
                            if($percentageMilestone > 100){
                                $percentageMilestone = 100;
                            }
                            ?>

                              <div class="col-md-4">
                                {{-- <a href="#" data-toggle="modal" data-target="signal_card_modal" data-card-id="#modal-{{ $signal->pair }}-{{ $signal->timestamp/1000 }}" style="color:black;text-decoration:none;"
                                  data-signal-pair="{{ $signal->pair }}" data-buy-price="{{number_format($signal->startingPrice, 8)}}" data-sell-price="{{number_format($signal->longLow, 8)}}" data-target-perc="{{ $target }}"> --}}
                                  <div class="element-box">

                                      <h4>{{$signal->pair}}</h4>
                                      @if($exchange === 'Bittrex')
                                          <img class="signal-logo" src="{{$signal->logo}}" />
                                      @endif
                                      <div class="c100 center p{{$percentageMilestone}}">
                                          <span>{{$currentProfit}}%</span>
                                          <div class="slice">
                                              <div class="bar"></div>
                                              <div class="fill"></div>
                                          </div>
                                      </div>
                                      <p class="webinar-date">Called At
                                        <?php
                                          $thisCarbon = Carbon::createFromTimestamp($signal->timestamp/1000);
                                          $thisCarbon->setTimeZone('America/Toronto');
                                          echo $thisCarbon->toDayDateTimeString() . ' EST';
                                          $minutesElapsed = $thisCarbon->diffInMinutes(Carbon::now());
                                        ?>
                                      </p>
                                      <p class="webinar-date">Enter {{$signal->pair}} target: <br>{{number_format($signal->startingPrice, 8)}}</p>
                                      <p class="webinar-date">Exit {{$signal->pair}} target: <br>{{number_format($signal->longLow, 8)}}</p>

                                      {{-- @if($minutesElapsed > 7) --}}
                                          {{-- <p style="color: red;">Over 7 Minutes Ago</p>
                                          <div class="row">
                                            <div class="col">
                                              <button class="btn btn-success" onclick="setPriceClipboard('{{ number_format($signal->startingPrice, 8) }}')" style="display:block; margin: 0 auto">Copy Enter Price</button>
                                            </div>
                                            <div class="col">
                                              <button class="btn btn-danger" onclick="setPriceClipboard('{{ number_format($signal->longLow, 8) }}')" style="display:block; margin: 0 auto">Copy Exit Price</button>                                        </div>
                                          </div> --}}
                                      @if(!$hasKey)
                                          <p style="color: red;">Api Keys Missing</p>
                                          <div class="row">
                                            <div class="col">
                                              <button class="btn btn-success" onclick="setPriceClipboard('{{ number_format($signal->startingPrice, 8) }}')" style="display:block; margin: 0 auto">Copy Enter Price</button>
                                            </div>
                                            <div class="col">
                                              <button class="btn btn-danger" onclick="setPriceClipboard('{{ number_format($signal->longLow, 8) }}')" style="display:block; margin: 0 auto">Copy Exit Price</button>                                        </div>
                                          </div>
                                      @elseif($hasKey && !isset($userTrades->{$signal->_id}))
                                          <p style="color: green;">Trade Available</p>
                                          <div class="row">
                                            <div style="margin: 0 auto">
                                              <a href="#" class="execute-trade btn btn-success bg-green" data-pair="{{$signal->pair}}" data-signalid="{{ $signal->_id }}" data-exchange="{{strtolower($exchange)}}">
                                                  <i class="fa fa-thumbs-up" style="font-size:32px;color:#fff;"></i>
                                              </a>

                                              {{-- <a href="#" class="btn btn-control bg-danger decline-trade">
                                                  <i class="fa fa-thumbs-down" style="font-size:32px;color:#fff;"></i>
                                              </a> --}}
                                              {{-- <p style="color:red">Instant-trading currently under maintenance.</p> --}}
                                            </div>
                                          </div>
                                      @endif
                                      <br>
                                      <div class="row">
                                          <div class="col text-center">
                                              Success Rate
                                          </div>
                                          <div class="col text-center">
                                              Average Gain
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col text-center">
                                              {{number_format(($signalStats->{$signal->pair}->successfull/$signalStats->{$signal->pair}->count)*100, 0)}} %
                                          </div>
                                          <div class="col text-center">
                                              <p style="color: {{$signalStats->{$signal->pair}->average > $target ? '#11c26d' : '#dc3545'}}">{{number_format($signalStats->{$signal->pair}->average, 2)}} % / {{ $target }} %</p>
                                          </div>
                                      </div>
                                  </div>
                                {{-- </a> --}}
                              </div>

                          @endif
                      @endforeach
                  </div>


            </div>

        </div>
      </div>
    </div>
  </div>
  </div>
  </div>

  <script>
    function setPriceClipboard(value) {
      var tempInput = document.createElement("input");
      tempInput.style = "position: absolute; left: -1000px; top: -1000px";
      tempInput.value = value;
      document.body.appendChild(tempInput);
      tempInput.select();
      document.execCommand("copy");
      document.body.removeChild(tempInput);
      alert("Price Copied to Clipboard");
    }
  </script>

@endsection
