@extends('layouts.front')

@section('content')
  <div class="all-wrapper">
      <div class="fade1"></div>
      <div class="desktop-menu menu-top-w menu-activated-on-hover">
          <div class="menu-top-i os-container">
              <div class="logo-w">
                <a href="/">
                  <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
                </a>
              </div>
              <ul class="main-menu">
                <li class="active">
                    <a href="#">User Agreement</a>
                </li>
                <li>
                    <a href="/privacy_policy">Privacy Policy</a>
                </li>
                <li>
                    <a href="/affiliate_agreement">Affiliate Agreement</a>
                </li>
                @if(Auth::check())
                  <li>
                      <a href="/home">Dashboard</a>
                  </li>
                @endif
              </ul>
              <ul class="small-menu">
                  @if(Auth::check())
                    <li class="separate">
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @else
                    <li class="separate">
                        <a href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @endif
              </ul>
          </div>
          <div class="mobile-menu-w">
              <div class="mobile-menu-holder color-scheme-dark">
                  <ul class="mobile-menu">
                      <li>
                          <a href="/">Home</a>
                      </li>
                      <li class="active">
                          <a href="#">User Agreement</a>
                      </li>
                      <li>
                          <a href="/privacy_policy">Privacy Policy</a>
                      </li>
                      <li>
                          <a href="/affiliate_agreement">Affiliate Agreement</a>
                      </li>
                      @if(Auth::check())
                        <li>
                            <a href="/home">Dashboard</a>
                        </li>
                      @endif
                      <li>
                          <a href="{{ route('login') }}">Sign Up</a>
                      </li>
                      <li>
                          <a href="{{ route('login') }}">Login</a>
                      </li>
                  </ul>
              </div>
              <div class="mobile-menu-i">
                  <div class="mobile-logo-w">
                      <img src="/img/ttraderlogo.png" width="200px"></img>
                  </div>
                  <div class="mobile-menu-trigger">
                      <i class="os-icon os-icon-hamburger-menu-1"></i>
                  </div>
              </div>
          </div>
      </div>

    <div class="container">
        <h1 style="padding-top:50px;"> Trio Trader User Agreement </h1> <br>
        <h5>LAST UPDATE OF THIS USER AGREEMENT – November 2nd, 2018</h5>
                                <br><br>
        <h4>BEFORE USING THIS SITE, PLEASE READ THIS AGREEMENT CAREFULLY.</h4>
                                <br>
        <h5>1.<span style="padding-left:40px;"></span>Introduction</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  Use of this website and the services provided via it TrioTrader.com are conditional upon you accepting the following Terms and Conditions.  Unless otherwise specified, your acceptance of this User Agreement shall be indicated by your use of and/or registration with TrioTrader.com.  Trio Trader is provided by Trio Software Inc. of Canada, Ontario, and its suppliers ("we"/"our"/"us").
          These Terms and Conditions, our Privacy Policy and Affiliate Agreement, (together the “User Agreement”) form our entire agreement with you in respect of chargeable use of TrioTrader.com and supersede any prior agreement or arrangement with you in respect of TrioTrader.com.  If a company name is inserted in your registration request, then the User Agreement shall be between us and that company and accordingly unless the context otherwise requires, references to "you" and "your" in this User Agreement shall also be to that company. If there are any updates to this User Agreement, we will bring this to your attention on the home page of TrioTrader.com.
        </p>

        <h5>2.<span style="padding-left:40px;"></span>Availability of TrioTrader.com</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  We will endeavour to ensure that TrioTrader.com is available 24 hours per day without any interruptions.  However, we reserve the right to make Trio Trader unavailable at any time or to restrict access to parts or all of Trio Trader without notice.  Trio Trader is a general information service.  We will endeavour not to make it misleading, but we  cannot represent that the information accessible on or via TrioTrader.com is completely accurate, not-misleading, or up to date as information is compiled from different sources. As new information is compiled updates will be sent out to keep information as up to date as possible.
        </p>

        <h5>3.<span style="padding-left:40px;"></span>Use of TrioTrader.com</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  TrioTrader.com is designed for your personal, non-commercial use and you must not use it in any other way without our consent.  Except as permitted under applicable law, you must not use, copy, translate, publish, licence or sell TrioTrader.com or any materials or information in TrioTrader.com or the structure, overall style and program code of TrioTrader.com without our consent.  If you wish to make a request for consent, please contact sphoun@trio.software.
        </p>

        <h5>4.<span style="padding-left:40px;"></span>Your Contributions</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>You agree to only use TrioTrader.com for lawful purposes and that any information that you provide in connection with, or which forms part of, TrioTrader.com will be, as far as you are aware, true and accurate and will not infringe any copyright or trade mark, or any right of privacy, publicity or personality or any other right, whether registered or unregistered, of any other nature or any person, or be obscene or libellous or blasphemous or defamatory and you agree to indemnify us against all claims, proceedings, damages, liabilities and costs, including legal costs arising out of your breach of this term.  We cannot make any assurances about the information or contribution made by any other user and you should exercise caution before acting or otherwise relying upon any information you obtain via the TrioTrader.com. By way of example, and not as a limitation, you agree to not use the website and the services provided via it:
          To abuse, harass, threaten, impersonate or intimidate any person; To post or transmit, or cause to be posted or transmitted, any Content that is libellous, defamatory, obscene, pornographic, abusive, offensive, profane, or that infringes any copyright or other right of any person; For any purpose (including posting or viewing Content) that is not permitted under the laws of the jurisdiction where you use the services; To post or transmit, or cause to be posted or transmitted, any communication or solicitation designed or intended to obtain password, account, or private information from any TrioTrader.com user; To create or transmit unwanted ‘spam’ to any person or any URL; To post copyrighted Content which doesn’t belong to you, with exception of articles, where you may post such Contect with explicit mention of the author’s name and link to the source of the Content unless it is prohibited by these Terms; You will not use any robot, spider, scraper or other automated means to access the website for any purpose without our express written permission. Additionally, you agree that you will not: (i) take any action that imposes, or may impose in our sole discretion an unreasonable or disproportionately large load on our infrastructure; (ii) interfere or attempt to interfere with the proper working of the website or any activities conducted on the website; or (iii) bypass any measures we may use to prevent or restrict access to the website; To advertise to, or solicit any user to buy or sell any external products or services, or to use any information obtained from the website and the services provided in order to contact, advertise to, solicit, or sell to any user without their prior explicit consent; To promote or sell Content of another person.
        </p>

        <h5>5.<span style="padding-left:40px;"></span>Links</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  TrioTrader.com may include links to other internet sites. Without limiting what we say elsewhere, we make no representations or warranties about those sites or their content, nor that the links work.  If you wish to link to TrioTrader.com you may only do so at www. TrioTrader.com or www.WorldCryptoCurrency101.com.  Details of our linking arrangements may be obtained from support@TrioTrader.com.
        </p>

        <h5>6.<span style="padding-left:40px;"></span>Data Protection</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  It is your responsibility to ensure that that you give us an accurate and valid e-mail address and other contact details and tell us of any changes to them, however we cannot make any assurances about any other user you may meet using the TrioTrader.com.  We comply with all applicable Data Protection laws in Canada.  For a description of how we use your personal data, please see our Privacy Policy.
        </p>

        <h5>7.<span style="padding-left:40px;"></span>Intellectual property</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>  TrioTrader.com, its style and structure, and the materials and information on TrioTrader.com of TrioTrader.com are protected by copyright and other intellectual property rights, and may not be used by you except as expressly provided in this User Agreement.  The authors of the documents in TrioTrader.com assert their moral rights. TrioTrader.com and Trio Software Inc. are registered trade marks of Trio Software Inc.
        </p>

        <h5>8.<span style="padding-left:40px;"></span>Our Liability</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>Since a substantial part of TrioTrader.com is education based, it is a condition that your use of TrioTrader.com and the information provided is at your own risk.  We shall not be liable to you or in breach of this User Agreement for any delay or failure to perform any obligation if the delay or failure is due to a cause beyond our reasonable control including, without limitation, the blocking or restricting of information to and/or from our network.
          Except as expressly provided in this User agreement, we disclaim any further representations, warranties, conditions or other terms, express or implied, by statute, collaterally or otherwise, including but not limited to implied warranties, conditions or other terms of satisfactory quality, fitness for a particular purpose or reasonable care and skill.
          Save as provided below, we disclaim all and will not be liable in contract, tort (including, without limitation, negligence) or otherwise arising in connection with this User Agreement or TrioTrader.com for: (i) consequential, indirect or special loss or damage; or (ii) any loss of goodwill or reputation; or (iii) any economic losses (including loss of revenues, profits, contracts, business or anticipated savings), in each case, even if we have been advised of the possibility of such loss or damage and howsoever incurred.
          Our maximum liability to you in contract, tort (including, without limitation, negligence) or otherwise arising in connection with this User Agreement or the TrioTrader.com shall be limited to $20.  Notwithstanding any other provision of this User Agreement, we will be liable to you without limit for any death or personal injury caused by our negligence and to the extent that liability arises under Part 1 or section 41 of the Consumer Protection Act 1987 and for liability arising from statements made fraudulently by us.
        </p>

        <h5>9.<span style="padding-left:40px;"></span>Small Print</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>Upon violation of the contract on either side, the opposing side reserves the right to terminate the agreement.  You may not transfer any of your rights or delegate any of your obligations under this User Agreement without our prior written consent.  If we fail to enforce any provision of this User Agreement, that failure will not preclude us from enforcing either that provision (or any similar provision) on a later occasion.  Nothing in this User Agreement shall confer on any third party any benefit or the right to enforce any term of the User Agreement. This User Agreement is governed by Canadian law and any dispute connected with this agreement is subject to the exclusive jurisdiction of the Canadian courts.  Nothing in this User Agreement affects your statutory rights as a consumer.
        </p>

        <h5>10.<span style="padding-left:40px;"></span>Complaints</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span> If you believe that your intellectual property or other rights are being infringed by TrioTrader.com, or if you are dissatisfied with TrioTrader.com or any aspect of our service, in the first instance please contact support@TrioTrader.com.
        </p>

    </div>
    {{-- End container --}}

    <div class="footer-w">
        <div class="fade3"></div>
        <div class="os-container">
            <div class="footer-i">
                <div class="row">
                    <div class="col-sm-7 col-lg-4 b-r padded">
                        <img src="/img/ttraderlogo.png" style="max-width:100%">
                        <h6 class="heading-small">
                            One click trading and all of your crypto education in one spot!
                        </h6>
                        <p>
                            Real News, Real Traders, Real Signals, Real Information!
                        </p>
                    </div>
                    <div class="col-sm-5 col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Location
                                </h6>
                                <p>
                                  1 Riverside Drive W <br/>Windsor Ontario, CA
                                </p>
                            </div>
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Email
                                </h6>
                                <ul>
                                    <li>
                                        support@trio.software
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deep-footer">
                Use of this site constitutes acceptance of our <a href="/user_agreement">User Agreement</a> and <a href="/privacy_policy">Privacy Policy</a>.<br>
                &copy; 2018 All rights reserved | Trio Trader | Trio Software Inc.
            </div>
        </div>
    </div>

  </div>
  {{-- End all-wrapper --}}
  {{-- Google Translate --}}
  <script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@endsection
