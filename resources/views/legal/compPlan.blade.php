@extends('layouts.front')

@section('content')
  <div class="all-wrapper">
      <div class="fade1"></div>
      <div class="desktop-menu menu-top-w menu-activated-on-hover">
          <div class="menu-top-i os-container">
              <div class="logo-w">
                  <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
              </div>
              <ul class="main-menu">
                  <li>
                      <a href="https://www.triotrader.com/#sectionIntro">Home</a>
                  </li>
                  <li>
                      <a href="https://www.triotrader.com/#sectionFeatures">Features</a>
                  </li>
                  <li>
                      <a href="https://www.triotrader.com/#sectionTestimonials">Philosophy</a>
                  </li>
                  <li class="active">
                    <a href="#">Comp Plan</a>
                  </li>
                  @if(Auth::check())
                    <li>
                        <a href="/home">Dashboard</a>
                    </li>
                  @endif
              </ul>
              <ul class="small-menu">
                  @if(Auth::check())
                    <li class="separate">
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @else
                    <li class="separate">
                        <a href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @endif
              </ul>
          </div>
          <div class="mobile-menu-w">
              <div class="mobile-menu-holder color-scheme-dark">
                  <ul class="mobile-menu">
                      <li>
                          <a href="https://www.triotrader.com/#sectionIntro">Home</a>
                      </li>
                      <li>
                          <a href="https://www.triotrader.com/#sectionFeatures">Features</a>
                      </li>
                      <li>
                          <a href="https://www.triotrader.com/#sectionTestimonials">Philosophy</a>
                      </li>
                      <li class="active">
                        <a href="#">Comp Plan</a>
                      </li>
                      @if(Auth::check())
                        <li>
                            <a href="/home">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}">Logout</a>
                        </li>
                      @else
                        <li>
                            <a href="#sectionPackages" class="highlight">Sign Up</a>
                        </li>
                        <li>
                            <a href="{{ route('login') }}">Login</a>
                        </li>
                      @endif
                  </ul>
              </div>
              <div class="mobile-menu-i">
                  <div class="mobile-logo-w">
                      <img src="/img/ttraderlogo.png" width="200px"></img>
                  </div>
                  <div class="mobile-menu-trigger">
                      <i class="os-icon os-icon-hamburger-menu-1"></i>
                  </div>
              </div>
          </div>
      </div>

    <div class="container">
      <h1 style="padding-top:50px;"> Affiliate Compensation Plan </h1> <br>
      <h5>LAST UPDATE OF THIS COMPENSATION PLAN – November 2nd, 2018</h5>

        {{--<span style="padding-left:20px;"></span>To get started with WCC101, you can either purchase a semi-annual $477 Membership or pay the annual Affiliate Fee--}}
        {{--of $97--}}

      <h5>Direct Commissions</h5>
      <p style="padding-top:10px;">
        For the purpose of this document, the approximation amount of compensation ($100) is based off of the semi-annual package currently listed at $600 USD of BTC.
        <span style="padding-left:20px;"></span>For each CUSTOMER recruited directly under the monthly package, you will receive approximately $10 worth of BTC.<br>
        <span style="padding-left:20px;"></span>For each CUSTOMER recruited directly under the semi annual, you will receive approximately $100 worth of BTC.<br>
        <span style="padding-left:20px;"></span>For each CUSTOMER recruited directly under the annual package, you will receive approximately $150 worth of BTC.<br>
      </p>

      <h5>Team Commissions</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>Once you have made your first subscription Sales Volume is tracked in your business center in two distinct groupsto as your “Left and Right” groups. <br>
      </p>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>To earn the Team Commissions, you must qualify by selling at least 3 Membership sales, of which 1 may be for yourself and a minimum 1 Personal Sale in your left and right teams. So you can either buy 1 and sell one left and right OR sell 3 with a minimum of 1 personal sale on each side. Once you are qualified, you earn approximately $105 worth of Bitcoin for every Membership sale in the team side with fewer sales (semi-annual package). The target amount is $100 although it does vary slightly because the relationship between USD and BTC fluctuates.<br>
      </p>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>The initial daily maximum commission is $3,000 USD worth of Bitcoin. Once you have 5 Personal Sales on your left team and 5 Personal Sales on your right team, the daily maximum commission is raised to $4,500 USD worth of Bitcoin.
          Additionally, there are 3 levels of matching bonus available. The first level match is 20% of the Team Commissions of all of your personally sponsored affiliates regardless of where they are in the genealogy. To qualify for this first level of match of 20%, you need to sponsor at least 1 affiliate left and right that are both qualified.
          The second level of match is also 20% on all of the team commissions of the affiliates that your personals sponsor. To qualify for the second level of match, you need to personally sponsor 2 affiliates right and left that are qualified.
          The third level of match is 10% of all of the team commissions from the third generation of affiliates. To qualify for the third level of match, you need to personally sponsor 3 affiliates right and left that are qualified.
          Remember, everyone can sponsor as many customers and affiliates as possible. The number of personals you can sponsor is unlimited, so the 3 levels of check match is also unlimited.  Keep in mind that the check matches are based on Team Commissions and not inclusive of their Match amounts
      </p>
      <table>
        <tr>
          <th>Subscription</th>
          <th>USD Price paid in BTC</th>
          <th>Point Value</th>
          <th>Direct Commission paid in BTC</th>
        </tr>
        <tr>
          <td>Monthly</td>
          <td>$100</td>
          <td>1</td>
          <td>$10</td>
        </tr>
        <tr>
          <td>Semiannual</td>
          <td>$600</td>
          <td>6</td>
          <td>$100</td>
        </tr>
        <tr>
          <td>Annual</td>
          <td>$1000</td>
          <td>10</td>
          <td>$150</td>
        </tr>
      </table>

      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>When a subscription is sold, the point(s) are accrued in the group side where the subscription originates. As the points accumulate they are matched 1:1 with the left and right group totals. This is known as a cycle. The value of each cycle is $15.   The target amount is $15 although it does vary slightly because the relationship between USD and BTC fluctuates.
      </p>


      <h5>Definitions</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>1. <b>ACCRUE:</b> Points can begin to accumulate as soon as you have made your first Membership sale. Sales volume is
        stored until matched by a sale in your smaller team. Sales volume in either team can accrue up to a maximum 45 sales.<br>
        <span style="padding-left:20px;"></span>2. <b>ACTIVE:</b> Any Affiliate or member that generates at least 1 sale in the last 180 days<br>
        <span style="padding-left:20px;"></span>3. <b>AFFILIATE:</b> Any person that chooses to participate in the Trio Trader Compensation and has received the company’s
        Terms and Conditions, and agrees to the AFFILIATE agreement.<br>
        <span style="padding-left:20px;"></span>4. <b>BUSINESS CENTER:</b>  This is the Affiliates’ Account in the Network/Genealogy that is used to track sales and
        commissions.<br>
        <span style="padding-left:20px;"></span>5. <b>CYCLE:</b> When a person has qualified as an Affiliate, the commission generated per sale on the smaller team side is
        approximately $15 USD worth of Bitcoin. The sales volume of 1 Membership sale per team side is deducted to
        generate the commission. This can happen a maximum of 45 times per day. Unused sales volume remains
        available for active Affiliates.<br>
        <span style="padding-left:20px;"></span>6. <b>CUSTOMER:</b> Anyone that purchases a $577 Pro Package.<br>
        <span style="padding-left:20px;"></span>7. <b>MATCH:</b> There are 3 levels of generational check matching available. A check match or “match” is based on the Team commissions earned starting with the affiliates that you sponsor. The affiliates that you sponsor can be placed anywhere in your genealogy but its is necessary to qualify with at least 1 personally sponsored and qualified affiliate on your left and right teams, for which you can receive an amount equal to 20% of their team commission that they earn. The 2nd generation match is also 20% and is available when you sponsor 2 qualified affiliates on your team and right teams. The 3rd level of match is available when you sponsor 3 qualified affiliates on your right and left. That match is 10% on your 3rd generation of affiliates anywhere in your genealogy. Remember that the match amounts are based on Team Commissions and are unlimited.<br>
        <span style="padding-left:20px;"></span>8. <b>NETWORK / GENEALOGY:</b> This is a series of relationships of Business Centers that determine the flow of shared
        sales volume and commissions. A maximum of two Business Centers can be placed directly to any given Affiliate
        below you on your Binary Team: one on the right side, and one on the left side.  Thereafter, each one of them can
        have two other Business Centers attached. Your Binary Network can go to an unlimited number of Affiliates in
        depth.<br>
        <span style="padding-left:20px;"></span>9. <b>PAY PERIOD:</b>  Commissions are calculated and paid daily at midnight EST.  Commissions are paid in Bitcoin. Be sure to add your cryptocurrency wallet to receive commissions.
      </p>
      <br><br>
      <img style="display:block; margin: 0 auto" src="/img/Comp_Graphic_1.png">
      <br>
      <img style="display:block; margin: 0 auto" src="/img/new-comp-graphic-match-final.png">
    </div>
    {{-- End container --}}

    <div class="footer-w">
        <div class="fade3"></div>
        <div class="os-container">
            <div class="footer-i">
                <div class="row">
                  <div class="col-sm-7 col-lg-4 b-r padded">
                      <img src="/img/ttraderlogo.png" style="max-width:100%">
                      <h6 class="heading-small">
                          One click trading and all of your crypto education in one spot!
                      </h6>
                      <p>
                          Real News, Real Traders, Real Signals, Real Information!
                      </p>
                  </div>
                    <div class="col-sm-5 col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Location
                                </h6>
                                <p>
                                    1 Riverside Drive W <br/>Windsor Ontario, CA
                                </p>
                            </div>
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Email
                                </h6>
                                <ul>
                                    <li>
                                        support@trio.software
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deep-footer">
                Use of this site constitutes acceptance of our <a href="/user_agreement">User Agreement</a> and <a href="/privacy_policy">Privacy Policy</a>.<br>
                &copy; 2018 All rights reserved | Trio Trader | Trio Software Inc.
            </div>
        </div>
    </div>
  </div>
  {{-- End all-wraper --}}
  {{-- Google Translate --}}
  <script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@endsection
