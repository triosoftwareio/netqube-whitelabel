@extends('layouts.front')

@section('content')
  <div class="all-wrapper">
      <div class="fade1"></div>
      <div class="desktop-menu menu-top-w menu-activated-on-hover">
          <div class="menu-top-i os-container">
              <div class="logo-w">
                <a href="/">
                  <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
                </a>
              </div>
              <ul class="main-menu">
                <li>
                    <a href="/user_agreement">User Agreement</a>
                </li>
                <li>
                    <a href="/affiliate_agreement">Affiliate Agreement</a>
                </li>
                <li class="active">
                    <a href="#">Privacy Policy</a>
                </li>
                @if(Auth::check())
                  <li>
                      <a href="/home">Dashboard</a>
                  </li>
                @endif
              </ul>
              <ul class="small-menu">
                  @if(Auth::check())
                    <li class="separate">
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @else
                    <li class="separate">
                        <a href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @endif
              </ul>
          </div>
          <div class="mobile-menu-w">
              <div class="mobile-menu-holder color-scheme-dark">
                  <ul class="mobile-menu">
                      <li>
                          <a href="/">Home</a>
                      </li>
                      <li>
                          <a href="/user_agreement">User Agreement</a>
                      </li>
                      <li>
                          <a href="/affiliate_agreement">Affiliate Agreement</a>
                      </li>
                      <li class="active">
                          <a href="#">Privacy Policy</a>
                      </li>
                      @if(Auth::check())
                        <li>
                            <a href="/home">Dashboard</a>
                        </li>
                      @endif
                      <li>
                          <a href="{{ route('login') }}">Sign Up</a>
                      </li>
                      <li>
                          <a href="{{ route('login') }}">Login</a>
                      </li>
                  </ul>
              </div>
              <div class="mobile-menu-i">
                  <div class="mobile-logo-w">
                      <img src="/img/ttraderlogo.png" width="200px"></img>
                  </div>
                  <div class="mobile-menu-trigger">
                      <i class="os-icon os-icon-hamburger-menu-1"></i>
                  </div>
              </div>
          </div>
      </div>

    <div class="container">
        <h1 style="padding-top:50px;"> Privacy Policy </h1> <br>
        <h5>LAST UPDATE OF THIS PRIVACY POLICY – November 2nd, 2018</h5>
                                <br><br>
        <h4>
          BEFORE USING OUR SITES, PLEASE READ THIS PRIVACY POLICY CAREFULLY.
        </h4>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>This Privacy Policy is applicable to Trio Software Inc. a Windsor Corporation ("Trio Software INC.") and sets out our policy on the gathering and use of information on this site and our other sites (collectively "Sites"). The Company is committed to providing safe web sites for visitors of all ages and has implemented this Privacy Policy to demonstrate our firm commitment to your privacy. The Company complies with Canadian Federal and Provincial privacy laws and regulations including the Personal Information and Electronic Documents Act. <br>
          <span style="padding-left:20px;"></span>There may be links from our Sites to other web sites; note that this Privacy Policy applies only to our Sites and not to web sites of other companies or organizations to which our Sites may be linked. You must check on any linked sites for the privacy policy that applies to that site and/or make any necessary inquiries in respect of that privacy policy with the operator of the linked site. These links to third party websites are provided as a convenience and are for informational purposes only. The Company does not endorse, and is not responsible for, these linked websites. <br>
          <span style="padding-left:20px;"></span>Although you are not required to register to access some of our Sites, you may be asked to provide us with personal information when you visit certain sections of our Sites. Your use of our Sites signifies your acknowledgement and consent to our Privacy Policy. If you do not agree to this Privacy Policy, please do not continue to use our Sites. Your continued use of the Sites signifies your acceptance of these terms and any changes in effect at the time of use.
        </p>

        <h5>COLLECTION OF PERSONAL INFORMATION</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>Personal Information is information about you that identifies you as an individual, for example, your name, address, e-mail address, or telephone number.<br>
          <span style="padding-left:20px;"></span>We collect information that you voluntarily provide to us through responses to surveys, search functions, questionnaires, feedback, Tell Your Story forms and the like. We may also ask you to provide additional information such as your e-mail address or payment information which will be safely stored and not used unless with your consent if you want to obtain additional services, information or participate in a contest or to resolve complaints or concerns.
        </p>

        <h5>HOW DOES COMPANY USE INFORMATION GATHERED ABOUT ONLINE VISITORS?</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>Before forwarding us any personal information, please be advised that any information gathered on our Sites may be used in the aggregate for research and development relating to our Sites and/or for future site development and, if you ask us to, to send you promotional materials. In particular, we may use information gathered about you for the following purposes: to monitor interest in our range of products and to assist us to tailor the content of our Sites to your needs by collecting information about your preferences through tracking of patterns page views on our Sites; to create a profile relating to you in order to show you the content that might be of interest to you and to display the content according to your preferences; and, in circumstances where you have indicated that you wish to receive additional information, to send you information about us and promotional material about our products together with details of any offers we may have available from time to time.
        </p>

        <h5>PROMOTIONAL AND INFORMATIONAL OFFERS</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>With the permission of an online visitor, information submitted at the time of registration or submission may be used for marketing and promotional purposes by the Company provided notice of this fact is made available online. If a visitor objects to such use for any reason, he/she may prevent that use, either by e-mail request or by modifying the registration information provided. The Company uses reasonable efforts to maintain visitors' information in a secure environment. If you have submitted personal information and want to change it or opt-out, please contact us as described below.
        </p>

        <h5>DISCLOSURE OF INFORMATION</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>The Company will not disclose personal information that you provide on its Sites to any third parties other than to a Company agent except: i) in accordance with the terms of this Privacy Policy, or ii) to comply with legal requirements such as a law, regulation, warrant, subpoena or court order, and/or iii) if you are reporting an adverse event/side effect, in which case the Company may be required to disclose such information to bodies such as, but not limited to, Canadian and/or international regulatory authorities. Please note that any of these disclosures may involve the storage or processing of personal information outside of Canada and may therefore be subject to different privacy laws than those applicable in Canada, including laws that require the disclosure of personal information to governmental authorities under circumstances that are different than those that apply in Canada.
        </p>

        <h5>COOKIES</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>    The Company, in common with many web site operators, may use standard technology called "cookies" on its Sites. Cookies are small data files that are downloaded onto your computer when you visit a particular web site. You can disable cookies by turning them off in your browser; however, some areas of the Sites may not function properly if you do so.
        </p>

        <h5>PROTECTION OF CHILDREN ONLINE</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>The Company considers the protection of children's privacy, especially online, to be of the utmost importance. We do not knowingly collect or solicit personal information from children nor do we allow them to become registered users of, or to request information through, our Sites or help-seeking information lines.
        </p>


        <h5>ADDITIONAL TERMS FOR CERTAIN WEBSITES</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>The following additional information applies to our Sites that require registration. Generally, you are not required to provide personal information as a condition of using our Sites, except as may be necessary to provide you with a product or service that you have requested. However, some of our Sites are restricted to certain individuals such as those above the age of 18 or only those with a credit card in their name and so we may require these individuals to register upon entry by providing us with certain information.
        </p>

        <h5>PROTECTION OF INFORMATION:</h5><br>
        <h6>Our Commitment to Security</h6>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>We have put in place physical, electronic, and managerial procedures to safeguard and help prevent unauthorized access, maintain data security, and correctly use the information we collect online. The Company applies security safeguards appropriate to the sensitivity of the information, such as retaining information in secure facilities and making personal information accessible only to authorized employees on a need-to-know basis.
        </p>
        <h6>Storage of Information</h6>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>Personal information you share with us is stored on our database servers at Company data centers (in whatever country they may be located), or hosted by third parties who have entered into agreements with us that require them to observe our Privacy Policy.
        </p>

        <h5>POLICY CHANGE:</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>If we alter our Privacy Policy, any changes will be posted on the home page of our Site so that you are always informed of the information we collect about you, how we use it and the circumstances under which we may disclose it.
        </p>

        <h5>ACCEPTANCE OF OUR PRIVACY POLICY:</h5>
        <p style="padding-top:10px;">
          <span style="padding-left:20px;"></span>By using this Site or any other The Company Site or interactive banner ads, you signify your acceptance of our Privacy Policy, and you adhere to the terms and conditions posted on the Site. By submitting your information, you agree that it will be governed by our Privacy Policy.
        </p>

    </div>
    {{-- End container --}}

    <div class="footer-w">
        <div class="fade3"></div>
        <div class="os-container">
            <div class="footer-i">
                <div class="row">
                  <div class="col-sm-7 col-lg-4 b-r padded">
                      <img src="/img/ttraderlogo.png" style="max-width:100%">
                      <h6 class="heading-small">
                          One click trading and all of your crypto education in one spot!
                      </h6>
                      <p>
                          Real News, Real Traders, Real Signals, Real Information!
                      </p>
                  </div>
                    <div class="col-sm-5 col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Location
                                </h6>
                                <p>
                                  1 Riverside Drive W <br/>Windsor Ontario, CA
                                </p>
                            </div>
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Email
                                </h6>
                                <ul>
                                    <li>
                                        support@trio.software
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deep-footer">
                Use of this site constitutes acceptance of our <a href="/user_agreement">User Agreement</a> and <a href="/privacy_policy">Privacy Policy</a>.<br>
                &copy; 2018 All rights reserved | Trio Trader | Trio Software Inc.
            </div>
        </div>
    </div>
  </div>
  {{-- End all-wraper --}}
  {{-- Google Translate --}}
  <script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@endsection
