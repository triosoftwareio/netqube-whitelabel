@extends('layouts.front')

@section('content')
  <div class="all-wrapper">
      <div class="fade1"></div>
      <div class="desktop-menu menu-top-w menu-activated-on-hover">
          <div class="menu-top-i os-container">
              <div class="logo-w">
                <a href="/">
                  <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
                </a>
              </div>
              <ul class="main-menu">
                <li>
                    <a href="/user_agreement">User Agreement</a>
                </li>
                <li>
                    <a href="/privacy_policy">Privacy Policy</a>
                </li>
                <li class="active">
                  <a href="#">Affiliate Agreement </a>
                </li>
                @if(Auth::check())
                  <li>
                      <a href="/home">Dashboard</a>
                  </li>
                @endif
              </ul>
              <ul class="small-menu">
                  @if(Auth::check())
                    <li class="separate">
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @else
                    <li class="separate">
                        <a href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="separate">
                      <div id="google_translate_element"></div>
                    </li>
                  @endif
              </ul>
          </div>
          <div class="mobile-menu-w">
              <div class="mobile-menu-holder color-scheme-dark">
                  <ul class="mobile-menu">
                      <li>
                          <a href="/">Home</a>
                      </li>
                      <li>
                          <a href="/user_agreement">User Agreement</a>
                      </li>
                      <li class="active">
                          <a href="/privacy_policy">Privacy Policy</a>
                      </li>
                      <li class="active">
                        <a href="#">Affiliate Agreement </a>
                      </li>
                      @if(Auth::check())
                        <li>
                            <a href="/home">Dashboard</a>
                        </li>
                      @endif
                      <li>
                          <a href="{{ route('login') }}">Sign Up</a>
                      </li>
                      <li>
                          <a href="{{ route('login') }}">Login</a>
                      </li>
                  </ul>
              </div>
              <div class="mobile-menu-i">
                  <div class="mobile-logo-w">
                      <img src="/img/ttraderlogo.png" width="200px"></img>
                  </div>
                  <div class="mobile-menu-trigger">
                      <i class="os-icon os-icon-hamburger-menu-1"></i>
                  </div>
              </div>
          </div>
      </div>

    <div class="container">
      <h1 style="padding-top:50px;"> Trio Trader Affiliate Agreement </h1> <br>
      <h5>LAST UPDATE OF THIS AFFILIATE AGREEMENT – May 14th, 2018</h5>

      <h4>Terms and Conditions</h4>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>The following Trio Trader (“Trio Trader”) Affiliate Agreement is a legal agreement
        between you and Trio Trader. Trio Trader IS WILLING TO EXTEND TO YOU THE RIGHTS AND BENEFITS
        OUTLINED IN THIS AGREEMENT ONLY UPON THE CONDITION THAT YOU ACCEPT ALL OF THE
        TERMS CONTAINED IN THIS AGREEMENT. IN ORDER TO COMPLETE THE APPLICATION PROCESS,
        YOU MUST INDICATE THAT YOU HAVE READ, UNDERSTAND AND AGREE TO ABIDE BY THESE
        DOCUMENTS.
      </p>

      <h5>1.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>  I understand that as a Trio Trader Affiliate I do not have the right to earn commissions until I have satisfied the sales requirements set forth in the Trio Trader Compensation Plan and am engaged by Trio Trader to be an Affiliate.
      </p>

      <h5>2.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>  By agreeing to this Affiliate Application and Agreement that is accepted by Trio Trader, the Affiliate consents to allow Trio Trader, its affiliates, and any related company to:  (a) process and utilize the information submitted in the Affiliate Application and Agreement (as amended from time to time) for business purposes related to the Trio Trader business; and (2) disclose, now or in the future, such Affiliate information to companies which Trio Trader may, from time to time, deal with to deliver information to a Affiliate to improve its marketing and promotional efforts.  Affiliates have the right to access their personal information via their respective back office, and to submit updates thereto.
      </p>

      <h5>3.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        In consideration of the Company's acceptance of this Agreement, I agree and understand that if I subsequently elect to become an Independent Affiliate, I will be sponsored by the Affiliate who has enrolled me as an Affiliate.  In the event that I desire to be sponsored by a different Affiliate, I must submit a written request to the Company for a change of Sponsor prior to the submission of my Independent Affiliate Application and Agreement.  Changes of Sponsor are highly discouraged and rarely permitted.  Unless I receive written approval from Trio Trader to change my sponsor, my request will be deemed to be denied.
      </p>

      <h5>4.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        If I fail to renew my Trio Trader Affiliate Agreement, or if it is canceled or terminated for any reason, I understand that I will permanently lose all rights as a Affiliate.
        Trio Trader reserves the right to terminate all Affiliate Agreements upon 30 days notice if the Company elects to: (1) cease business operations; (2) dissolve as a business entity; or (3) terminate distribution of its products and/or services via direct selling channels.  Affiliate may cancel this Agreement at any time, and for any reason, upon written notice to Trio Trader at its principal business address.
      </p>

      <h5>5.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        Trio Trader, its parent or affiliated companies, directors, officers, shareholders, employees, assigns, and agents (collectively referred to as “corporate affiliates”), shall not be liable for, and I release Trio Trader and its corporate affiliates from, all claims for consequential and exemplary damages for any claim or cause of action relating to the Agreement.
      </p>

      <h5>6.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        The Agreement, in its current form and as amended by Trio Trader at its discretion, constitutes the entire contract between Trio Trader and myself.  Any promises, representations, offers, or other communications not expressly set forth in the Agreement are of no force or effect.
      </p>

      <h5>7.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        Any waiver by Trio Trader of any breach of the Agreement must be in writing and signed by an authorized officer of Trio Trader.  Waiver by Trio Trader of any breach of the Agreement by me shall not operate or be construed as a waiver of any subsequent breach.
      </p>

      <h5>8.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        If any provision of the Agreement is held to be invalid or unenforceable, such provision shall be severed, and the severed provision shall be reformed only to the extent necessary to make it enforceable.  The balance of the Agreement shall remain in full force and effect.
      </p>

      <h5>9.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        This Agreement will be governed by and construed in accordance with the laws of Canada without regard to principles of conflicts of laws.  In the event of a dispute between a Affiliate and Trio Trader arising from or relating to the Agreement, or the rights and obligations of either party, the parties shall attempt in good faith to resolve the dispute through nonbinding mediation as more fully described in the Policies and Procedures.  Trio Trader shall not be obligated to engage in mediation as a prerequisite to disciplinary action against an Affiliate.  If the parties are unsuccessful in resolving their dispute through mediation, the dispute shall be settled totally and finally by arbitration as more fully described in the Policies and Procedures. Notwithstanding the foregoing, Trio Trader shall be entitled to bring an action before the Courts in Canada seeking a restraining order, temporary or permanent injunction, or other equitable relief to protect its intellectual property rights, including but not limited to customer and/or distributor lists as well as other trade secrets, trademarks, trade names, patents, and copyrights.
      </p>

      <h5>10.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        The parties consent to jurisdiction and venue before any court in Canada for purposes of enforcing an award by an arbitrator, an action by Trio Trader for equitable relief, or any other matter not subject to arbitration.
      </p>

      <h5>11.</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>
        If a Affiliate wishes to bring an action against Trio Trader for any act or omission relating to or arising from the Agreement, such action must be brought within one year from the date of the alleged conduct giving rise to the cause of action, or the shortest time permissible under law.  Failure to bring such action within such time shall bar all claims against Trio Trader for such act or omission.  Affiliate waives all claims that any other statute of limitations applies.
      </p>

      <h5>Automatic Renewal Option</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>The term of the Affiliate Agreement is dependent on the initial program selected and may be renewed for successive terms on or near the expiration date of the Agreement. If the Agreement is not renewed on each expiration date, it will be cancelled and you will lose all rights as a Trio Trader Affiliate.  So that you do not inadvertently forget to renew and lose these benefits, Trio Trader offers an optional automatic renewal program. The option to renew or not renew can be edited at any time in the Trio Trader Dashboard or by emailing Customer Support.
      </p>

      <h5>Electronic Signature/Submit Application</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>This Application will not be "signed" in the sense of a traditional paper document. To “sign” this Application, complete the online application and indicate acceptance and your agreement to all provisions set forth in this document. By electronically “signing” below and clicking on the “Submit” button, you:  (1) certify that you are of legal age (the age of majority) in the area in which you reside; (2) authorize Trio Trader to charge the identified payment method for your Subscription fee; and (3) verify that you have carefully read and agree to abide by all of the terms set forth in the <a href="https://triotrader.com/user_agreement">User Agreement</a>.  In the event your Application is accepted by Trio Trader, you will have the right to terminate the Agreement at any time, with or without reason.
      </p>

      <h5>Notice of Right to Cancel</h5>
      <p style="padding-top:10px;">
        <span style="padding-left:20px;"></span>You may CANCEL this transaction, without any penalty or obligation, within THREE BUSINESS DAYS from the above Order Date if payment was not made with a Cryptocurrency (FIVE BUSINESS [5] DAYS for Alaska residents and FIFTEEN [15] BUSINESS DAYS in North Dakota for individuals age 65 and older). If payment was made with a Cryptocurrency, the cancellation request must be initiated the day of payment before 10PM EST to be given a full refund. To receive a partial refund (minus commissions) you must initiate a cancellation request within THREE BUSINESS DAYS from the date of purchase.  If you cancel, any property traded in, any payments made by you under the contract or sale, and any negotiable instrument executed by you will be returned within TEN BUSINESS DAYS following receipt by the seller of your cancellation notice, and any security interest arising out of the transaction will be cancelled.  If you cancel, you must make available to the seller at your residence, in substantially as good condition as when received, any goods delivered to you under this contract or sale, or you may, if you wish, comply with the instructions of the seller regarding the return shipment of the goods at the seller’s expense and risk.  If you do make the goods available to the seller and the seller does not pick them up within 20 days of the date of your Notice of Cancellation, you may retain or dispose of the goods without any further obligation.  If you fail to make the goods available to the seller, or if you agree to return the goods to the seller and fail to do so, then you remain liable for performance of all obligations under the contract.  To cancel this transaction, indicate so in a support message in our back office NO LATER THAN MIDNIGHT of the third business day following the date set forth above.
        <br><br>
        I HEREBY CANCEL THIS TRANSACTION.
        <br>
        Buyer’s Signature  ___________________________________		Date: _______________
        <br><br>

        Buyer – photocopy or print TWO copies of this receipt. If you elect to cancel your purchase, sign and return one copy to Trio Trader, and retain the second for your records.

      </p>

    </div>
    {{-- End container --}}

    <div class="footer-w">
        <div class="fade3"></div>
        <div class="os-container">
            <div class="footer-i">
                <div class="row">
                  <div class="col-sm-7 col-lg-4 b-r padded">
                      <img src="/img/ttraderlogo.png" style="max-width:100%">
                      <h6 class="heading-small">
                          One click trading and all of your crypto education in one spot!
                      </h6>
                      <p>
                          Real News, Real Traders, Real Signals, Real Information!
                      </p>
                  </div>
                    <div class="col-sm-5 col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Location
                                </h6>
                                <p>
                                    1 Riverside Drive W <br/>Windsor Ontario, CA
                                </p>
                            </div>
                            <div class="col-lg-6 b-r padded">
                                <h6 class="heading-small">
                                    Email
                                </h6>
                                <ul>
                                    <li>
                                        support@triotrader.com
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="deep-footer">
                Use of this site constitutes acceptance of our <a href="/user_agreement">User Agreement</a> and <a href="/privacy_policy">Privacy Policy</a>.<br>
                &copy; 2018 All rights reserved | Trio Trader | WorldCryptoCurrency101 | Trio Software Inc.
            </div>
        </div>
    </div>
  </div>
  {{-- End all-wraper --}}

  {{-- Google Translate --}}
  <script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@endsection
