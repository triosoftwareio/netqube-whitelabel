@extends('layouts.newApp')

@section('content')

  <?php
    use Carbon\Carbon;
  ?>

  @if($inactive)
    <div id="paymentModal" aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade bd-example-modal-lg" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              Complete Payment
            </h5>
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
          </div>
          <div class="modal-body">
            <h6 style="text-align:center;">Complete your deposit to gain access to your back office & start your crypto education!</h6>
            <h4 style="text-align:center;">Payment accepted in Bitcoin.</h4>

            @if($user->addressType == 'btc')
              <img src="{{$user->payment_address}}" style="display:block;margin:auto;"/>
              <u><h4 style="text-align:center;">Payment amount valid for:</h4></u>
              <h2 style="text-align:center;" id='countdown'></h2>
              <h6 style="text-align:center;">QR Code Not Working?</h6>

              @if($pendingBalance != 0)
                <h6 style="text-align:center;text-transform:initial;color:green !important;">{{ $pendingBalance }} is currently being processed </h6>
              @endif
              @if($receivedBalance != 0)
                <h6 style="text-align:center;text-transform:initial;color:green !important;">{{ $receivedBalance }} has been received. Please send the difference and refresh your page. </h6>
              @endif
              <h6 style="text-align:center;text-transform:initial !important;">Send exactly {{substr($user->payment_address, strpos($user->payment_address, "amount=") + 7)}} BTC  to {{strstr(substr($user->payment_address, strpos($user->payment_address, "bitcoin:") + 8), '?amount=', true)}}</h6>
              <h4 style="text-align:center;text-transform:initial;color:red !important;"><u>If you are sending from an exchange make sure to add the fee to the listed amount above.</u></h4>
              <h4 style="text-align:center;text-transform:initial;color:red !important;">If the amount received is less than the listed amount your account will not be activated</h4>
              <h4 style="text-align:center;text-transform:initial;color:green !important;">Once you send the amount listed, refresh the page and your account will be active if the amount received was enough.</h4>

            @else
              <img src="{{$user->payment_address_ltc}}" style="display:block;margin:auto;"/>
              <h6 style="text-align:center;">QR Code Not Working?</h6>

              <h6 style="text-align:center;text-transform:initial !important;">Send exactly {{substr($user->payment_address_ltc, strpos($user->payment_address_ltc, "amount=") + 7)}} LTC  to {{strstr(substr($user->payment_address_ltc, strpos($user->payment_address_ltc, "bitcoin:") + 8), '?amount=', true)}}</h6>
              <h6 style="text-align:center;text-transform:initial;color:red !important;">If you are sending from an exchange please ensure the amount received (after fees) is equal to or higher than the amount above</h6>
              <h4 style="text-align:center;text-transform:initial;color:red !important;">If the amount received is less than the listed amount your account will not be activated</h4>
            @endif
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button"> Save changes</button>
          </div>
        </div>
      </div>
    </div>
  @endif


        <div class="content-w">
          <!--------------------
          START - Breadcrumbs
          -------------------->
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="/home">Home</a>
            </li>
            <li class="breadcrumb-item">
              <span>Dashboard</span>
            </li>
            <li class="breadcrumb-item">
              <div id="google_translate_element" style="float:right"></div>
            </li>
          </ul>
          <!--------------------
          END - Breadcrumbs
          -------------------->
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Dashboard
                    </h6>
                    <div class="element-content" style="padding-bottom:25px;">
                        @if($user->status == 'Active')
                          <a href="/wallet">Add/Update Commission Wallet Address</a>
                        @endif
                    </div>
                    <div class="element-content">
                      <div class="row">
                        <div class="col">
                          <div class="element-box el-tablo">
                            <div class="label">
                              Users Recruited
                            </div>
                            <div class="value">
                              {{$usersRecruited}}<small>&nbspTotal</small>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <div class="element-box el-tablo">
                            <div class="label">
                              Lesser Leg Volume
                            </div>
                            <div class="value">
                              {{$lesserLegVolume/150}}<small>VOL</small>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <div class="element-box el-tablo">
                            <div class="label">
                              Total Volume
                            </div>
                            <div class="value">
                              {{$totalVolume/150}}<small>VOL</small>
                            </div>
                          </div>
                        </div>
                      </div>
                      {{-- End row --}}
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Academic Achievements
                  </h6>
                  <div class="element-content">
                    <div class="row">
                      <div class="col">
                        <div class="element-box el-tablo">
                            <a href="/courses">
                              <h6 class="lh-1">{{$userCompletedCourses}} Courses Completed</h6>
                            </a>
                            <i class="glyphicon glyphicon-book dashboard-icons"></i>
                            </div>
                        </div>
                        <div class="col">
                            <div class="element-box">
                                <div class="bgc-white p-20 bd panel-body stats-box modulebg">
                                    <i class="glyphicon glyphicon-bookmark dashboard-icons"></i>
                                    <h6 class="lh-1">{{$userCompletedVideos}} Modules Completed</h6>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col">
                            <div class="element-box">
                                <div class="row">
                                    <div class="col">
                                    <?php
                                      $completed = $userCompletedVideosList->where('courseId', 1)->count();
                                      $videos = $coursesList[0]->video->count();
                                      if($videos == 0) $videos = 1;
                                      else $percentage = ($completed/$videos)*100;
                                    ?>

                                        <div class="c100 center p{{$percentage}}">
                                            <span>{{round($percentage)}}%</span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                            <h6 class="text-center">Completed</h6>
                                            <a href="/courses/1/{{ $videoList->where('courseId', 1)->first()->id }}">
                                              <h6 class="text-center">{{$coursesList->where('id', 1)[0]->name}}</h6>
                                            </a>
                                    </div>

                                    {{-- <div class="col">


                                        <div class="c100 center p{{$percentage}}">
                                            <span>{{round($percentage)}}%</span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                            <h6 class="text-center">Completed</h6>
                                            <a href="/courses/1/{{ $videoList->where('courseId', 2)->first()->id }}">
                                              <h6 class="text-center">{{$coursesList->where('id', 2)[1]->name}}</h6>
                                            </a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              </div>

              {{-- @if(Auth::user()->status == 'Admin' || Auth::user()->status == 'Free' || (Auth::user()->status == 'Active' && Auth::user()->plan == '1')) --}}
              <div class="row">
                <div class="col">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Upcoming Webinars
                    </h6>
                    <div class="col">
                      <div class="element-box el-tablo">
                        @foreach($webinars as $webinar)
                          <div class="label">
                            {{ $webinar->prettyDate }} {{ $webinar->prettyTime }}
                          </div>
                          <div class="value">
                            <a href="{{ $webinar->link }}" style="text-decoration:none">
                              <small style="font-size:1.3rem !important">
                                {{ $webinar->name }}
                              </small>
                            </a>
                          </div>
                          <br>
                        @endforeach
                        @if($webinars == '[]')
                          <h4> No webinars scheduled </h4>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Previous Webinars
                    </h6>
                    <div class="col">
                      <div class="element-box el-tablo">
                        @foreach($prevWebinars as $webinar)
                          <div class="label">
                            {{ $webinar->date }}
                          </div>
                          <div class="value">
                            <a href="{{ $webinar->link }}" style="text-decoration:none">
                              <small style="font-size:1.4rem !important">
                                {{ $webinar->name }}
                              </small>
                            </a>
                          </div>
                          <br>
                        @endforeach
                        @if($prevWebinars == '[]')
                          <h4> No uploaded webinars </h4>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{-- End row --}}

              @if(Auth::user()->plan != 8 && (Auth::user()->status == 'Active' || Auth::user()->status == 'Free' || Auth::user()->status == 'Admin'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Signals (Bittrex)
                    </h6>
                    {{-- <form action="/change/default/exchange" method="post">
                      {!! csrf_field() !!}
                      <label for> Change default exchange </label>
                      <div class="input-group">
                        <select class="form-control" name="exchange">
                          @foreach($exchanges as $exchange)
                            <option value="{{ $exchange->value }}"> {{ $exchange->name }} </option>
                          @endforeach
                        </select>
                        <div class="input-group-btn">
                          <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                      </div>
                    </form><br> --}}
                    <span style="color:red">
                      <b><h6>Disclaimer: </h6></b>Signals are to be used at your own risk and are just a reference. There is no guarantee the target will be hit.
                    </span>
                      <?php $count = 0; ?>
                      <div class="row">
                        @foreach($signals as $signal)
                          @if($count == 3)
                            <div class="row">
                          @endif
                            @if($signal->type === 'Buy' && $count < 6)
                                <?php
                                $count++;
                                $currentProfit = round((($signal->price/$signal->startingPrice)-1)*100, 2);
                                $target = round((($signal->longLow/$signal->startingPrice)-1)*100, 2);
                                $percentageMilestone = round(($currentProfit/$target)*100);
                                if($percentageMilestone > 100){
                                    $percentageMilestone = 100;
                                }
                                ?>
                                    <div class="col" style="margin-top:12px;">
                                      <div class="element-box el-tablo" style="height:376px;">
                                          <h6 class="lh-1">{{$signal->pair}}</h6>
                                          <div class="c100 center p{{$percentageMilestone}}">
                                              <span>{{$currentProfit}}%</span>
                                              <div class="slice">
                                                  <div class="bar"></div>
                                                  <div class="fill"></div>
                                              </div>
                                          </div>
                                          <h6 class="lh-1">Target {{$target}}%</h6>
                                          <p class="webinar-date">Enter {{$signal->pair}} at <br>{{number_format($signal->startingPrice, 8)}}</p>
                                          <p class="webinar-date">Exit {{$signal->pair}} at <br>{{number_format($signal->longLow, 8)}}</p>
                                          <h6 class="lh-1">Called At
                                            <?php
                                              $thisCarbon = Carbon::createFromTimestamp($signal->timestamp/1000);
                                              $thisCarbon->setTimeZone('America/Toronto');
                                              echo $thisCarbon->toDayDateTimeString() . ' EST';
                                            ?>
                                          </h6>
                                      </div>
                                    </div>
                            @endif
                            @if($count == 3)
                              </div>
                            @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              {{-- @endif --}}
              {{-- End row signals --}}


              {{-- <div class="row">
                <div class="col-sm-6">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Withdrawls
                    </h6>
                    <div class="element-box">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Transaction ID
                              </th>
                              <th>
                                Amount
                              </th>
                              <th class="text-center">
                                Date
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($withdrawls as $withdrawl)
                              <tr>
                                <td class="nowrap">
                                  {{$withdrawl->id}}
                                </td>
                                <td class="text-center">
                                  {{$withdrawl->amount}}
                                </td>
                                <td class="text-right">
                                  {{$withdrawl->created_at}}
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Deposits
                    </h6>
                    <div class="element-box">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-warning">
                            <tr>
                              <th>
                                Transaction ID
                              </th>
                              <th>
                                Amount
                              </th>
                              <th class="text-center">
                                Date
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($payments as $deposit)
                              <tr>
                                <td class="nowrap">
                                  {{$deposit->id}}
                                </td>
                                <td class="text-center">
                                  {{$deposit->amount}}
                                </td>
                                <td class="text-right">
                                  {{$deposit->created_at}}
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}
            </div>
            <!--------------------
            START - Sidebar
            -------------------->
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Live Tickers
                </h6>
                {{-- BTC Ticker --}}
                <div class="coinmarketcap-currency-widget" data-currencyid="1" data-base="USD" data-secondary="" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-stats="USD" data-statsticker="false"></div>
                {{-- ETH Ticker --}}
                <hr>
                <div class="coinmarketcap-currency-widget" data-currencyid="1027" data-base="USD" data-secondary="BTC" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-stats="USD" data-statsticker="false"></div>
                {{-- LTC Ticker --}}
                <hr>
                <div class="coinmarketcap-currency-widget" data-currencyid="2" data-base="USD" data-secondary="BTC" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-stats="USD" data-statsticker="false"></div>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Summary / Update Archives
                </h6>
                <div class="element-box-tp">
                  {{-- <div class="activity-boxes-w"> --}}
                    {{-- @if(Auth::user()->status == 'Admin') --}}
                    <ul>
                      @foreach($summaries as $record)
                        <a href="/archive/?month={{ $record['month'] }}&year={{ $record['year'] }}">
                          <div class="user-name">
                            <h6 class="user-title">
                              {{$record['month']}} {{$record['year']}}
                            </h6>
                          </div>
                        </a>
                        {{-- <li>
                          <a href="/archive/?month={{ $record['month'] }}&year={{ $record['year'] }}">{{ $record['month'] }} {{ $record['year'] }}</a>
                        </li> --}}
                      @endforeach
                    </ul>
                    <ul>
                      <a href="/Example_Trading_Journal.xlsx" download> Click Here to Download Example Training Journal </a>
                    </ul>
                    {{-- @endif --}}
                  {{-- </div> --}}
                </div>
              </div>
              {{-- <div class="element-wrapper">
                <h6 class="element-header">
                  New Members
                </h6>
                <div class="element-box-tp">
                  <div class="users-list-w">
                  @foreach($recentUsers as $user)
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="/uploads/avatars/{{ $user->avatar }}">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          {{$user->username}}
                        </h6>
                        <div class="user-role">
                          {{$user->binaryPlan->name}} Package
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div> --}}
            </div>
            <!--------------------
            END - Sidebar
            -------------------->
            <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script>
            <script>

            Date.prototype.subHours = function(h){
              this.setHours(this.getHours()-h);
              return this;
            }

              // Set the date we're counting down to
              var countDownDate = new Date("{{ $endTime }}").getTime();

              // Update the count down every 1 second
              var x = setInterval(function() {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                document.getElementById("countdown").innerHTML = minutes + "m " + seconds + "s ";

                // If the count down is finished, write some text
                if (distance < 0) {
                  clearInterval(x);
                  document.getElementById("countdown").innerHTML = "EXPIRED. REFRESH PAGE";
                }
              }, 1000);
            </script>

@endsection
