@extends('layouts.newApp')

@section('content')
  {{-- @if(Auth::check() && Auth::user()->status == 'Admin') --}}
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Archive
                </h6>
                {{-- <div class="element-box">
                  <h4> Free Summary and Update </h4>
                  <div class="row">
                    <div class="col">
                      <div style="padding:20px !important;">
                        <div class="label">
                          {{ $summaries[0]->name }}
                        </div>
                        <div class="value">
                          {{ $summaries[0]->created_at }}<br>
                          <a href="{{ $summaries[0]->localPath }}" download> Click Here to download </a>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      <div style="padding:20px !important;">
                        <div class="label">
                          {{ $updates[0]->name }}
                        </div>
                        <div class="value">
                          {{ $updates[0]->created_at }}<br>
                          <a href="{{ $updates[0]->localPath }}" download> Click Here to download </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> --}}
                {{-- Add Auth check for below as more are pushed out --}}
                @if(count($summaries) > 0)
                  <h6 class="element-header">
                    Summaries
                  </h6>
                @endif
                @foreach($summaries as $summary)
                  <div class="element-box">
                    <div style="padding:20px !important;">
                      <div class="label">
                        {{ $summary->name }}
                      </div>
                      <div class="value">
                        {{ $summary->created_at }}<br>
                        <a href="{{ $summary->localPath }}" download> Click Here to download </a>
                      </div>
                    </div>
                  </div>
                @endforeach
                @if(count($updates) > 0)
                  <h6 class="element-header">
                    Updates
                  </h6>
                @endif
                @foreach($updates as $update)
                  <div class="element-box">
                    <div style="padding:20px !important;">
                      <div class="label">
                        {{ $update->name }}
                      </div>
                      <div class="value">
                        {{ $update->created_at }}<br>
                        <a href="{{ $update->localPath }}" download> Click Here to download </a>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- @endif --}}
@endsection
