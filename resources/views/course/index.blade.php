@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="element-wrapper">
                <h3 class="element-header">
                  Courses
                </h3>
                @if($courses == '[]')
                  <div class="element-box table-responsive">
                    <div class="list-group">
                        <h1 class="text-center"> Videos coming soon <h1>
                    </div>
                  </div>
                @else
                  <div class="row">
                    <?php $count = 0;?>
                  @foreach($courses as $course)
                    @if($count%2 == 0 && $count != 0) <div class="row">
                    @endif
                    <div class="col-md-6">
                      <div class="element-box table-responsive">
                        {{-- <h1>{{$course->name}}</h1> --}}
                            @foreach($course->course as $subCourse)
                              <a href="/courses/{{$subCourse->id}}/{{$subCourse->video->first()->id}}" style="text-decoration:none">
                                <div class="bgc-white p-20 bd" style="margin-top:12px;">
                                  <?php
                                      $totalCount = count($subCourse->video);
                                      $totalCompletedInCourse = $userCompletedVideos->where('courseId', $subCourse->id)->count();
                                      $percentage = ($totalCompletedInCourse/$totalCount)*100;
                                  ?>
                                  <div class="col-lg-12">
                                      <div class="c100 center p{{round($percentage)}}">
                                          <span>{{round($percentage)}}%</span>
                                          <div class="slice">
                                              <div class="bar"></div>
                                              <div class="fill"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-12 padleft30">
                                      <h6 class="c-grey-900 text-align-center">{{$subCourse->name}}</h6>
                                      <p class="description text-align-center">{{$subCourse->description}}</p>
                                  </div>
                                </div>
                              </a>
                            @endforeach
                          </div>
                    </div>
                    @if($count%2 == 0 && $count != 0) </div>
                    @endif
                    <?php $count++; ?>
                  @endforeach
                  </div>
                @endif
        {{-- @endif --}}
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
