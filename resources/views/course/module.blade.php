@extends('layouts.newApp')

@section('content')
<div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
        <div class="content-i">
            <div class="content-box">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="element-wrapper">
                            <h3 class="element-header">
                                {{ $course->name }} - {{ $currentVideo->videoName }}
                            </h3>
                            <div class="element-box table-responsive">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12 section">
                                        <div class="bgc-white p-20 bd" style="margin-top:12px;">
                                            {!!$currentVideo->embed!!}
                                        </div>
                                    </div>

                                    <div class=" col-lg-4 col-md-4 col-sm-12 section">
                                        <div class="bgc-white p-20 bd" style="margin-top:12px;">
                                            <div class="table-responsive">
                                                <table>
                                                    <?php
                                                    $totalCount = count($course->video);
                                                    $percentage = ($userCompletedVideos/$totalCount)*100;
                                                    ?>
                                                    <tr>
                                                      <div class="c100 center p{{round($percentage)}}">
                                                          <span>{{round($percentage)}}%</span>
                                                          <div class="slice">
                                                              <div class="bar"></div>
                                                              <div class="fill"></div>
                                                          </div>
                                                      </div>
                                                    </tr>
                                                    <tr>
                                                      <h6 class="c-grey-900 text-center">{{$course->name}}</h6>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="bgc-white p-20 bd" style="margin-top:12px;">
                                          <div class="table-responsive">
                                            <table>
                                            <?php $counter = 1;?>
                                            @foreach($videos as $video)
                                              <tr>
                                                <td>
                                                  <?php $completedThisVid = $completedVideos->where('videoId', $video->id)->count(); ?>
                                                  @if($completedThisVid)
                                                      <img src="/img/checked-symbol.png">
                                                  @else
                                                      <img src="/img/unchecked.png">
                                                  @endif
                                                </td>
                                                <td>
                                                  <a href="/courses/{{$course->id}}/{{$video->id}}" class="business-statsdw module-video">
                                                      <h6 class="lh-1">{{$counter}} - {{$video->videoName}}</h6>

                                                  </a>
                                                  <?php $counter++; ?>
                                                </td>
                                              </tr>
                                            @endforeach
                                            </table>
                                          </div>

                                            <div class="col-lg-12">
                                                <a class="btn btn-primary" style="margin-top:30px" href="/course/complete/{{$currentVideo->id}}">MARK MODULE COMPLETE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
