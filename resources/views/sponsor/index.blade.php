@extends('layouts.front')

@section('content')
    <div class="all-wrapper">
        <div class="fade1"></div>
        <div class="desktop-menu menu-top-w menu-activated-on-hover">
            <div class="menu-top-i os-container">
                <div class="logo-w">
                    <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
                </div>
                <ul class="main-menu">
                    <li>
                        <a href="/">Home</a>
                    </li>
                </ul>
                <ul class="small-menu">
                    @if(Auth::check())
                      <li class="separate">
                          <a href="{{ route('logout') }}">Logout</a>
                      </li>
                    @else
                      <li class="separate">
                          <a href="{{ route('login') }}">Login</a>
                      </li>
                    @endif
                </ul>
            </div>
            <div class="mobile-menu-w">
                <div class="mobile-menu-holder color-scheme-dark">
                    <ul class="mobile-menu">
                        <li class="active">
                            <a href="/">Home</a>
                        </li>
                        @if(Auth::check())
                          <li>
                              <a href="{{ route('logout') }}">Logout</a>
                          </li>
                        @else
                          <li>
                              <a href="{{ route('login') }}">Login</a>
                          </li>
                        @endif
                    </ul>
                </div>
                <div class="mobile-menu-i">
                    <div class="mobile-logo-w">
                        <img src="/img/ttraderlogo.png" width="200px"></img>
                    </div>
                    <div class="mobile-menu-trigger">
                        <i class="os-icon os-icon-hamburger-menu-1"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="padding-top:30px">
          <div class="row">
            <div class="col">
              <h1 class="text-center">
                <u>
                  Enter Sponsor Username
                </u>
              </h1>
              <input type="text" name="sponsorName" id="sponsorName" placeholder="Sponsor Username" class="form-control"> <br>
              @if(isset($_GET['package']) && isset($_GET['price']))
                <a class="btn btn-primary btn-sm" href="/register/?package={{$_GET['package']}}&price={{ $_GET['price'] }}&sponsor=" onclick="return redirectClick(this)" id="redirect">Submit</a>
              @else
                <a class="btn btn-primary btn-sm" href="https://www.triotrader.com/?sponsor=" onclick="return redirectClick(this)" id="redirect">Submit</a>
              @endif
            </div>
          </div>
        </div>


        <div class="display-type"></div>
    </div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="js/main_front.js?version=3.0"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-XXXXXXXX-9', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
      function redirectClick(btn){
        document.getElementById('redirect').href += document.getElementById('sponsorName').value;
        return true;
      }
    </script>
@endsection
