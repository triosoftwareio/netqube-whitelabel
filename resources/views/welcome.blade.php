@extends('layouts.front')

@section('content')
  @if(!Auth::check())
  <div class="modal fade" id="welcomeModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          @if(request('sponsor') != NULL)
            <h6 style="text-align:center;">This site is brought to you by <b><u>{{ request('sponsor') }}</u></b></h6>
          @endif
        </div>
      </div>
    </div>
  </div>
  @endif

    <div class="all-wrapper">
        <div class="fade1"></div>
        <div class="desktop-menu menu-top-w menu-activated-on-hover">
            <div class="menu-top-i os-container">
                <div class="logo-w">
                    <img src="/img/ttraderlogo.png" style="max-width:225px"></img>
                </div>
                <ul class="main-menu">
                    <li class="active">
                        <a href="#sectionIntro">Home</a>
                    </li>
                    <li>
                        <a href="#sectionFeatures">Features</a>
                    </li>
                    <li>
                        <a href="#sectionTestimonials">Philosophy</a>
                    </li>
                    <li>
                      <a href="/comp_plan">Comp Plan</a>
                    </li>
                    @if(Auth::check())
                      <li>
                          <a href="/home">Dashboard</a>
                      </li>
                    @endif
                </ul>
                <ul class="small-menu">
                    @if(Auth::check())
                      <li class="separate">
                          <a href="{{ route('logout') }}">Logout</a>
                      </li>
                      <li class="separate">
                        <div id="google_translate_element"></div>
                      </li>
                    @else
                      <li class="separate">
                        <div id="google_translate_element"></div>
                      </li>
                      <li class="separate">
                          <a href="{{ route('login') }}">Login</a>
                      </li>
                      <li class="separate">
                          <a href="#sectionPackages" class="highlight">Sign Up</a>
                      </li>
                    @endif
                </ul>
            </div>
            <div class="mobile-menu-w">
                <div class="mobile-menu-holder color-scheme-dark">
                    <ul class="mobile-menu">
                        <li class="active">
                            <a href="#sectionIntro">Home</a>
                        </li>
                        <li>
                            <a href="#sectionFeatures">Features</a>
                        </li>
                        <li>
                            <a href="#sectionTestimonials">Philosophy</a>
                        </li>
                        <li>
                          <a href="/comp_plan">Comp Plan</a>
                        </li>
                        @if(Auth::check())
                          <li>
                              <a href="/home">Dashboard</a>
                          </li>
                          <li>
                              <a href="{{ route('logout') }}">Logout</a>
                          </li>
                          <li class="separate">
                            <div id="google_translate_element"></div>
                          </li>
                        @else
                          <li>
                              <a href="#sectionPackages" class="highlight">Sign Up</a>
                          </li>
                          <li>
                              <a href="{{ route('login') }}">Login</a>
                          </li>
                          <li class="separate">
                            <div id="google_translate_element"></div>
                          </li>
                        @endif
                    </ul>
                </div>
                <div class="mobile-menu-i">
                    <div class="mobile-logo-w">
                        <img src="/img/ttraderlogo.png" width="200px"></img>
                    </div>
                    <div class="mobile-menu-trigger">
                        <i class="os-icon os-icon-hamburger-menu-1"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="intro-w layout-v1" id="sectionIntro">
            <div class="os-container" style="padding-top:70px;">
                <div class="fade2"></div>
                <div class="intro-i">
                    <div class="intro-description">
                        <h1 class="intro-heading">
                            The most <span>in-depth</span> crypto education program + signals!
                        </h1>
                        <div class="intro-text">
                            What is the difference? The difference is we provide real information, real results and all in advance before the jumps happen!
                            Not only do you get summaries and updates but notifications for entries and exits on manual trading signals <br>that are completely automated!
                        </div>
                    </div>
                    <div class="intro-media">
                      {{-- <script src="https://fast.wistia.com/embed/medias/uifp29ohfa.jsonp" async></script>
                      <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
                      <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                        <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                          <div class="wistia_embed wistia_async_uifp29ohfa videoFoam=true" style="height:100%;position:relative;width:100%">
                            <div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;">
                              <img src="https://fast.wistia.com/embed/medias/uifp29ohfa/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" onload="this.parentNode.style.opacity=1;" />
                            </div>
                          </div>
                        </div>
                      </div> --}}
                        <div class="shot shot1">
                            <div class="shot-i" style="background-image: url(&#39;/img/binaryScreen.png&#39;)"></div>
                        </div>
                        <div class="shot shot2">
                            <div class="shot-i" style="background-image: url(&#39;/img/signalScreen.png&#39;)"></div>
                        </div>
                        <div class="shot shot3">
                            <div class="shot-i" style="background-image: url(&#39;/img/dashboardScreen.png&#39;)"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="counters-w">
            <div class="os-container">
                <div class="counters-i">
                    <div class="counter">
                        <div class="counter-value-w">
                            <div class="counter-value">
                                1
                            </div>
                            <div class="counter-name">
                                Team
                            </div>
                        </div>
                        <div class="counter-description">
                            Our growing team is dedicated to providing you with all of the tools to learn and succeed in the crypto world with the click of a button!
                        </div>
                    </div>
                    <div class="counter">
                        <div class="counter-value-w">
                            <div class="counter-value">
                                1
                            </div>
                            <div class="counter-name">
                                Stop
                            </div>
                        </div>
                        <div class="counter-description">
                            Your all in-one crypto guide. Get informed, get started and get trading! Grow your wealth at your pace.
                        </div>
                    </div>
                    <div class="counter">
                        <div class="counter-value-w">
                            <div class="counter-value">
                                100
                            </div>
                            <div class="counter-name">
                                Dollars in BTC
                            </div>
                        </div>
                        <div class="counter-description">
                            Earn about $100 in BTC for every customer referral you make (Semi Package) and about $100 in BTC per sale on the smaller team along with three levels of matching bonus at 20-20-10.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-header-w" id="sectionFeatures">
            <div class="fade3"></div>
            <div class="os-container">
                <div class="section-header">
                    <h5 class="section-sub-title">
                        This is what separates us.
                    </h5>
                    <h2 class="section-title">
                        What We Offer
                    </h2>
                </div>
            </div>
        </div>
        <div class="features-table">
            <div class="os-container">
                <div class="row no-gutters">
                    <div class="col-xl-4 col-sm-6 b-l b-t feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-pencil-1"></i>
                        </div>
                        <h6 class="feature-title">
                            Full Responsive Support
                        </h6>
                        <div class="feature-text">
                            Live chat with support agents on any issues you may be having.
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 b-r b-l b-t feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-signs-11"></i>
                        </div>
                        <h6 class="feature-title">
                            Signals (HitBTC)
                        </h6>
                        <div class="feature-text">
                            View and utilize automated trade signals generated from our proven, 5 year back-tested algorithms!
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 b-r b-t feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-mail-14"></i>
                        </div>
                        <h6 class="feature-title">
                            Trailing Stop Loss
                        </h6>
                        <div class="feature-text">
                            Our algorithms not only go for the percentage you ask but also trail it upwards to get the most profit out of every trade!
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 b-l feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-cv-2"></i>
                        </div>
                        <h6 class="feature-title">
                            Affiliate Marketing
                        </h6>
                        <div class="feature-text">
                            Earn more by marketing and referring new users.
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 b-r b-l feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-crown"></i>
                        </div>
                        <h6 class="feature-title">
                            Education
                        </h6>
                        <div class="feature-text">
                            Learn from our pro traders by going through our video course designed to simplify understanding and reading the markets and reading charts. Summaries and updates of coins/ICOs are also provided.
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6 b-r feature-cell">
                        <div class="feature-icon">
                            <i class="os-icon os-icon-ui-44"></i>
                        </div>
                        <h6 class="feature-title">
                            All in one solution
                        </h6>
                        <div class="feature-text">
                            Whether you are new to crypto or a pro, we offer in-depth summaries and provide proven automated signals to further educate you
                            and help to build your crypto wealth quickly and securely.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-w relative" id="sectionTestimonials">
            <div class="fade4"></div>
            <div class="testimonials-i">
                <div class="os-container">
                    <div class="section-header dark">
                        <h5 class="section-sub-title">
                            "If there was any way to take advantage of a global change, would you take it?"
                        </h5>
                        <h2 class="section-title">
                            Our Philosophy
                        </h2>
                        <div class="section-desc">
                          It’s a wondrous thing to ponder... all of the incredible changes the world has undergone in the past century.
                          We have grown from the horse and buggy to super highways and from the telegraph to smart phones.
                          We have grown since the dawn of aviation to sending probes beyond Pluto.
                          Money, currency and the whole transaction based industry are changing too.
                          If it sometimes feels like technological breakthroughs are becoming common place… well, that's because they are!

                           <br><br>

                           For us, we want to be proactive regarding the global change.
                           We want all of our friends and family to enjoy the benefits of change too. That includes like-minded people around the world.
                           In times of change, there is both danger and opportunity. That's why we are here to help guide you through the murky waters.
                           So many people are jumping in head first into the crypto pool without a plan or the knowledge necessary to succeed and limit risk.
                           We welcome you to take the next step towards cryptocurrency education and proficiency with our Trio Trader.
                        </div>
                    </div>
                </div>

                {{-- <div class="testimonials-slider-w">
                    <div class="testimonials-slider">
                        <div class="slide-w">
                            <div class="slide">
                                <div class="testimonial-title">
                                    Truly transparent and legitimate!
                                </div>
                                <div class="testimonial-content">
                                    I was asked to write my thoughts on this product, and in all honesty besides a few little improvments that can be made this system is great and it really does what it says it does. I was getting fed up with alot of systems out there that were coming and going and i finally found my home for crypto investments.
                                </div>
                                <div class="testimonial-by">
                                    <strong>Sandra P.</strong><span>Invester</span>
                                </div>
                            </div>
                        </div>
                        <div class="slide-w">
                            <div class="slide">
                                <div class="testimonial-title">
                                    Incredible Trading Platform!
                                </div>
                                <div class="testimonial-content">
                                    Probably the best bitcoin trading platform I have seen so far, im already building a binary residual income using this system.
                                </div>
                                <div class="testimonial-by">
                                    <strong>Greg</strong><span>Multi Level Marketer</span>
                                </div>
                            </div>
                        </div>
                        <div class="slide-w">
                            <div class="slide">
                                <div class="testimonial-title">
                                    The best bitcoin trading pool out there!
                                </div>
                                <div class="testimonial-content">
                                    The user friendly design and backoffice is exactly what i needed, ive already told all my friends and family about this!
                                </div>
                                <div class="testimonial-by">
                                    <strong>Brad M</strong><span>Investor</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="section-header-w" id="sectionPackages">
            <div class="fade3"></div>
            <div class="os-container">
                <div class="section-header">
                    <h5 class="section-sub-title">
                        Packages for short and long term trials!
                    </h5>
                    <h2 class="section-title">
                        Packages
                    </h2>
                    <div class="section-desc">
                        Be an affiliate or subscribe for full access to all content!
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="projects-slider-w">
            <div class="fade2"></div>
            <div class="projects-slider-i"> --}}
            {{-- <div class="project-slide-w"> --}}
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                          <div class="project-title">
                            Pro Package / 1 Month - $100 (BTC)
                          </div>
                          <div class="project-text" style="height:269px">
                              <p>Live Trading Webinars</p>
                              <p>Training Videos</p>
                              <p>Weekly Summaries/Updates</p>
                              <p>Professional automated signals</p>
                              <p><b>Trio Trader</b></p>
                              <p>Affiliate package</p>
                          </div>
                          <div class="project-icons-buton">
                            @if(isset($_GET['sponsor']))
                              <a class="btn btn-primary" href="/register?package=Pro_Monthly&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=100">Sign Up</a>
                            @else
                              <a class="btn btn-primary" href="/enter/sponsor/?package=Pro_Monthly&price=100">Sign Up</a>
                            @endif
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                          <div class="project-title">
                            Pro Package / 6 Months - $600 (BTC)
                          </div>
                          <div class="project-text" style="height:269px">
                              <p>Live Trading Webinars</p>
                              <p>Training Videos</p>
                              <p>Weekly Summaries/Updates</p>
                              <p>Professional automated signals</p>
                              <p>Affiliate package</p>
                              <p><b>Trio Trader</b></p>
                          </div>
                          <div class="project-icons-buton">
                            @if(isset($_GET['sponsor']))
                              <a class="btn btn-primary" href="/register?package=Pro_Semi&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=600">Sign Up</a>
                            @else
                              <a class="btn btn-primary" href="/enter/sponsor/?package=Pro_Semi&price=600">Sign Up</a>
                            @endif
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                          <div class="project-title">
                            Pro Package / 12 Months - $1000 (BTC)
                          </div>
                          <div class="project-text" style="height:269px">
                              <p>Live Trading Webinars</p>
                              <p>Training Videos</p>
                              <p>Weekly Summaries/Updates</p>
                              <p>Professional automated signals</p>
                              <p>Affiliate package</p>
                              <p><b>Trio Trader</b></p>
                          </div>
                          <div class="project-icons-buton">
                            @if(isset($_GET['sponsor']))
                              <a class="btn btn-primary" href="/register?package=Pro_Annual&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=1000">Sign Up</a>
                            @else
                              <a class="btn btn-primary" href="/enter/sponsor/?package=Pro_Annual&price=1000">Sign Up</a>
                            @endif
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <p class="text-center">&nbsp&nbspIf you would like to join as an Affiliate only - no content or signals -
              @if(isset($_GET['sponsor']))
                <a href="/register?package=Sales_Rep&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=97">Click Here</a>
              @else
                <a href="/enter/sponsor/?package=Sales_Rep&price=97">Click Here</a>
              @endif
            </p>

            {{-- </div> --}}

                {{-- <div class="project-slide-w">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/f/fa/Blank_BitCoin_Logo_Graphic.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                          <div class="project-title">
                              Basic Package / 1 Year - $857 (USD/BTC/LTC)
                          </div>
                          <div class="project-text" style="height:269px">
                              <p>Live Trading Webinars</p>
                              <p>Training Videos</p>
                              <p>Weekly Summaries/Updates</p>
                              <p>Approximately $150 commission on sale on weaker leg (In BTC/Cycle)</p>
                          </div>
                            <div class="project-icons-buton">
                              <a class="btn btn-primary btn-sm" href="/register?package=BasicAnnual&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=857">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="project-slide-w">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/f/fa/Blank_BitCoin_Logo_Graphic.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                            <div class="project-title">
                                Pro Package / 6 Month - $847 (USD/BTC/LTC)
                            </div>
                            <div class="project-text" style="height:269px">
                                <p>Live Trading Webinars</p>
                                <p>Basic Package</p>
                                <p>Professional automated signals for manual traders created with top traders</p>
                                <p>Approximately $150 commission on sale on weaker leg (In BTC/Cycle)</p>
                            </div>
                            <div class="project-icons-buton">
                              <a class="btn btn-primary btn-sm" href="/register?package=ProSemi&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=847">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="project-slide-w">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/f/fa/Blank_BitCoin_Logo_Graphic.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                            <div class="project-title">
                                Pro Package / 1 Year - $1497 (USD/BTC/LTC)
                            </div>
                            <div class="project-text" style="height:269px">
                                <p>Live Trading Webinars</p>
                                <p>Basic Package</p>
                                <p>Professional automated signals for manual traders created with top traders</p>
                                <p>Approximately $150 commission on sale on weaker leg (In BTC/Cycle)</p>
                            </div>
                            <div class="project-icons-buton">
                              <a class="btn btn-primary btn-sm" href="/register?package=ProAnnual&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=1497">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="project-slide-w">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/f/fa/Blank_BitCoin_Logo_Graphic.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                            <div class="project-title">
                                Signal Only Package / 6 Month - $497 (USD/BTC/LTC)
                            </div>
                            <div class="project-text" style="height:269px">
                                <p>Live Trading Webinars</p>
                                <p>Professional automated signals for manual traders.</p>
                                <p>Created with a select group of successful crypto traders</p>
                                <p>Integrated in Poloniex market with more to come!</p>
                                <p>Approximately $150 commission on sale on weaker leg (In BTC/Cycle)</p>
                            </div>
                            <div class="project-icons-buton">
                                <a class="btn btn-primary btn-sm" href="/register?package=SignalSemi&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=497">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="project-slide-w">
                    <div class="project-slide">
                        <div class="project-media-w">
                            <div class="project-media">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/f/fa/Blank_BitCoin_Logo_Graphic.png" width="256px" style="display:block;margin:auto;">
                            </div>
                        </div>
                        <div class="project-content">
                            <div class="project-title">
                                Signal Only Package / 1 Year - $887 (USD/BTC/LTC)
                            </div>
                            <div class="project-text" style="height:269px">
                                <p>Live Trading Webinars</p>
                                <p>Professional automated signals for manual traders.</p>
                                <p>Created with a select group of successful crypto traders</p>
                                <p>Integrated in Poloniex market with more to come!</p>
                                <p>Approximately $150 commission on sale on weaker leg (In BTC/Cycle)</p>
                            </div>
                            <div class="project-icons-buton">
                                <a class="btn btn-primary btn-sm" href="/register?package=SignalAnnual&sponsor={{isset($_GET['sponsor']) ? $_GET['sponsor'] : ''}}&price=887">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
            {{-- </div>
        </div> --}}

        <div class="footer-w">
            <div class="fade3"></div>
            <div class="os-container">
                <div class="footer-i">
                    <div class="row">
                        <div class="col-sm-7 col-lg-4 b-r padded">
                            <img src="/img/ttraderlogo.png" style="max-width:100%">
                            <h6 class="heading-small">
                                One click trading and all of your crypto education in one spot!
                            </h6>
                            <p>
                                Real News, Real Traders, Real Signals, Real Information!
                            </p>
                        </div>
                        <div class="col-sm-5 col-lg-8">
                            <div class="row">
                                <div class="col-lg-6 b-r padded">
                                    <h6 class="heading-small">
                                        Location
                                    </h6>
                                    <p>
                                        1 Riverside Drive W <br/>Windsor Ontario, CA
                                    </p>
                                </div>
                                <div class="col-lg-6 b-r padded">
                                    <h6 class="heading-small">
                                        Email
                                    </h6>
                                    <ul>
                                        <li>
                                            support@trio.software
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="deep-footer">
                    Use of this site constitutes acceptance of our <a href="/user_agreement">User Agreement</a>,<a href="/privacy_policy">Privacy Policy</a> and <a href="/affiliate_agreement"> Affiliate Agreement.</a><br>
                    &copy; 2018 All rights reserved | Trio Software | Trio Trader
                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="js/main_front.js?version=3.0"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-XXXXXXXX-9', 'auto');
        ga('send', 'pageview');
    </script>


        {{-- Google Translate --}}
        <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
        }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>



    {{-- <script>
      $(window).on('load', function(){
        $('#welcomeModal').modal('show);')
      });
    </script> --}}
@endsection
