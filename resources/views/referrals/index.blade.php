@extends('layouts.newApp')

@section('content')

  <?php
    use Carbon\Carbon;
  ?>

    <div class="content-w">
      <div class="container-fluid" style="min-height:1200px;">

        <div class="content-i">
          <div class="content-box">
            <div class="row">
              <div class="col">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    My Referrals
                  </h6>
                  <div class="element-box">
                    <div style="padding:20px !important;">
                      <div class="table-responsive">
                        <table class="table table-lightborder">
                          <thead class="text-success">
                            <tr>
                              <th>
                                Name
                              </th>
                              <th>
                                Username
                              </th>
                              <th>
                                Email
                              </th>
                              <th>
                                Status
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($referralUsers as $ru)
                              <tr>
                                <td>
                                  {{ $ru->name }}
                                </td>
                                <td>
                                  {{ $ru->username }}
                                </td>
                                <td>
                                  {{ $ru->email }}
                                </td>
                                <td>
                                  @if($ru->status == 'Active') Paid
                                  @else Not Paid
                                  @endif
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- End Row --}}
        </div>
      </div>
    </div>
  </div>

@endsection
