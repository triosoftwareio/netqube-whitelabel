@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Webinars
                </h6>
                <div class="row">
                  <div class="col">
                    <div class="element-box el-tablo">
                      <h6> Upcoming Webinars </h6>
                      <div style="padding:20px !important;">
                        @foreach($webinars as $webinar)
                          <div class="label">
                            {{ $webinar->prettyDate }} {{ $webinar->prettyTime }}
                          </div>
                          <div class="value">
                            <a href="{{ $webinar->link }}">{{ $webinar->name }}</a>
                          </div>
                          @if(Auth::user()->status == 'Admin')
                            <br>
                              <a href="/admin/webinar/edit/{{$webinar->id}}">Edit</a> &nbsp&nbsp
                              <a href="/admin/webinar/remove/{{$webinar->id}}">Delete</a>
                          @endif
                        @endforeach
                        @if($webinars == '[]')
                          <h4 class="text-center"> No webinars scheduled </h4>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="col">
                    <div class="element-box el-tablo">
                      <h6> Uploaded Webinars </h6>
                      <div style="padding:20px !important;">
                        @foreach($prevWebinars as $webinar)
                          <div class="label">
                            {{ $webinar->date }}
                          </div>
                          <div class="value">
                            <a href="{{ $webinar->link }}">{{ $webinar->name }}</a>
                          </div>
                          @if(Auth::user()->status == 'Admin')
                            <br>
                              <a href="/admin/prevwebinar/edit/{{$webinar->id}}">Edit</a> &nbsp&nbsp
                              <a href="/admin/prevwebinar/remove/{{$webinar->id}}">Delete</a>
                          @endif
                        @endforeach
                        @if($prevWebinars == '[]')
                          <h4 class="text-center"> No uploaded webinars </h4>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {{-- End Row --}}

          @if(Auth::user()->status == 'Admin')
            <div class="row">
              <div class="col-md-12">
                <div class="element-wrapper">
                  <h6 class="element-header">
                    Admin
                  </h6>
                  <div class="element-box">
                    <div class="bgc-white p-20 bd" style="margin-top:12px;">
                      <h6 class="c-grey-900">Add New Webinar</h6>
                      <form action="/admin/add/webinar" method="post">
                          <input type="hidden" value="{{ csrf_token() }}" name="_token">
                          <div class="form-group">
                              <label for="webinar-name">
                                  Name
                              </label>
                              <input type="text" name="webinarname" id="webinarname" placeholder="" value="" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="webinar-date">
                                  Date
                              </label>
                              <input type="text" name="webinardate" id="webinardate" placeholder="MM-DD-YYYY" value="" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="webinar-time">
                                  Time
                              </label>
                              <input type="text" name="webinartime" id="webinartime" placeholder="XX:XX (A/P)M EST" value="" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="webinar-link">
                                  Link
                              </label>
                              <input type="text" name="webinarlink" id="webinarlink" placeholder="" value="" class="form-control">
                          </div>
                          <button class="btn btn-primary margin-top-40">Add Webinar</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="element-wrapper">
                  <div class="element-box">
                    <div class="bgc-white p-20 bd" style="margin-top:12px;">
                        <h6 class="c-grey-900">Upload Previous Webinar</h6>
                      <form action="/admin/upload/webinar" method="post">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="form-group">
                          <label for="webinar-name">
                            Name
                          </label>
                          <input type="text" name="webinarname" id="webinarname" placeholder="" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="webinar-date">
                                Date
                            </label>
                            <input type="text" name="webinardate" id="webinardate" placeholder="MM-DD-YYYY" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="webinar-link">
                                Link
                            </label>
                            <input type="text" name="webinarlink" id="webinarlink" placeholder="" value="" class="form-control">
                        </div>
                        <button class="btn btn-primary margin-top-40">Upload Webinar</button>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>

@endsection
