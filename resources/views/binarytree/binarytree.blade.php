@extends('layouts.newApp')

@section('content')

  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">`
              <div class="element-wrapper">
                <div class="element-header">
                  <h4 class="title">Your Trio Trader Network</h4>
                </div>
                <div class="element-box" style="padding: 0px !important">
                  <div id="treeContainer" style="overflow: scroll !important;">
                    <div class="tree" id="tree" style="min-width: 2400px !important;">
                        <?php
                        function printTree($downlines, $userId, $number, $users, $side, $first){
                          $number++;
                          if($number < 4){
                            echo '<ul>';
                            if(!empty($downlines['left_child'])){
                                echo '<li>';
                                echo '<a style="background-image: url(/uploads/avatars/'.$users->where('id', $downlines['left_child']['id'])->first()->avatar.')" href="/binary/get/children?parent='.base64_encode($downlines['left_child']['id']).'&side='.(isset($_GET['side']) ? $_GET['side'] : $side ).'"></a>';
                                if($downlines['left_child']['status'] === 'Active'){
                                    echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['left_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #6df723">Paid/';
                                    if($downlines['left_child']['plan_name'] == 'Sales_Rep'){
                                      echo 'Affiliate ';
                                    } else {
                                      echo 'Customer ';
                                    }
                                    echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['left_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_child']['left_volume']/150).' - '.'Right: '.($downlines['left_child']['right_volume']/150).'</p>';
                                }else{
                                    echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['left_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #eb2323">Not Paid/';
                                    if($downlines['left_child']['plan_name'] == 'Sales_Rep'){
                                      echo 'Affiliate ';
                                    } else {
                                      echo 'Customer ';
                                    }
                                    echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['left_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_child']['left_volume']/150).' - '.'Right: '.($downlines['left_child']['right_volume']/150).'</p>';
                                }

                                if($first == true){
                                    printTree($downlines['left_child'], $downlines['left_child']['id'], $number, $users, 'left', false);
                                }else{
                                    printTree($downlines['left_child'], $downlines['left_child']['id'], $number, $users, $side, false);
                                }
                                echo '</li>';
                            }else{
                                echo '<li>';
                                echo '<a style="background-image: url(/img/open3.png)" href="#"></a>';
                                echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">Open Position</p>';
                                // Removed:
                                // .'<p style="margin: 0;font-size: 12px;line-height: 16px;"></p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">0 VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left:  - '.'Right: </p>'
  //                              printTree($downline, $userId, $number);
                                echo '</li>';
                            }
                            if(!empty($downlines['right_child'])){
                                echo '<li>';
                                echo '<a style="background-image: url(/uploads/avatars/'.$users->where('id', $downlines['right_child']['id'])->first()->avatar.')" href="/binary/get/children?parent='.base64_encode($downlines['right_child']['id']).'&side='.(isset($_GET['side']) ? $_GET['side']  : $side ).'"></a>';
                                if($downlines['right_child']['status'] === 'Active'){
                                    echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['right_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #6df723">Paid/';
                                    if($downlines['right_child']['plan_name'] == 'Sales_Rep'){
                                      echo 'Affiliate ';
                                    } else {
                                      echo 'Customer ';
                                    }
                                    echo '</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['right_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['right_child']['left_volume']/150).' - '.'Right: '.($downlines['right_child']['right_volume']/150).'</p>';
                                }else{
                                    echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['right_child']['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #eb2323">Not Paid/';
                                    if($downlines['right_child']['plan_name'] == 'Sales_Rep'){
                                      echo 'Affiliate ';
                                    } else {
                                      echo 'Customer ';
                                    }
                                    echo '</p><p style="margin: 0;font-size: 12px;line-height: 16px;">'.($downlines['right_child']['volume_amount']/150).' VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['right_child']['left_volume']/150).' - '.'Right: '.($downlines['right_child']['right_volume']/150).'</p>';
                                }
                                if($first == true){
                                    printTree($downlines['right_child'], $downlines['right_child']['id'], $number, $users, 'right', false);
                                }else{
                                    printTree($downlines['right_child'], $downlines['right_child']['id'], $number, $users, $side, false);
                                }
                                echo '</li>';
                            }else{
                                echo '<li>';
                                echo '<a style="background-image: url(/img/open3.png)" href="#"></a>';
                                echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">Open Position</p>';
                                // Removed
                                // .'<p style="margin: 0;font-size: 12px;line-height: 16px;"></p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">0 VOL'.'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left:  - '.'Right: </p>'
  //                              printTree($downline, $userId, $number);
                                echo '</li>';
                            }
                            echo '</ul>';

                          }

                        }
                        echo '<ul>';
                        echo '<li>';
                        echo '<a href="#" style="background-image: url(/uploads/avatars/'.$users->where('id', $downlines['id'])->first()->avatar.')"></a>';
                        if($downlines['status'] === 'Active'){
                            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #6df723">Paid</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_volume']/150).' - '.'Right: '.($downlines['right_volume']/150).'</p>';
                        }else{
                            echo '<p style="margin: 0;font-size: 12px;line-height: 16px;">'.$downlines['username'].'</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;color: #eb2323">Not Paid</p>'.'<p style="margin: 0;font-size: 12px;line-height: 16px;">'.'Left: '.($downlines['left_volume']/150).' - '.'Right: '.($downlines['right_volume']/150).'</p>';
                        }
                        printTree($downlines, $userId, 0, $users, null, true);
                        echo '</li>';
                        echo '</ul>';
                        ?>


                    </div>
                  </div>
                </div>
                {{-- End element box for tree --}}
                <div class="element-box">
                  <div class="element-box-content">
                    <div class="row justify-content-center">
                      {{-- <div class="col"> --}}
                        <a href="/binary">
                            <button class="mr-2 mb-2 btn btn-primary">Back to top of tree</button>
                        </a>
                      {{-- </div> --}}
                      {{-- <div class="col"> --}}
                        <form method="post" action="/binary/upone">
                          {!! csrf_field() !!}
                          <input type="hidden" name="currentId" value="{{ $downlines['id'] }}">
                          <button class="mr-2 mb-2 btn btn-primary" type="submit">Up One Level</button>
                        </form>
                      {{-- </div> --}}
                      @if($leftPlacementUser !== NULL)
                      {{-- <div class="col"> --}}
                        <a href="/binary/get/children?parent={{$leftPlacementUser}}">
                            <button class="mr-2 mb-2 btn btn-primary">My Outside Left</button>
                        </a>
                      {{-- </div> --}}
                      @endif
                      @if($rightPlacementUser !== NULL)
                      {{-- <div class="col"> --}}
                        <a href="/binary/get/children?parent={{$rightPlacementUser}}">
                            <button class="mr-2 mb-2 btn btn-primary">My Outside Right</button>
                        </a>
                      {{-- </div> --}}
                      @endif
                      {{-- <div class="col"> --}}
                          <form method="post" action="/binary/search">
                              {!! csrf_field() !!}
                              <div class="input-group">
                                <input type="text" name="username" value="" placeholder="Search By Username" class="form-control">
                                <div class="input-group-btn">
                                  <button type="submit" class="btn btn-primary" style="width:80%">
                                    <span class="os-icon os-icon-ui-37"></span>
                                  </button>
                                </div>
                              </div>
                          </form>
                      {{-- </div> --}}
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <div class="element-header">
                  <h4 class="title">Overview</h4>
                </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="element-box" style="height:150px;">
                          <div style="">
                            <h5 class="text-center">Trio Trader Affiliate Link</h5>
                            <div class="text-center" style="border: 1px solid #ccc;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;">
                                <strong>triotrader.com/?sponsor={{Auth::user()->username}}</strong>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="element-box" style="height:150px;">
                        <div style="padding:20px !important;">
                            <div>
                                <h5 class="panel-title text-center">Qualified</h5>
                            </div>
                            <div class="panel-body text-center" style="border: 1px solid #ccc;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;">
                                <strong>{{($qualified) ? 'Yes! You Qualify & Your Daily Limit Is $'.$dailyLimit : 'No'}}</strong>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="element-box" style="height:150px;">
                        <div style="padding:20px !important;">
                            <div>
                                <h5 class="panel-title text-center">Left Volume</h5>
                            </div>
                            <div class="panel-body text-center" style="border: 1px solid #ccc;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;">
                                <strong>{{$downlines['left_volume'] / 150}}</strong>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="element-box" style="height:150px;">
                        <div style="padding:20px !important;">
                            <div class="panel-heading" data-background-color="green">
                                <h5 class="panel-title text-center">Right Volume</h5>
                            </div>
                            <div class="panel-body text-center" style="border: 1px solid #ccc;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;">
                                <strong>{{$downlines['right_volume'] / 150}}</strong>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
  {{-- End row --}}
  <div class="row">
    <div class="col-md-6">
      <div class="element-wrapper">
        <div class="element-header">
          <h4 class="title">Default Placement Settings</h4>
        </div>
        <div class="element-box">
          <div style="padding:20px !important;">
            <div>
                <h5 class="panel-title text-center">Placement Direction</h5>
            </div>
            <form action="/binary/updatedirection" method="POST">
              {{ csrf_field() }}
              <select style="width:100%;" name="direction">
                @if(Auth::user()->default_position == 'Left')
                  <option value="Left">Outside Bottom Left</option>
                  <option value="Right">Outside Bottom Right</option>
                @else
                  <option value="Right">Outside Bottom Right</option>
                  <option value="Left">Outside Bottom Left</option>
                @endif
              </select>
            </br></br>
              <button class="btn btn-primary" type="submit" id="updateBranch" style="width:100%;">Update</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End row --}}
</div>
</div>
</div>
</div>
    <script>
        $(document).ready(function(){
            var outer=document.getElementById('treeContainer').offsetWidth;
            var inner=document.getElementById('tree').offsetWidth;
            $('#treeContainer').scrollLeft((inner - outer)/2);
        });
    </script>
@endsection
