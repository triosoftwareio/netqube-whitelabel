@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Upgrade to Pro Package
                </h6>
                <div class="element-box">
                    <div class="row">
                      <h6> Your total is ${{ $amount }}USD which in btc at the current price of ${{ $currentBtcPrice }} per BTC is {{ $costBtc }}<h6>
                        <br>
                      <img src="{{Auth::user()->payment_address}}" style="display:block;margin:auto;"/>
                      <br>

                      <h6>Send exactly {{$costBtc}} BTC  to {{strstr(substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "bitcoin:") + 8), '?amount=', true)}}</h6>
                      <h6 style="color:red !important;">If you are sending from an exchange please ensure the amount received (after fees) is equal to or higher than the amount above</h6>
                      <h4 style="color:red !important;">If the amount received is less than the listed amount your account will not be activated</h4>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
@endsection
