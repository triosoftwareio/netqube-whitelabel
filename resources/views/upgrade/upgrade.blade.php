@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Upgrade to Pro Package
                </h6>
                <div class="element-box">
                    <div class="row">
                      @if(!$pending)
                      <form class="form-horizontal" role="form" method="POST" action="/upgrade/generate">
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class="col-md-8 control-label">Packages</label>

                              <div class="col-md-12">
                                <h4>Current Package: <span style="font-size:14px;">${{$currentPackage}}</span></h4>
                              </br>
                                <h4>Other Packages:</h4>
                                  <select name="plan" class="form-control">
                                    @foreach($plans as $plan)
                                      <option value="{{$plan->id}}">{{$plan->name." ".$plan->amount." BTC"}}</option>
                                    @endforeach
                                  </select>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-8 col-md-offset-4">
                                  <button type="submit" class="btn btn-primary">
                                      Generate New Payment
                                  </button>
                              </div>
                          </div>
                      </form>
                      @endif
                    </div>
                    <div class="row">
                      @if($pending)
                        <h4 style="text-align:center;">Complete your deposit to upgrade.</h4>
                        <h5 style="text-align:center;">Payment accepted in Bitcoin.</h5>
                        <h6 style="text-align:center;">Payment could take up to 2 hours to process keep refreshing the page to check if it went through, increasing the transaction fee will allow payments to proccess within 15 minutes.</h6>
                        <img src="/{{Auth::user()->payment_address}}" style="display:block;margin:auto;width:256px;"/>
                        <h6 style="text-align:center;">QR Code Not Working?</h6>
                        <h6 style="text-align:center;text-transform:initial !important;">Send exactly {{$amount}} BTC  to {{strstr(substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "bitcoin:") + 8), '?amount=', true)}}</h6>
                      @endif
                    </div>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
@endsection
