@extends('layouts.app')

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="card">
                          <div class="card-header" data-background-color="green">
                              <h4 class="title">Transactions</h4>
                              <p class="category">These are daily profits and transactions added to your account balance based on your package and roi</p>
                          </div>
                          <div class="card-content table-responsive">
                              <table class="table table-hover">
                                  <thead class="text-warning">
                                    <th>Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                  </thead>
                                  <tbody>
                                    @foreach($transactions as $transaction)
                                      <tr>
                                        <td>{{$transaction->id}}</td>
                                        <td>{{$transaction->amount}}</td>
                                        <td>{{$transaction->created_at}}</td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
        </div>
      </div>
    </div>
  </div>

@endsection
