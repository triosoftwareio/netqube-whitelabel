@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @elseif(isset($success))
                    <div class="alert alert-success">
                        <strong>Great!</strong> Your Api Key, Secret & Risk Amount has been updated.
                    </div>
                @endif
                <h6 class="element-header">
                  Exchange Keys
                </h6>
                <div class="row">
                  {{-- <div class="col">
                    <div class="element-box el-tablo">
                      <h6> Poloniex </h6>
                      <form action="/settings/api-keys/poloniex/update" method="POST">
                          {!! csrf_field() !!}
                            <fieldset>
                                <!-- Password input-->
                                  <div class="form-group label-floating">
                                      <label class="control-label">Poloniex API Key</label>
                                      <input id="apiKey" name="apiKey" type="text" placeholder="" class="form-control input-md" value="{{($poloniex) ? $poloniex->api_key : ''}}">
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">Poloniex API Secret</label>
                                      <input id="apiSecret" name="apiSecret" type="password" placeholder="" class="form-control input-md" value="{{($poloniex) ? $poloniex->api_secret : ''}}">
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">Amount to Risk</label>
                                      <input id="btcRisk" name="btcRisk" type="text" placeholder="Ex. 0.01" class="form-control input-md" value="{{($poloniex) ? $poloniex->risk_amount : ''}}">
                                  </div>
                                  <div class="form-group">
                                      <input type="submit" id="updateCredentialsPolo" class="btn btn-primary btn-lg full-width" value="Update Poloniex Credentials">
                                  </div>
                            </fieldset>
                      </form>
                    </div>
                  </div> --}}

                  <div class="col">
                    <div class="element-box el-tablo">
                      <h6> Bittrex </h6>
                      <form action="/settings/api-keys/bittrex/update" method="POST">
                          {!! csrf_field() !!}
                            <fieldset>
                                  <div class="form-group label-floating">
                                      <label class="control-label">Bittrex API Key</label>
                                      <input id="apiKey" name="apiKey" type="text" placeholder="" class="form-control input-md" value="{{($bittrex) ? $bittrex->api_key : ''}}">
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">Bittrex API Secret</label>
                                      <input id="apiSecret" name="apiSecret" type="password" placeholder="" class="form-control input-md" value="{{($bittrex) ? $bittrex->api_secret : ''}}">
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">$ Amount to Risk In USD</label>
                                      <input id="btcRisk" name="btcRisk" type="text" placeholder="Ex. 10" class="form-control input-md" value="{{($bittrex) ? $bittrex->risk_amount : ''}}">
                                  </div>
                                  <div class="form-group">
                                      <input type="submit" id="updateCredentialsPolo" class="btn btn-primary btn-lg full-width" value="Update Bittrex Credentials">
                                  </div>
                            </fieldset>
                      </form>
                    </div>
                  </div>

                  <div class="col">
                    <div class="element-box el-tablo">
                      <h6> HitBTC </h6>
                      <form action="/settings/api-keys/hitbtc/update" method="POST">
                          {!! csrf_field() !!}
                            <fieldset>
                                <!-- Password input-->
                                  <div class="form-group label-floating">
                                      <label class="control-label">HitBTC API Key</label>
                                      <input id="apiKey" name="apiKey" type="text" placeholder="" class="form-control input-md" value="{{($hitbtc) ? $hitbtc->api_key : ''}}">
                                      {{-- {{ $data->apiKey }} --}}
                                      {{-- <span class="help-block">Check Poloniex API Key Settings</span> --}}
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">HitBTC API Secret</label>
                                      <input id="apiSecret" name="apiSecret" type="password" placeholder="" class="form-control input-md" value="{{($hitbtc) ? $hitbtc->api_secret : ''}}">
                                      {{-- {{ $data->apiSecret }} --}}
                                      {{-- <span class="help-block">Check Poloniex API Key Settings</span> --}}
                                  </div>
                                  <div class="form-group label-floating">
                                      <label class="control-label">$ Amount to Risk In USD</label>
                                      <input id="btcRisk" name="btcRisk" type="text" placeholder="Ex. 10" class="form-control input-md" value="{{($hitbtc) ? $hitbtc->risk_amount : ''}}">
                                  </div>
                                  <div class="form-group">
                                      <input type="submit" id="updateCredentialsPolo" class="btn btn-primary btn-lg full-width" value="Update HitBTC Credentials">
                                  </div>
                            </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
                {{-- End Inner Row --}}

                <br><br>
                <div class="row">
                  <div class="col">
                    <h6 class="element-header">
                      Setup Instructions
                    </h6>
                    <div class="row">
                      <div class="col">
                        <div class="element-box el-tablo">
                          <h6>Step 1:</h6>
                          <p>
                            <b>1.1:</b> Log into exchange account (Poloniex, HitBTC or Bittrex).<br>
                            <b>1.2:</b> Access <b>Account Settings Page</b> or <b>API Keys Page</b> on the exchange
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="element-box el-tablo">
                          <h6>Step 2:</h6>
                          <p>
                            <b>2.1:</b> Generate new API Keys<br>
                            <b>2.2:</b> Ensure the following API Key Settings:<br>
                            <b>Allow Trading</b><br>
                            <b>Allow Any Access from any IP</b><br>
                            <b>DO NOT allow withdrawls</b><br>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="element-box el-tablo">
                          <h6>Step 3:</h6>
                          <p>
                            <b>3.1:</b> Fill in the field above for the respective exchange<br>
                            <b>3.2:</b> Double check to ensure keys are correct<br>
                            <b><u>IF YOU ARE TRADING ON HITBTC READ BELOW</u></b><br>
                            Ensure that funds are in your <b>TRADING</b> account not the main account otherwise you will get an error for lack of funds
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- End 2nd inner row --}}
              </div>
            </div>
          </div>
          {{-- End Outer Row --}}

        </div>
      </div>
    </div>
  </div>

@endsection
