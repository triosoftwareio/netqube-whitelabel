<tr>
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
        <img src="https://www.triotrader.com/public/img/ttraderlogo.png" style="margin-left:auto;margin-right:auto;display:block;width:250px;">
    </td>
</tr>
