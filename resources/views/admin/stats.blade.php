@extends('layouts.newApp')

@section('content')
  {{-- @if(Auth::check() && Auth::user()->status == 'Admin') --}}
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Basic Stats
                </h6>
                <div class="element-content">
                  <div class="row">
                    <div class="col">
                      <div class="element-box el-tablo">
                        <div class="label">
                          Total Users
                        </div>
                        <div class="value">
                          {{ $numUsers }} <small>Total</small>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      <div class="element-box el-tablo">
                        <div class="label">
                          Active Users
                        </div>
                        <div class="value">
                          {{ $activeUserCount }}
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      <div class="element-box el-tablo">
                        <div class="label">
                          Active Customer Accounts
                        </div>
                        <div class="value">
                          {{ $customerCount }}
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      <div class="element-box el-tablo">
                        <div class="label">
                          Active Affiliate Accounts
                        </div>
                        <div class="value">
                          {{ $affiliateCount }}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
        {{-- End Row --}}
        <div class="row">
          <div class="col">
            <div class="element-box">
              <div class="row">
                <div class="col">
                <?php
                  $percentage = ($activeUserCount/$numUsers)*100;
                ?>
                  <div class="c100 center p{{$percentage}}">
                      <span>{{round($percentage)}}%</span>
                      <div class="slice">
                          <div class="bar"></div>
                          <div class="fill"></div>
                      </div>
                  </div>
                  <h6 class="text-center">Active Accounts</h6>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="element-box">
              <div class="row">
                <div class="col">
                <?php
                  $percentage2 = ($customerCount/$activeUserCount)*100;
                ?>
                  <div class="c100 center p{{$percentage2}}">
                      <span>{{round($percentage2)}}%</span>
                      <div class="slice">
                          <div class="bar"></div>
                          <div class="fill"></div>
                      </div>
                  </div>
                  <h6 class="text-center">Customer Accounts</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- End Percentage Box Row --}}

    </div>
  </div>
  {{-- @endif --}}
@endsection
