@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col">
              <div class="element-wrapper">
                <h3 class="element-header">
                  Admin
                </h3>
                <div class="row">
                  <div class="col">
                    <div class="element-box">
                      <a href="/admin/users/list">
                        List Users
                      </a>
                    </div>
                  </div>
                  <div class="col">
                    <div class="element-box">
                      <a href="/admin/create/mail">
                        Create Update/Summary
                      </a>
                    </div>
                  </div>
                  <div class="col">
                    <div class="element-box">
                      <a href="/admin/upload/file">
                        Upload Update/Summary
                      </a>
                    </div>
                  </div>
                  <div class="col">
                    <div class="element-box">
                      <a href="/admin/stats">
                        Basic Stats
                      </a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="element-box">
                      <a href="/admin/email/users">
                        Email All Users
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
