@extends('layouts.newApp')

{{-- Admin Page --}}

@section('content')

<div class="content-w">
  <div class="container-fluid" style="min-height:1200px;">
    <div class="content-i">
      <div class="content-box">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="element-wrapper">
              <h3 class="element-header">
                Users
              </h3>
              <div class="element-box table-responsive">
                <div class="list-group">
                  @foreach($users as $user)
                    <a href="/admin/user/edit/{{$user->id}}" class="list-group-item">{{$user->username}} <span class="pull-right"> Edit User</span></a>
                    &nbsp&nbsp
                  @endforeach
                </div>
              </br>
                {{ $users->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
