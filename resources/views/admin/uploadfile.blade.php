@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col">
              <div class="element-wrapper">
                <h3 class="element-header">
                  Upload File
                </h3>
                <div class="row">
                  <div class="col-md-6">
                    <div class="element-box">
                      <form method="post" action="/admin/upload/summary" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        Select File to Upload:<br><br>
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <br><br>
                        <button class="btn btn-success" type="submit">Upload Summary</button>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="element-box">
                      <form method="post" action="/admin/upload/update" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        Select File to Upload:<br><br>
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <br><br>
                        <button class="btn btn-success" type="submit">Upload Update</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
