@extends('layouts.newApp')


@section('content')

<div class="content-w">
  <div class="container-fluid" style="min-height:1200px;">
    <div class="content-i">
      <div class="content-box">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="element-wrapper">
              <div class="element-header">
                  <h3>User: {{ $user->name }}</h3>
              </div>
              <div class="card-content table-responsive">
                <form method="post" action="/admin/user/edit/info/{{$user->id}}">
                  {!! csrf_field() !!}
                    <input type="password" name="newpassword" placeholder="New Password" class="form-control">
                    <button class="btn btn-success" type="submit">Update Account Password</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
