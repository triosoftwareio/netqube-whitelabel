@extends('layouts.newApp')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div id="short-tab" class="panel panel-default">
        <div class="panel-body">
          <form id="webinarInfoForm">
            <input type="hidden" value="{{ csrf_token() }}" name="_token">
            <div class="form-group">
                <label for="webinar-name">
                    Name
                </label>
                <input type="text" name="webinarname" id="webinarname" placeholder="" value="{{ $webinar->name }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="webinar-date">
                    Date
                </label>
                <input type="text" name="webinardate" id="webinardate" placeholder="MM-DD-YYYY" value="{{ $webinar->date }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="webinar-link">
                    Link
                </label>
                <input type="text" name="webinarlink" id="webinarlink" placeholder="" value="{{ $webinar->link }}" class="form-control">
            </div>
          </form>
          <button class="btn-settings btn" id="submitWebinar">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$('#submitWebinar').click(function(){
  $.ajax({
    url: '/admin/prevwebinar/update/{{$webinar->id}}',
    data: $('#webinarInfoForm').serialize(),
    type: 'POST',
    dataType: 'json',
    success: function(response){
      alert(response);
      console.log(response);
    },
    error: function(error){
      alert(error);
      console.log(error.responseText);
    }
  });
});
</script>
@endsection
