@extends('layouts.newApp')

@section('content')
  {{-- @if(Auth::check() && Auth::user()->status == 'Admin') --}}
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-10">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Send Email
                </h6>
                <div class="element-box">
                  <div style="padding:20px !important;">
                    <form method="post" action="/admin/send/new/summary">
                      {!! csrf_field() !!}

                      <input type="text" name="title" placeholder="Title: Ex. 'A Complete Summary of *CoinName* v1'" class="form-control">
                      <input type="text" name="date" placeholder="Date: Ex. 'March 31st, 2018 (2:00PM EST)'" class="form-control">
                      <select name="summary" class="form-control">
                        @foreach($summaries as $summary)
                          <option value="{{ $summary->filePath }}">{{ $summary->name }}</option>
                        @endforeach
                      </select>
                      <br>
                      <button class="btn btn-primary" type="submit" name="preview" onclick="return emailClick(this)">Preview Email</button>
                      <button class="btn btn-primary" type="submit" name="send" onclick="return emailClick(this)">Send Email</button>
                      <input type="hidden" name="clickedButton" id="clickedButton" value="">
                    </form>
                  </div>
              </div>
            </div>
          </div>
        </div>

          <div class="row">
            <div class="col-md-10">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Send Update
                </h6>
                <div class="element-box">
                  <div style="padding:20px !important;">
                    <form method="post" action="/admin/send/new/update">
                      {!! csrf_field() !!}
                      <input type="text" name="title" placeholder="Title: Ex. 'An update for *COINNAME* v1'" class="form-control">
                      <input type="text" name="date" placeholder="Date: Ex. 'March 31st, 2018 (2:00PM EST)'" class="form-control">
                      <select name="summary" class="form-control">
                        @foreach($updates as $update)
                          <option value="{{ $update->filePath }}">{{ $update->name }}</option>
                        @endforeach
                      </select>
                      <br>
                      <button class="btn btn-primary" type="submit" name="preview" onclick="return emailClick(this)">Preview Email</button>
                      <button class="btn btn-primary" type="submit" name="send" onclick="return emailClick(this)">Send Email</button>
                      <input type="hidden" name="clickedButton" id="clickedButton" value="">
                    </form>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  {{-- @endif --}}
  <script>
    function emailClick(btn){
      document.getElementById('clickedButton').value = btn.name;
      return true;
    }
  </script>
@endsection
