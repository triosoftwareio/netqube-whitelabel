@extends('layouts.newApp')

@section('content')
  @if(Auth::check() && Auth::user()->status == 'Admin')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Mail All Users
                </h6>
                <div class="element-box">
                  <div style="padding:20px !important;">
                    <form method="post" action="/admin/send/all/users">
                      {!! csrf_field() !!}

                      <input type="text" name="subject" placeholder="Subject" class="form-control">
                      <textarea name="content" class="form-control" rows="25"></textarea>

                      <br>
                      <button class="btn btn-primary" type="submit" name="send" onclick="return emailClick(this)">Send Email</button>
                      <input type="hidden" name="clickedButton" id="clickedButton" value="">
                    </form>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="element-wrapper">
              <h6 class="element-header">
                Mail Active Users
              </h6>
              <div class="element-box">
                <div style="padding:20px !important;">
                  <form method="post" action="/admin/send/active/users">
                    {!! csrf_field() !!}

                    <input type="text" name="subject" placeholder="Subject" class="form-control">
                    <textarea name="content" class="form-control" rows="25"></textarea>

                    <br>
                    <button class="btn btn-primary" type="submit" name="send" onclick="return emailClick(this)">Send Email</button>
                    <input type="hidden" name="clickedButton" id="clickedButton" value="">

                  </form>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  <script>
    function emailClick(btn){
      document.getElementById('clickedButton').value = btn.name;
      return true;
    }
  </script>
@endsection
