@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Fill out the form to get access to your back office. If you have already paid you will not be charged. ($129.98)
                </h6>
                <div class="row">
                  <div class="col">
                    <div class="element-box">
                      {{-- <div id="sq-ccbox"> --}}
                        <!--
                          You should replace the action attribute of the form with the path of
                          the URL you want to POST the nonce to (for example, "/process-card")
                        -->
                        <form>
                            {{-- <img src="/img/ajax-loader.gif" class="loaderImage" style="display:none;"> --}}
                            <div class="form-group">
                              <label for="authCardholderFirstName">
                                Cardholder First Name
                              </label>
                              <input type="text" name="authCardholderFirstName" id="authCardholderFirstName" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="authCardholderLastName">
                                Cardholder Last Name
                              </label>
                              <input type="text" name="authCardholderLastName" id="authCardholderLastName" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="AuthAddress">
                                Address
                              </label>
                              <input type="text" name="AuthAddress" id="AuthAddress" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="AuthCity">
                                City
                              </label>
                              <input type="text" name="AuthCity" id="AuthCity" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="AuthState">
                                State/Province
                              </label>
                              <input type="text" name="AuthState" id="AuthState" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="AuthCountry">
                                Country
                              </label>
                              <input type="text" name="AuthCountry" id="AuthCountry" placeholder="" value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <label for="AuthZip">
                                Zip
                              </label>
                              <input type="text" name="AuthZip" id="AuthZip" placeholder="" value="" class="form-control"><br><br>
                            </div>
                            <div class="form-group">
                              <label for="cardNumberID">
                                Card Number
                              </label>
                              <input class="form-control" type="tel" id="cardNumberID" placeholder="5424000000000015" autocomplete="off" />
                            </div>
                            <div class="form-group">
                              <label for="monthID">
                                Expiration Date (Month)
                              </label>
                              <input class="form-control" type="text" id="monthId" placeholder="12" value="12" />
                            </div>
                            <div class="form-group">
                              <label for="yearID">
                                Expiration Date (Year)
                              </label>
                              <input class="form-control" type="text" id="yearId" placeholder="2025" value="2025" />
                            </div>
                            <div class="form-group">
                              <label for="yearID">
                                Card Security Code / CVV
                              </label>
                              <input class="form-control" type="text" id="cardCodeID" placeholder="123" />
                            </div>
                            {{-- <div class="form-group" style="display:none;">
                              <label for="auth1-membership-plan">
                                Plan
                              </label>
                              <select id="auth1-membership-plan" name="auth_sub_id" class="form-control selected-package" required style="display:none;">
                                <option value="signals">Signals - $55 One Time Fee - $45 Monthly Subscription</option>
                                <option value="basic">Basic - $200 One Time Fee - $145 Monthly Subscription</option>
                                <option value="cps">Professional - $200 One Time Fee - $160 Monthly Subscription</option>
                                <option value="autotrader">Auto Trader - $225 One Time Fee - $185 Monthly Subscription</option>
                              </select>
                            </div> --}}
                            {{-- <div class="form-group">
                              <label for="authCoupon">
                                Coupon
                              </label>
                              <input class="form-control" type="text" id="authCoupon" placeholder="00000" value="vol200" readonly="readonly"/>
                            </div>
                            <div class="form-group">
                              <p>
                                New Price After Discount: $0.00
                              </p>
                            </div> --}}

                            <input class="form-control" type="text" id="auth1-membership-plan" value="QubeTrader" />
                            <!-- On submit, cause this data to be sent to the "sendPaymentDataToAnet()" function. -->
                            <button class="btn btn-primary" type="button" id="submitButton" onclick="sendPaymentDataToAnet()">Pay</button>
                        </form>
                        {{-- <h6>Payment processing is currently unavailable. We will be back shortly</h6> --}}
                        {{-- <h6>Thank you for your patience</h6> --}}
                      </div>

                      {{-- <div id="sq-walletbox">
                        Pay with a Digital Wallet
                        <div id="sq-apple-pay-label" class="wallet-not-enabled">Apple Pay for Web not enabled</div>
                        <!-- Placeholder for Apple Pay for Web button -->
                        <button id="sq-apple-pay" class="button-apple-pay"></button>

                        <div id="sq-masterpass-label" class="wallet-not-enabled">Masterpass not enabled</div>
                        <!-- Placeholder for Masterpass button -->
                        <button id="sq-masterpass" class="button-masterpass"></button>
                      </div> --}}

                    {{-- </div> --}}
                  </div>


                </div>
              </div>
            </div>
          </div>
          {{-- End Row --}}



        </div>
      </div>
    </div>
  </div>



  <script type="text/javascript">
      function sendPaymentDataToAnet() {
          var secureData = {}; authData = {}; cardData = {};
          console.log('test');

          // Extract the card number, expiration date, and card code.
          cardData.cardNumber = $('#cardNumberID').val();
          cardData.month = $("#monthId").val();
          cardData.year = $("#yearId").val();
          cardData.cardCode = $("#cardCodeID").val();
          secureData.cardData = cardData;

          // The Authorize.Net Client Key is used in place of the traditional Transaction Key. The Transaction Key
          // is a shared secret and must never be exposed. The Client Key is a public key suitable for use where
          // someone outside the merchant might see it.
          authData.clientKey = "{{ $merchantFrontKey }}";
          authData.apiLoginID = "{{ $merchantKey }}";
          secureData.authData = authData;

          // Pass the card number and expiration date to Accept.js for submission to Authorize.Net.
          var responseAccept = Accept.dispatchData(secureData, responseHandler);

          // Process the response from Authorize.Net to retrieve the two elements of the payment nonce.
          // If the data looks correct, record the OpaqueData to the console and call the transaction processing function.
          function responseHandler(response) {

              if (response.messages.resultCode === 'Error') {
                console.log('error');

                  for (var i = 0; i < response.messages.message.length; i++) {

                      console.log(response.messages.message[i].code + ':' + response.messages.message[i].text);

                      $("#blob").val(response.messages.message[i].code + ':' + response.messages.message[i].text);

                  }

              } else {
                console.log('no error');
                  useOpaqueData(response.opaqueData)

              }

          }


          function useOpaqueData(responseData) {

              // console.log(responseData.dataDescriptor);
              //
              // console.log(responseData.dataValue);
              $('.loaderImage').show();
              $.ajax({
                  type: "POST",
                  url: "/authnet",
                  data: {
                      '_token': "{{csrf_token()}}",
                      'nonce': responseData.dataValue,
                      'firstName': $('#authCardholderFirstName').val(),
                      'lastName': $('#authCardholderLastName').val(),
                      'address': $('#AuthAddress').val(),
                      'city': $('#AuthCity').val(),
                      'state': $('#AuthState').val(),
                      'country': $('#AuthCountry').val(),
                      'zip': $('#AuthZip').val(),
                      'package': $('#auth1-membership-plan').val(),
                      'initialCoupon': $('#authCoupon').val(),
                      'cardNumber': $('#cardNumberID').val(),
                      'monthId': $('#monthId').val(),
                      'yearId': $('#yearId').val()
                  },
                  async: false,
                  dataType: 'json',
                  success: function (data) {
                      // console.log(data);
                      $('.loaderImage').hide();
                      var prompt = window.confirm(data);
                      if (prompt) {
                          document.location.href = "/home";
                      }
                  },
                  error: function (err) {
                      console.log(err.responseText);
                      $('.loaderImage').hide();
                      alert(err.responseText);
                  }
              });

          }
      }
  </script>
@endsection


{{--
<table>
<tbody>
  <tr>
    <td>Card Number:</td>
    <td><div id="sq-card-number"></div></td>
  </tr>
  <tr>
    <td>CVV:</td>
    <td><div id="sq-cvv"></div></td>
  </tr>
  <tr>
    <td>Expiration Date: </td>
    <td><div id="sq-expiration-date"></div></td>
  </tr>
  <tr>
    <td>Postal Code:</td>
    <td><div id="sq-postal-code"></div></td>
  </tr>
  <tr>

    <td colspan="2">
      button-credit-card
      <button id="sq-creditcard" class="btn btn-primary" onclick="requestCardNonce(event)">
        Pay with card
      </button>
    </td>


  </tr>
</tbody>
</table> --}}

{{-- <div class="form-group">
  <label for="">Card Number:</label>
  <div class="form-control" id="sq-card-number"></div>
</div>
<div class="form-group">
  <label for="">CVV:</label>
  <div class="form-control" id="sq-cvv"></div>
</div>
<div class="form-group">
  <label for="">Expiration Date:</label>
  <div class="form-control" id="sq-expiration-date"></div>
</div>
<div class="form-group">
  <label for="">Postal Code</label>
  <div class="form-control" id="sq-postal-code"></div>
</div>

<button id="sq-creditcard" class="btn btn-primary" onclick="requestCardNonce(event)">
  Pay with card
</button> --}}

{{-- <form id="nonce-form" novalidate action="/process/payment" method="post">
  {{ csrf_field() }}
  <h6>Pay with a Credit Card - You will be charged upon submitting this form ($25)</h6>
  <table>
  <tbody>
    <tr>
      <td>Card Number:</td>
      <td><div id="sq-card-number"></div></td>
    </tr>
    <tr>
      <td>CVV:</td>
      <td><div id="sq-cvv"></div></td>
    </tr>
    <tr>
      <td>Expiration Date: </td>
      <td><div id="sq-expiration-date"></div></td>
    </tr>
    <tr>
      <td>Postal Code:</td>
      <td><div id="sq-postal-code"></div></td>
    </tr>
    <tr>

      <td colspan="2">
        <button id="sq-creditcard" class="btn btn-primary" onclick="requestCardNonce(event)">
          Pay with card
        </button>
      </td>


    </tr>
  </tbody>
  </table> --}}


  <!--
    After a nonce is generated it will be assigned to this hidden input field.
  -->
  {{-- <input type="hidden" id="card-nonce" name="nonce">
</form> --}}
