@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">

      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Select Package
                </h6>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-4" style="background:white;">
                        <div class="project-slide">
                            <div class="project-media-w">
                                <div class="project-media">
                                    <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                                </div>
                            </div>
                            <div class="project-content">
                              <div class="project-title">
                                Pro Package / 1 Month - $100 (BTC)
                              </div>
                              <div class="project-text" style="height:269px">
                                  <p>Live Trading Webinars</p>
                                  <p>Training Videos</p>
                                  <p>Weekly Summaries/Updates</p>
                                  <p>Professional automated signals</p>
                                  <p><b>Trio Trader</b></p>
                                  <p>Affiliate package</p>
                              </div>
                              <div class="project-icons-buton">
                                  <a class="btn btn-primary" href="/generate/imported/user/wallet/Pro_Monthly/100">Select</a>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-4" style="background:white;">
                        <div class="project-slide">
                            <div class="project-media-w">
                                <div class="project-media">
                                    <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                                </div>
                            </div>
                            <div class="project-content">
                              <div class="project-title">
                                Pro Package / 6 Months - $600 (BTC)
                              </div>
                              <div class="project-text" style="height:269px">
                                  <p>Live Trading Webinars</p>
                                  <p>Training Videos</p>
                                  <p>Weekly Summaries/Updates</p>
                                  <p>Professional automated signals</p>
                                  <p>Affiliate package</p>
                                  <p><b>Trio Trader</b></p>
                              </div>
                              <div class="project-icons-buton">
                                  <a class="btn btn-primary" href="/generate/imported/user/wallet/Pro_Semi/600">Select</a>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-4" style="background:white;">
                        <div class="project-slide">
                            <div class="project-media-w">
                                <div class="project-media">
                                    <img src="/img/ttraderlogo.png" width="256px" style="display:block;margin:auto;">
                                </div>
                            </div>
                            <div class="project-content">
                              <div class="project-title">
                                Pro Package / 12 Months - $1000 (BTC)
                              </div>
                              <div class="project-text" style="height:269px">
                                  <p>Live Trading Webinars</p>
                                  <p>Training Videos</p>
                                  <p>Weekly Summaries/Updates</p>
                                  <p>Professional automated signals</p>
                                  <p>Affiliate package</p>
                                  <p><b>Trio Trader</b></p>
                              </div>
                              <div class="project-icons-buton">
                                  <a class="btn btn-primary" href="/generate/imported/user/wallet/Pro_Annual/1000">Select</a>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>

                    <br>
                    <p class="text-center">&nbsp&nbspIf you would like to join as an Affiliate only - no content or signals -
                        <a href="/generate/imported/user/wallet/Sales_Rep/97">Click Here</a>
                    </p>

                  </div>
                </div>
              </div>
            </div>
          </div>
          {{-- End Row --}}

        </div>
      </div>
    </div>
  </div>

@endsection
