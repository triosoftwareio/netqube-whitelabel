@extends('layouts.newApp')

@section('content')
  <div class="content-w">
    <div class="container-fluid" style="min-height:1200px;">
      <div class="content-i">
        <div class="content-box">
          <div class="row">
            <div class="col-md-12">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Complete Payment For Access To Your Back Office!
                </h6>
                <div class="row">
                  <div class="col">
                    <div class="element-box">
                      <h6>{{ $message }}</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
