import io from 'socket.io-client';
import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;

$(document).ready(function(){
  console.log('everything loaded');
  /**
   * First we will load all of this project's JavaScript dependencies which
   * includes Vue and other libraries. It is a great starting point when
   * building robust, powerful web applications using Vue and Laravel.
   */

  require('./playSound');

  var Switchery = require('switchery');
  //
  // if($('.js-switch').length > 0){
  //   var elem = document.querySelector('.js-switch');
  //   var init = new Switchery(elem);
  // }

  // window.cron = io('https://billionbitclub.com');
  // window.cron = io('http://swipe-frontend.local');
  window.cron = io('https://cryptotrader.trio.software', {transports: ['websocket'], upgrade: false});

  let pair;
  let exchange;
  let signalId;
  let executeTradeBtn;
  let sellOrderId;
  let executeSellBtn;

  cron.on('connect', () => {
      console.log('Connected');
  });

  cron.on('poloniexPrices', function(poloniexPrices){
      for(let pair in poloniexPrices){
          $('.'+pair).each(function(){
              let percentage = (((poloniexPrices[pair]/$(this).data('starting-price'))-1)*100);
              $(this).css('width', percentage/$(this).data('target')*100+'%');
              if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) > parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-success');
                  $(this).parent().parent().find('span').addClass('badge-danger');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }else if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) < parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-danger');
                  $(this).parent().parent().find('span').addClass('badge-success');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }
          });
      }
  });

  cron.on('hitbtcPrices', function(hitbtcPrices){
      for(let pair in hitbtcPrices){
          $('.'+pair).each(function(){
              let percentage = (((hitbtcPrices[pair]/$(this).data('starting-price'))-1)*100);
              $(this).css('width', percentage/$(this).data('target')*100+'%');
              if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) > parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-success');
                  $(this).parent().parent().find('span').addClass('badge-danger');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }else if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) < parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-danger');
                  $(this).parent().parent().find('span').addClass('badge-success');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }
          });
      }
  });

  cron.on('bittrexPrices', function(bittrexPrices){
      for(let pair in bittrexPrices){
          $('.'+pair).each(function(){
              let percentage = (((bittrexPrices[pair]/$(this).data('starting-price'))-1)*100);
              $(this).css('width', percentage/$(this).data('target')*100+'%');
              if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) > parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-success');
                  $(this).parent().parent().find('span').addClass('badge-danger');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }else if(parseFloat($(this).parent().parent().find('span').text()).toFixed(2) < parseFloat(percentage).toFixed(2)){
                  $(this).parent().parent().find('span').removeClass('badge-danger');
                  $(this).parent().parent().find('span').addClass('badge-success');
                  $(this).parent().parent().find('span').text(parseFloat(percentage).toFixed(2)+'%');
              }
          });
      }
  });


  cron.on('scannerLog', function(scannerLog){
      $.playSound('https://notificationsounds.com/soundfiles/49ae49a23f67c759bf4fc791ba842aa2/file-sounds-847-office-2.mp3');

      var botImage = '/img/up.png';

      var currentNotification = $('<li><img class="width-32" style="margin-right:5px;" src="'+botImage+'" />'+scannerLog.name+': Scanning For Signals</li>');
      $(".notifications").prepend(currentNotification);
      setTimeout(function(){
          $(currentNotification).remove();
      }, 5000);
  });

  cron.on('newSignal', function(signal){
      $.playSound('https://notificationsounds.com/soundfiles/49ae49a23f67c759bf4fc791ba842aa2/file-sounds-847-office-2.mp3');

      if(signal.type === 'Buy'){
          var imageSignal = '/img/up.png';
      }else{
          var imageSignal = '/img/down.png';
      }
      var sellPriceRaw = parseFloat(signal.price)+((parseFloat(signal.price)*parseFloat(signal.shortLow))/10);

      var sellPrice = sellPriceRaw.toFixed(8);
      var date = new Date();
      var currentSignal = $('<li><img class="width-32" style="margin-right:5px;" src="'+imageSignal+'" />'+signal.message+' sell at '+sellPrice+'<br>'+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()+'<br> Exchange: '+signal.exchange+'</li>');
      $(".notifications").prepend(currentSignal);
      setTimeout(function(){
          $(currentSignal).remove();
      }, 20000);
  });

  Window.cryptoRates = {
      BTCUSD: 0,
      ETHUSD: 0,
      BCHUSD: 0,
      LTCUSD: 0
  };

  $('.execute-trade').click(function(){
      executeTradeBtn = $(this);
      $('#swipe-modal').modal('toggle');
      pair = $(this).data('pair');
      $("#currentPairToTrade").val(pair);
      exchange = $(this).data('exchange');
      signalId = $(this).data('signalid');
  });

  $('.sell-trade').click(function(){
    executeSellBtn = $(this);
    $('#sell-modal').modal('toggle');
    sellOrderId = $(this).data('sellorderid');
    exchange = $(this).data('exchange');
    console.log(sellOrderId);
  });

  $('#executeTrade').click(function(){
      $('#swipe-modal').modal('toggle');
      $('#notification-title').text('Trade Executing');
      $('#notification-body-text').text('Please stand by while we execute your trade.');
      $('#notification-modal').modal('toggle');
      $.ajax({
          type: "POST",
          url: '/trade/execute',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {pair: pair, exchange: exchange, signalId: signalId, percentageAmount: $('#percentageAmount').val()},
          success: function( response ) {
              // console.log('exchange: ' + exchange);
              console.log(response);
              $('#notification-modal').modal('hide');
              if(response.error){
                  $('#notification-title').text('Trade Warning');
                  $('#notification-body-text').text(response.data.message);
                  setTimeout(function(){
                      $('#notification-modal').modal('show');
                  }, 2000);
              }else{
                  $('#notification-title').text('Successfully Executed Trade');
                  $('#notification-body-text').text(response.data.message);
                  setTimeout(function(){
                      $('#notification-modal').modal('show');
                  }, 2000);
              }

          },
          error: function( err ){
              $('#notification-modal').modal('hide');
              alert('System Error Contact Support.');
              console.log(err);
          }
      });
  });

  $('#sellAtMarket').click(function(){
      $('#sell-modal').modal('toggle');
      $('#notification-title').text('Sell at Market Executing');
      $('#notification-body-text').text('Please stand by while we execute your trade.');
      $('#notification-modal').modal('toggle');
      $.ajax({
          type: "POST",
          url: '/trade/sell-at-market',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {exchange: exchange, sellOrderId: sellOrderId},
          success: function( response ) {
              // console.log('exchange: ' + exchange);
              console.log(response);
              $('#notification-modal').modal('hide');
              if(response.error){
                  $('#notification-title').text('Trade Warning');
                  $('#notification-body-text').text(response.data.message);
                  setTimeout(function(){
                      $('#notification-modal').modal('show');
                  }, 2000);
              }else{
                  $('#notification-title').text('Successfully Executed Trade');
                  $('#notification-body-text').text(response.data.message);
                  setTimeout(function(){
                      $('#notification-modal').modal('show');
                  }, 2000);
              }

          },
          error: function( err ){
              $('#notification-modal').modal('hide');
              alert('System Error Contact Support.');
              console.log(err);
          }
      });
  });

  $('#cancelTrade').click(function(){
      $('#swipe-modal').modal('toggle');
  });

  $('#cancelSell').click(function(){
    $('#sell-modal').modal('toggle');
  });

  $('#demoSwitch').on('change', function(){
    console.log('inheheinh');
      const checked = $('#demoSwitch').is(':checked');
      $.ajax({
          type: "GET",
          url: '/swipe-trade/demo/'+checked,
          success: function( response ) {
              console.log(response);
              alert(response);
          },
          error: function( err ){
              console.log(err);
              alert(err);
          }
      });
  });

});
