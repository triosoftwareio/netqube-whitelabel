<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
    //
    protected $fillable = [
        'name', 'language', 'prettyDate', 'prettyTime', 'link'
    ];
}
