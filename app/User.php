<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'plan',
        'status',
        'avatar',
        'first_name',
        'last_name',
        'country',
        'postal_code',
        'phone_number',
        'city',
        'address',
        'sponsor_name',
        'trader_demo',
        'authProfileId',
        'paymentProfileId',
        'default_exchange',
        'fromApi',
        'length',
        'expiry_date',
        'payment_address',
        'default_position',
        'btcPrice',
        'addressType',
        'last_price_time',
        'trader_demo',
        'api_key',
        'expiry_date',
        'imported'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function binaryPlan()
    {
        return $this->belongsTo('App\Plan', 'plan', 'id');
    }

    public function withdrawls(){
        return $this->belongsTo('App\Withdrawl', 'user_id', 'id');
    }

    public function personal(){
        return $this->hasMany('App\Binary', 'parent_id', 'id');
    }
}
