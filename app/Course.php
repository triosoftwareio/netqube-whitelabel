<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
  public function video(){
    return $this->hasMany('App\Video', 'courseId', 'id');
  }
  
  public function download(){
    return $this->hasMany('App\Download', 'course_id', 'id');
  }
}
