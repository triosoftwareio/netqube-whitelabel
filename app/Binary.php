<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Binary extends Model
{
    protected $table = 'binarytree';

    public function parent(){
        return $this->belongsTo('App\User', 'parent_id', 'id');
    }

    public function child(){
        return $this->belongsTo('App\User', 'child_id', 'id');
    }

    public function placement(){
        return $this->belongsTo('App\User', 'placement_id', 'id');
    }
}
