<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompletedVideos extends Model
{
  protected $table = 'user_completed_videos';

  public function user(){
    return $this->belongsTo('App\User', 'userId', 'id');
  }

  public function video(){
    return $this->belongsTo('App\Video', 'videoId', 'id');
  }

  public function course(){
    return $this->belongsTo('App\Course', 'courseId', 'id');
  }

}
