<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPayout extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->inputs['admin']){
            $subject = 'Commissions Report';
        }else{
            $subject = 'We sent daily commissions to your wallet!';
        }

        return $this->markdown('emails.payout')->with(['payoutData' => $this->inputs['payoutData'], 'admin' => $this->inputs['admin']])->subject($subject);
    }
}
