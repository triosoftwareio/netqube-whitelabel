<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiPayment extends Model
{
    //
    protected $fillable = [
        'length',
        'amount',
        'username',
        'created_at',
        'updated_at',
        'userId'
    ];
}
