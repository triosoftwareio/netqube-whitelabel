<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompletedCourses extends Model
{
  protected $table = 'user_completed_courses';

  public function user(){
    return $this->belongsTo('App\User', 'userId', 'id');
  }

  public function course(){
    return $this->belongsTo('App\Course', 'courseId', 'id');
  }
}
