<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedWebinar extends Model
{
    //
    protected $fillable = [
        'name', 'link', 'date'
    ];
}
