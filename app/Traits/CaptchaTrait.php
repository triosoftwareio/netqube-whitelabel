<?php
 namespace App\Traits;

 use Request;
 use ReCaptcha\ReCaptcha;

 trait CaptchaTrait{
     public static function captchaCheck(){
         $response = Request::get('g-recaptcha-response');
         $remoteip = $_SERVER['REMOTE_ADDR'];
         $secret   = '6LfvrSoUAAAAAEAzk79B8Jx8F3NWBQSoB2VuciWR';

         $recaptcha = new ReCaptcha($secret);
         $resp = $recaptcha->verify($response, $remoteip);
         if ($resp->isSuccess()) {
             return 1;
         } else {
             return 0;
         }
    }
 }