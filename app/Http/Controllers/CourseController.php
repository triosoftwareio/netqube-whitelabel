<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Video;
use App\UserCompletedVideos;
use App\CourseType;
use Auth;
use App\UserCompletedCourses;
use App\Archive;

class CourseController extends Controller
{
    public function index(){
      $courses = CourseType::with('course')->get();
      $userCompletedVideos = UserCompletedVideos::where('userId', Auth::id())->get();
      return view('course.index', ['courses' => $courses, 'userCompletedVideos' => $userCompletedVideos]);
    }

    public function indexSubCourse($id, $video){
      $videos = Video::where('courseId', $id)->get();
      $currentVideo = Video::where('id', $video)->first();
      $course = Course::with('video', 'download')->where('id', $id)->first();
      $userCompletedVideos = UserCompletedVideos::where('userId', Auth::id())->where('courseId', $id)->count();
      $userCompletedFull = UserCompletedVideos::where('userId', Auth::id())->where('courseId', $id)->get();
      return view('course.module', ['videos' => $videos, 'currentVideo' => $currentVideo, 'course' => $course, 'userCompletedVideos' => $userCompletedVideos, 'completedVideos' => $userCompletedFull]);
    }

    public function markComplete($id){
      $video = Video::where('id', $id)->first();
      $course = Course::where('id', $video->courseId)->first();
      $completedVideos = UserCompletedVideos::where('userId', Auth::id())->where('videoId', $video->id)->count();
      $videoId = $video->id;
      if($completedVideos){
        $video = Video::where('id', '>', $videoId)->where('courseId', $course->id)->first();
        if($video){
          return redirect("/courses/".$course->id."/".$video->id);
        }else{
          $userCompletedCourse = UserCompletedCourses::where('userId', Auth::id())->where('courseId', $course->id)->first();
          if(!$userCompletedCourse){
            $newCompletedCourse = new UserCompletedCourses;
            $newCompletedCourse->courseId = $course->id;
            $newCompletedCourse->userId = Auth::id();
            $newCompletedCourse->save();
          }
          return redirect("/courses");
        }
      }else{
        $newCompletion = new UserCompletedVideos;
        $newCompletion->videoId = $video->id;
        $newCompletion->courseId = $course->id;
        $newCompletion->userId = Auth::id();
        $newCompletion->save();

        $video = Video::where('id', '>', $videoId)->where('courseId', $course->id)->first();
        if($video){
          return redirect("/courses/".$course->id."/".$video->id);
        }else{
          $userCompletedCourse = UserCompletedCourses::where('userId', Auth::id())->where('courseId', $course->id)->first();
          if(!$userCompletedCourse){
            $newCompletedCourse = new UserCompletedCourses;
            $newCompletedCourse->courseId = $course->id;
            $newCompletedCourse->userId = Auth::id();
            $newCompletedCourse->save();
          }
          return redirect("/courses");
        }
      }
    }

    public function listArchives(){
      $archives = Archive::all();
      return view('archive.index', ['archives' => $archives]);
    }
}
