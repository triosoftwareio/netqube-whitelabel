<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Binary;
use App\Plan;
use App\Transaction;
use App\CronLog;
use Carbon\Carbon;

class CronController extends Controller
{
    public function getFullDownline($userId){

      $currentUser = User::with('binaryPlan')->where('id', $userId)->first();
      $amount = $currentUser->binaryPlan->amount;

      $directDownline = array(
        'id' => $userId,
        'username' => $currentUser->username,
        'name' => $currentUser->email,
        'status' => $currentUser->status,
        'package' => $amount,
        'leftVolume' => 0,
        'rightVolume' => 0,
        'children' => []
      );

      $children = Binary::where('placement_id', $userId)->get();

      $count = 0;
      foreach($children as $child){
        $count++;
        $nextChildren = $this->getFullDownline($child->child_id);
        $directDownline['children'][$child->child_id] = $nextChildren;

        if($count == 1){
          $directDownline['leftVolume'] = $directDownline['leftVolume']+$nextChildren['package']+$nextChildren['leftVolume']+$nextChildren['rightVolume'];
        }else{
          $directDownline['rightVolume'] = $directDownline['rightVolume']+$nextChildren['package']+$nextChildren['rightVolume']+$nextChildren['leftVolume'];
        }
      }
      $currentUser->totalVolume = $directDownline['rightVolume']+$directDownline['leftVolume'];
      ($directDownline['leftVolume'] > $directDownline['rightVolume']) ? $lesserLeg = $directDownline['rightVolume'] : $lesserLeg = $directDownline['leftVolume'];
      $currentUser->lesserLegVolume = $lesserLeg;
      $currentUser->leftLegVolume = $directDownline['leftVolume'];
      $currentUser->rightLegVolume = $directDownline['rightVolume'];
      $currentUser->save();
      return $directDownline;
    }

    public function calculateDailyVolume(){
      $this->getFullDownline(1);
      return response()->json('Success');
    }

    public function addDailyPercentage(Request $request){
        if(CronLog::where('created_at', '>=', Carbon::now()->subHours(20)->toDateTimeString())->first()){

        }else{
          $cronLog = new CronLog;
          $cronLog->type = 'Daily';
          $cronLog->save();

          $users = User::with('binaryPlan')->where('status', 'Active')->where('percentage', '<', 99)->get();
          foreach($users as $user){

            $percentage = $user->binaryPlan->daily_percentage;

            $percentageToAdd = ($percentage/2)*100;

            $amount = $user->binaryPlan->amount;
            $addition = $amount*$percentage;

            $balance = $user->balance;

            $newBalance = $balance+$addition;

            $user->balance = $newBalance;

            $currentPercentage = $user->percentage;

            $user->percentage = $currentPercentage+$percentageToAdd;

            $profit = $user->profit_count;

            $newProfit = $profit+$addition;

            $newTransaction = new Transaction;
            $newTransaction->user_id = $user->id;
            $newTransaction->amount = $addition;
            $newTransaction->save();

            $user->profit_count = $newProfit;
            $user->save();


        }
            $users = User::where('status', 'Active')->where('wallet_address', '!=', NULL)->where('balance', '>', 0)->get();
            foreach($users as $user){
                $apiKey = "bf17-c433-d986-1e48";
                $version = 2; // API version
                $pin = "19931992";

                try{
                    $block_io = new \BlockIo($apiKey, $pin, $version);
                    $estNetworkFee = $block_io->get_network_fee_estimate(array('to_address' => $user->wallet_address, 'amount' => $user->balance));
                    $payout = $block_io->withdraw(array('amount' => $user->balance, 'to_addresses' => $user->wallet_address));

                    $amountToDeduct = $user->balance;
                    $amountWithFee = $estNetworkFee->data->estimated_network_fee+$amountToDeduct;
                    $user->balance = $user->balance - $amountWithFee;
                    $user->save();

                    $newWithdrawl = new Withdrawl;
                    $newWithdrawl->amount = $amountWithFee;
                    $newWithdrawl->user_id = $user->id;
                    $newWithdrawl->save();


                    return response()->json('Success');
                }catch(Exception $e){
                    $error = $e->getMessage();
                }

            }
        }
    }
}
