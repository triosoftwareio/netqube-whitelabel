<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuthNetSubscription;
use App\CreditCard;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Auth;
use Carbon\Carbon;
use App\User;

class MerchantController extends Controller
{

  public function displayForm(){
    $merchantFrontKey = '5FHmn9K43qCsnXn9wrAsH37cQ6n7r7tJzc8H9t9Kwvg2X79r2yP9YD4GFaDmT3L4';
    $merchantKey = '4Ne9Bq8K';
    $merchantSecret = 'B62479FEA712355DA2C22C834C9C71D3C8A8B7E9E64A7B3283493214611676AE5699F9B4B041DABC0CD699F3FE15740E970F89198A6E05C62FBDE3A552BD5335';
    return view('payment.index', [
      'merchantFrontKey' => $merchantFrontKey,
      'merchantKey' => $merchantKey,
      'merchantSecret' => $merchantSecret
    ]);
  }

  public function processForm(Request $request){
    return view('payment.process');
  }

  public function webhook(Request $request){
      $subscriptionId = $request->payload->id;
      $authSub = AuthNetSubscription::where('subId', $subscriptionId)->first();
      if($authSub){
          $user = User::where('id', $authSub->userId)->first();
          if($user){
              $user->status = 'Inactive';
              $user->save();
              $authSub->delete();
          }
      }
      return response('Success', 200)
          ->header('Content-Type', 'text/plain');
  }

  public function authStartSubscription($customerProfileId, $customerPaymentProfileId, $name, $transactionKey){
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName($name);
      $merchantAuthentication->setTransactionKey($transactionKey);

      $refId = 'ref' . time();
      // Subscription Type Info
      $subscription = new AnetAPI\ARBSubscriptionType();

      $amount = 129.98;
      $prettyName = 'QubeTrader Subscription';


      $subscription->setName($prettyName);

      $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
      $interval->setLength(30);
      $interval->setUnit("days");


      $paymentSchedule = new AnetAPI\PaymentScheduleType();
      $paymentSchedule->setInterval($interval);
      $todaysDate = new \DateTime();
      $paymentSchedule->setStartDate($todaysDate->add(new \DateInterval('P30D')));
      $paymentSchedule->setTotalOccurrences(9999);

      $subscription->setPaymentSchedule($paymentSchedule);
      $subscription->setAmount($amount);

      $profile = new AnetAPI\CustomerProfileIdType();
      $profile->setCustomerProfileId($customerProfileId);
      $profile->setCustomerPaymentProfileId($customerPaymentProfileId);

      $subscription->setProfile($profile);

      $request = new AnetAPI\ARBCreateSubscriptionRequest();
      $request->setmerchantAuthentication($merchantAuthentication);
      $request->setRefId($refId);
      $request->setSubscription($subscription);
      $controller = new AnetController\ARBCreateSubscriptionController($request);

      $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

      if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
      {
          $response = ['Success',$response->getSubscriptionId()];
      }
      else
      {
          $errorMessages = $response->getMessages()->getMessage();
          $response = ['Fail', $errorMessages[0]->getCode()." ".$errorMessages[0]->getText()];
      }
      return $response;
  }

  public function chargeCustomerProfileId($profileid, $paymentprofileid, $name, $transactionKey){
      // Common setup for API credentials
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName($name);
      $merchantAuthentication->setTransactionKey($transactionKey);
      $refId = 'ref' . time();

      $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
      $profileToCharge->setCustomerProfileId($profileid);
      $paymentProfile = new AnetAPI\PaymentProfileType();
      $paymentProfile->setPaymentProfileId($paymentprofileid);
      $profileToCharge->setPaymentProfile($paymentProfile);

      $transactionRequestType = new AnetAPI\TransactionRequestType();
      $transactionRequestType->setTransactionType( "authCaptureTransaction");

      if(Auth::user()->status == 'Active'){
        $response = (object)[
            'status' => 'Success',
            'amount' => 129.98,
            'message' => 'No Response'
        ];
        return $response;
      }
      else $amount = 129.98;

      $transactionRequestType->setAmount($amount);
      $transactionRequestType->setProfile($profileToCharge);

      $request = new AnetAPI\CreateTransactionRequest();
      $request->setMerchantAuthentication($merchantAuthentication);
      $request->setRefId( $refId);
      $request->setTransactionRequest( $transactionRequestType);
      $controller = new AnetController\CreateTransactionController($request);
      $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

      if ($response != null)
      {
          $tresponse = $response->getTransactionResponse();

          if ($tresponse != null && $tresponse->getMessages() != null)
          {
              $response = (object)[
                  'status' => 'Success',
                  'amount' => $amount,
                  'message' => 'No Response'
              ];
          }
          else
          {
              if($tresponse->getErrors() != null)
              {
                  $response = (object)[
                      'status' => 'Fail',
                      'amount' => $amount,
                      'message' => $tresponse->getErrors()[0]->getErrorText()];
              }
          }
      }
      else
      {
          $response = (object)[
              'status' => 'Fail',
              'amount' => $amount,
              'message' => 'No Response'
          ];
      }

      return $response;
  }

  public function createCustomerProfileAuth($paymentNonce, $request, $name, $transactionKey){
      // Common setup for API credentials
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName($name);
      $merchantAuthentication->setTransactionKey($transactionKey);
      $refId = 'ref' . time();
      // Create the payment data for an accept token
      $op = new AnetAPI\OpaqueDataType();
      $op->setDataDescriptor("COMMON.ACCEPT.INAPP.PAYMENT");
      $op->setDataValue($paymentNonce);
      $paymentOne = new AnetAPI\PaymentType();
      $paymentOne->setOpaqueData($op);
      // Create the Bill To info
      $billto = new AnetAPI\CustomerAddressType();
      $billto->setFirstName($request->firstName);
      $billto->setLastName($request->lastName);
      $billto->setAddress($request->address);
      $billto->setCity($request->city);
      $billto->setState($request->state);
      $billto->setZip($request->zip);
      $billto->setCountry($request->country);

      // Create a Customer Profile Request
      //  1. create a Payment Profile
      //  2. create a Customer Profile
      //  3. Submit a CreateCustomerProfile Request
      //  4. Validate Profiiel ID returned
      $paymentprofile = new AnetAPI\CustomerPaymentProfileType();
      $paymentprofile->setCustomerType('individual');
      $paymentprofile->setBillTo($billto);
      $paymentprofile->setPayment($paymentOne);
      $paymentprofiles[] = $paymentprofile;
      $customerprofile = new AnetAPI\CustomerProfileType();
      $customerprofile->setDescription("QubeTrade_Customer");
      $customerprofile->setMerchantCustomerId("M_".Auth::user()->id);
      $customerprofile->setEmail(Auth::user()->email);
      $customerprofile->setPaymentProfiles($paymentprofiles);
      $request = new AnetAPI\CreateCustomerProfileRequest();
      $request->setMerchantAuthentication($merchantAuthentication);
      $request->setRefId($refId);
      $request->setProfile($customerprofile);
      $controller = new AnetController\CreateCustomerProfileController($request);
      $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
      if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
      {
          $paymentProfiles = $response->getCustomerPaymentProfileIdList();
          return ['Success', $response->getCustomerProfileId(), $paymentProfiles[0]];
      }
      else
      {
          $errorMessages = $response->getMessages()->getMessage();
          $message = "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
          return ['Fail', $message];
      }

  }

  public function getSubscriptions(){
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantKey = '4Ne9Bq8K';
    $merchantSecret = '7g855zWf3qNU5hhN';

    $name = $merchantKey;
    $transactionKey = $merchantSecret;
    $merchantAuthentication->setName($name);
    $merchantAuthentication->setTransactionKey($transactionKey);

    $refId = 'ref' . time();

    $sorting = new AnetAPI\ARBGetSubscriptionListSortingType();
    $sorting->setOrderBy("id");
    $sorting->setOrderDescending(false);
    $paging = new AnetAPI\PagingType();
    $paging->setLimit("1000");
    $paging->setOffset("1");
    $request = new AnetAPI\ARBGetSubscriptionListRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setSearchType("subscriptionActive");
    $request->setSorting($sorting);
    $request->setPaging($paging);

    $controller = new AnetController\ARBGetSubscriptionListController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
    dd($response);
    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
        echo "SUCCESS: Subscription Details:" . "\n";
        foreach ($response->getSubscriptionDetails() as $subscriptionDetails) {
            echo "Subscription ID: " . $subscriptionDetails->getId() . "\n";
        }
        echo "Total Number In Results:" . $response->getTotalNumInResultSet() . "\n";
    } else {
        echo "ERROR :  Invalid response\n";
        $errorMessages = $response->getMessages()->getMessage();
        echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
    }
    return $response;
  }

  public function authNet(Request $request){

      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $paymentNonce = $request->nonce;

      // $merchantFrontKey = '4Ne9Bq8K';
      // $merchantKey = '7g855zWf3qNU5hhN';
      // $merchantSecret = 'B62479FEA712355DA2C22C834C9C71D3C8A8B7E9E64A7B3283493214611676AE5699F9B4B041DABC0CD699F3FE15740E970F89198A6E05C62FBDE3A552BD5335';
      // public client key = 5FHmn9K43qCsnXn9wrAsH37cQ6n7r7tJzc8H9t9Kwvg2X79r2yP9YD4GFaDmT3L4

      $merchantFrontKey = '5FHmn9K43qCsnXn9wrAsH37cQ6n7r7tJzc8H9t9Kwvg2X79r2yP9YD4GFaDmT3L4';
      $merchantKey = '4Ne9Bq8K';
      $merchantSecret = '7g855zWf3qNU5hhN';

      $authProfileId = Auth::user()->authProfileId;
      $paymentProfileId = Auth::user()->paymentProfileId;

      $name = $merchantKey;
      $transactionKey = $merchantSecret;
      $merchantAuthentication->setName($name);
      $merchantAuthentication->setTransactionKey($transactionKey);


      if($authProfileId){

          //Missing profile id and payment id as params
          $chargeOneTime = $this->chargeCustomerProfileId($authProfileId, $paymentProfileId, $name, $transactionKey);

          if($chargeOneTime->status == 'Success'){
            $newSub = $this->authStartSubscription($authProfileId, $paymentProfileId, $name, $transactionKey);
            if($newSub[0] == 'Success'){
                $newDbSub = new AuthNetSubscription;
                $newDbSub->subId = $newSub[1];
                $newDbSub->userId = Auth::id();
                $newDbSub->save();

                return response()->json('Success');
            }else{
                return response()->json($newSub[1]);
            }
          }else{
            return response()->json('Error charging card please contact support.');
          }


              //Set user to active
      }else{
          //billing info missing in param and nounce missing in param
          $profileData = $this->createCustomerProfileAuth($paymentNonce, $request, $name, $transactionKey);

          if($profileData[0] == 'Success'){
              $authProfileId = $profileData[1];
              $paymentProfileId = $profileData[2];
              $chargeOneTime = $this->chargeCustomerProfileId($authProfileId, $paymentProfileId, $name, $transactionKey);
              if($chargeOneTime->status == 'Success'){

                $creditCard = new CreditCard;
                $creditCard->last4 = substr($request->cardNumber, -4);
                $creditCard->expMonth = $request->monthId;
                $creditCard->expYear = $request->yearId;
                $creditCard->userId = Auth::id();
                $creditCard->save();

                Auth::user()->authProfileId = $authProfileId;
                Auth::user()->paymentProfileId = $paymentProfileId;
                Auth::user()->status = 'Active';
                Auth::user()->default_exchange = 'poloniex';
                Auth::user()->save();

                $newSub = $this->authStartSubscription($authProfileId, $paymentProfileId, $name, $transactionKey);

                if($newSub[0] == 'Success'){

                    $newDbSub = new AuthNetSubscription;
                    $newDbSub->subId = $newSub[1];
                    $newDbSub->userId = Auth::id();
                    $newDbSub->save();

                    return response()->json('Success');
                }else{
                    return response()->json($newSub[1]);
                }
              }else{
                  return response()->json('Error Charging Card Please Contact Support');
              }

          }else{
              return response()->json($profileData[1]);
          }
      }

  }

}
