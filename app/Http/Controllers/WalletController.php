<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Withdrawl;

class WalletController extends Controller
{
    public function index(){
      return view('wallet.wallet');
    }

    public function updateWalletAddress(Request $request){
        $walletAddress = $request->wallet_address;
        $user = Auth::user();
        $user->wallet_address = $walletAddress;
        $user->save();
        return view('error.display', ['status' => 'Success', 'message' => 'Wallet successfully updated.']);
    }

    public function payOut(){
        $users = User::where('status', 'Active')->where('wallet_address', '!=', NULL)->where('balance', '>', 0)->get();
        foreach($users as $user){
            $apiKey = "bf17-c433-d986-1e48";
            $version = 2; // API version
            $pin = "19931992";
            
            try{
                $block_io = new \BlockIo($apiKey, $pin, $version);
                $estNetworkFee = $block_io->get_network_fee_estimate(array('to_address' => $user->wallet_address, 'amount' => $user->balance));
                $payout = $block_io->withdraw(array('amount' => $user->balance, 'to_addresses' => $user->wallet_address));
                $user = User::where('id', Auth::id())->first();

                $amountToDeduct = $user->balance;
                $amountWithFee = $estNetworkFee->data->estimated_network_fee+$user->balance;
                $user->balance = $user->balance - $amountToDeduct;
                $user->save();

                $newWithdrawl = new Withdrawl;
                $newWithdrawl->amount = $amountWithFee;
                $newWithdrawl->user_id = Auth::id();
                $newWithdrawl->save();
                $user->balance = Auth::user()->balance - $amountWithFee;
                $user->save();

                return response()->json('Success');
            }catch(Exception $e){
                $error = $e->getMessage();
            }

        }
    }

    public function withdrawl(Request $request){
//      if($request->amount < 0.05){
//        return view('error.display', ['status' => 'Error', 'message' => 'Withdrawl amount must be larger than 0.05 Bitcoins']);
//      }
//      if($request->amount > Auth::user()->balance){
//        return view('error.display', ['status' => 'Error', 'message' => 'You have insufficient funds']);
//      }
//
//        $user = User::where('id', Auth::id())->first();
//        $amountToDeduct = $request->amount;
//        $user->balance = Auth::user()->balance - $amountToDeduct;
//        $user->save();
//
//
//
//      //init wallet api
//      $apiKey = "bf17-c433-d986-1e48";
//      $version = 2; // API version
//      $pin = "19931992";
//      // dd(phpinfo());
//      try{
//        $block_io = new \BlockIo($apiKey, $pin, $version);
//        $estNetworkFee = $block_io->get_network_fee_estimate(array('to_address' => $request->toAddress, 'amount' => $request->amount));
//          $payout = $block_io->withdraw(array('amount' => $request->amount, 'to_addresses' => $request->toAddress));
//          $user = User::where('id', Auth::id())->first();
//          $amountWithFee = $estNetworkFee->data->estimated_network_fee;
//          $user->balance = Auth::user()->balance - $amountWithFee;
//          $user->save();
//
//        return view('error.display', ['status' => 'Success', 'message' => 'Funds have been succesfully sent']);
//      }catch(Exception $e){
//        return view('error.display', ['status' => 'Error', 'message' => $e->getMessage()]);
//      }

    }
}
