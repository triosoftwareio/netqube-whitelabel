<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Binary;
use Auth;
use DB;

class BinaryController extends Controller
{
    public function getOuterPlacement($data, $startId, $side){
        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'amount' => $data[$startId]['amount'],
            'left_volume' => 0,
            'right_volume' => 0,
            'left_child' => [],
            'right_child' => [],
            'placement_id' => NULL
        );

        if($side == 'Left'){
            if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
                $left_child = $this->getOuterPlacement($data, $data[$startId]['left_child'], $side);
                $directDownline['left_child'] = $left_child;
                $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['amount'];
                $directDownline['placement_id'] = $left_child['placement_id'];
            }else{
                $directDownline['placement_id'] = $startId;
            }
        }else{
            if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
                $right_child = $this->getOuterPlacement($data, $data[$startId]['right_child'], $side);
                $directDownline['right_child'] = $right_child;
                $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['amount'];
                $directDownline['placement_id'] = $right_child['placement_id'];
            }else{
                $directDownline['placement_id'] = $startId;
            }
        }




        return $directDownline;
    }

  public function getBinaryDownline($data, $startId, $lastPaidOutLeft, $lastPaidOutRight, $firstUser){

      if($data[$startId]['status'] === 'Active' && $data[$startId]['plan_name'] !== 'Sales_Rep'){
          $volume_amount = 150;
      }else{
          $volume_amount = 0;
      }

//      if(($firstUser == false && $lastPaidOutLeft !== null && $lastPaidOutLeft > $data[$startId]['last_renewal']) || ($firstUser == false && $lastPaidOutRight !== null && $lastPaidOutRight > $data[$startId]['last_renewal']) ){
//          $volume_amount = 0;
//      }


      $directDownline = array(
          'id' => $startId,
          'name' => $data[$startId]['name'],
          'username' => $data[$startId]['username'],
          'status' => $data[$startId]['status'],
          'plan_name' => $data[$startId]['plan_name'],
          'volume_amount' => $volume_amount,
          'left_volume' => 0,
          'right_volume' => 0,
          'left_child' => [],
          'right_child' => []
      );

      if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
          $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child'], $lastPaidOutLeft, $lastPaidOutLeft, false);
          $directDownline['left_child'] = $left_child;
          $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['volume_amount'];
      }
      if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
          $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child'], $lastPaidOutRight, $lastPaidOutRight, false);
          $directDownline['right_child'] = $right_child;
          $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['volume_amount'];
      }

      return $directDownline;
  }

  public function getChildrenIds($startId){
    $children = Binary::where('parent_id', $startId)->get();
    $childrenId = [];
    foreach($children as $child){
      $childrenId[] = $child->child_id;
    }
    foreach($childrenId as $cid){
      $childrenId[] = $this->getChildrenIds($cid);
    }
    return($childrenId);
  }

  public function checkInnerArray($childArray, $thisUser){
    if(in_array($thisUser->id, $childArray)) return True;
    else{
      foreach($childArray as $child){
        if(is_array($child)){
          $check = $this->checkInnerArray($child, $thisUser);
          if($check) return True;
        }
      }
    }
    return false;
  }

    public function index(){
        $testQuery = DB::select("SELECT p.id,p.last_renewal as last_renewal, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $parents = [];

        foreach($testQuery as $parent){
            $parents[$parent->id]['id'] = $parent->id;
            $parents[$parent->id]['name'] = $parent->parent_name;
            $parents[$parent->id]['username'] = $parent->username;
            $parents[$parent->id]['status'] = $parent->status;
            $parents[$parent->id]['plan_name'] = $parent->name;
            $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
            $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
            $parents[$parent->id]['left_child'] = $parent->left_child;
            $parents[$parent->id]['right_child'] = $parent->right_child;
        }

        $downline = $this->getBinaryDownline($parents, Auth::id(), Auth::user()->last_paid_out_left, Auth::user()->last_paid_out_right, true);
        $userId = Auth::id();

        $leftDownline = Binary::where('parent_id', Auth::id())->where('side', 'Left')->get();
        $rightDownline = Binary::where('parent_id', Auth::id())->where('side', 'Right')->get();
        $personalCountLeft = 0;
        $personalCountRight = 0;
        foreach($leftDownline as $ld){
          $user = User::where('id', $ld->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountLeft++;
        }
        foreach($rightDownline as $rd){
          $user = User::where('id', $rd->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountRight++;
        }

        $placementQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $placementParents = [];

        foreach($placementQuery as $parent){
            $placementParents[$parent->id]['id'] = $parent->id;
            $placementParents[$parent->id]['name'] = $parent->parent_name;
            $placementParents[$parent->id]['username'] = $parent->username;
            $placementParents[$parent->id]['status'] = $parent->status;
            $placementParents[$parent->id]['plan_name'] = $parent->name;
            $placementParents[$parent->id]['amount'] = $parent->amount;
            $placementParents[$parent->id]['left_child'] = $parent->left_child;
            $placementParents[$parent->id]['right_child'] = $parent->right_child;
        }

        $outerLeftPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Left');
        $outerRightPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Right');


        $dailyLimit = 0;
        $thisUser = User::where('id', Auth::id())->first();
        if($personalCountLeft > 0 && $personalCountRight > 0 && $thisUser->status == 'Active') $qualified = true;
        else $qualified = false;
        if($personalCountLeft > 4 && $personalCountRight > 4) {
            $dailyLimit = 4500; // 30 Vol
        }else{
            $dailyLimit = 3000; // 20 Vol
        }

        $allUsers = User::all();

        if($outerRightPlacementData['placement_id'] !== NULL){
            $rightPlacementUser = base64_encode($outerRightPlacementData['placement_id']);
        }else{
            $rightPlacementUser = NULL;
        }

        if($outerLeftPlacementData['placement_id'] !== NULL){
            $leftPlacementUser = base64_encode($outerLeftPlacementData['placement_id']);
        }else{
            $leftPlacementUser = NULL;
        }

        return view('binarytree.binarytree', ['downlines' => $downline, 'userId' => $userId, 'qualified' => $qualified, 'dailyLimit' => $dailyLimit, 'users' => $allUsers, 'rightPlacementUser' => $rightPlacementUser, 'leftPlacementUser' => $leftPlacementUser]);
    }

    public function getChildren(Request $request){
        $intVal = intval($request->parent);

        if($intVal > 0){
        return view('error.display', [
          'status' => 'Error',
          'message' => 'This user does not exist.'
        ]);
        }

        $testQuery = DB::select("SELECT p.id, p.name as parent_name,p.last_renewal as last_renewal, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $parents = [];

        foreach($testQuery as $parent){
            $parents[$parent->id]['id'] = $parent->id;
            $parents[$parent->id]['name'] = $parent->parent_name;
            $parents[$parent->id]['username'] = $parent->username;
            $parents[$parent->id]['status'] = $parent->status;
            $parents[$parent->id]['plan_name'] = $parent->name;
            $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
            $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
            $parents[$parent->id]['left_child'] = $parent->left_child;
            $parents[$parent->id]['right_child'] = $parent->right_child;
        }

        if($request->side === 'right'){
            $lastPaidOut = Auth::user()->last_paid_out_right;
        }elseif($request->side === 'left'){
            $lastPaidOut = Auth::user()->last_paid_out_left;
        }else{
          $lastPaidOut = NULL;
        }

        $downline = $this->getBinaryDownline($parents, base64_decode($request->parent), $lastPaidOut, $lastPaidOut, false);
        $userId = Auth::id();
        $personalsLeft = Binary::where('parent_id', Auth::id())->where('side', 'Left')->get();
        $personalsRight = Binary::where('parent_id', Auth::id())->where('side', 'Right')->get();
        $personalCountLeft = 0;
        $personalCountRight = 0;

        foreach($personalsLeft as $ld){
          $user = User::where('id', $ld->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountLeft++;
        }
        foreach($personalsRight as $rd){
          $user = User::where('id', $rd->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountRight++;
        }

        $dailyLimit = 0;
        $thisUser = User::where('id', Auth::id())->first();
        if($personalCountLeft > 0 && $personalCountRight > 0 && $thisUser->status == 'Active') $qualified = true;
        else $qualified = false;
        if($personalCountLeft > 4 && $personalCountRight > 4) {
            $dailyLimit = 4500; // 30 Vol
        }else{
            $dailyLimit = 3000; // 20 Vol
        }

        $holdingTankUsers = Binary::with('child')->where('parent_id', Auth::id())->where('placement_id', NULL)->get();

        if(Auth::id() == 1){
            $holdingTankUsers = Binary::with('child')->get();
        }

        $allUsers = User::all();

        $placementQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $placementParents = [];

        foreach($placementQuery as $parent){
            $placementParents[$parent->id]['id'] = $parent->id;
            $placementParents[$parent->id]['name'] = $parent->parent_name;
            $placementParents[$parent->id]['username'] = $parent->username;
            $placementParents[$parent->id]['status'] = $parent->status;
            $placementParents[$parent->id]['plan_name'] = $parent->name;
            $placementParents[$parent->id]['amount'] = $parent->amount;
            $placementParents[$parent->id]['left_child'] = $parent->left_child;
            $placementParents[$parent->id]['right_child'] = $parent->right_child;
        }

        $outerLeftPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Left');
        $outerRightPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Right');

        if($outerRightPlacementData['placement_id'] !== NULL){
            $rightPlacementUser = base64_encode($outerRightPlacementData['placement_id']);
        }else{
            $rightPlacementUser = NULL;
        }

        if($outerLeftPlacementData['placement_id'] !== NULL){
            $leftPlacementUser = base64_encode($outerLeftPlacementData['placement_id']);
        }else{
            $leftPlacementUser = NULL;
        }

        return view('binarytree.binarytree', ['downlines' => $downline, 'userId' => $userId, 'holdingTankUsers' => $holdingTankUsers, 'qualified' => $qualified, 'dailyLimit' => $dailyLimit, 'users' => $allUsers, 'rightPlacementUser' => $rightPlacementUser, 'leftPlacementUser' => $leftPlacementUser]);
    }

    public function upOne(Request $request){
        $currentPositionId = $request->currentId;
        $binaryPosition = Binary::where('child_id', $currentPositionId)->first();
        $userbyPosition = User::where('id', $binaryPosition->placement_id)->first();
        $userInTree = false;

        if($userbyPosition == null){
            return view('error.display', [
                'status' => 'Error',
                'message' => 'This user does not exist.'
            ]);
        }

        $userId = $userbyPosition->id;
        $allChildren[] = $this->getChildrenIds(Auth::user()->id);
        if(in_array($userbyPosition->id, $allChildren[0])) $userInTree = True;
        else{
          foreach($allChildren[0] as $child){
            if(is_array($child)){
              $userInTree = $this->checkInnerArray($child, $userbyPosition);
              if($userInTree) break;
            }
          }
        }

        if($userInTree == false && ($userId != Auth::id())){
          return view('error.display', [
              'status' => 'Error',
              'message' => 'This user is not in your tree.'
          ]);
        }

        $testQuery = DB::select("SELECT p.id, p.name as parent_name,p.last_renewal as last_renewal, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $parents = [];

        foreach($testQuery as $parent){
            $parents[$parent->id]['id'] = $parent->id;
            $parents[$parent->id]['name'] = $parent->parent_name;
            $parents[$parent->id]['username'] = $parent->username;
            $parents[$parent->id]['status'] = $parent->status;
            $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
            $parents[$parent->id]['plan_name'] = $parent->name;
            $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
            $parents[$parent->id]['left_child'] = $parent->left_child;
            $parents[$parent->id]['right_child'] = $parent->right_child;
        }

        $downline = $this->getBinaryDownline($parents, $userId, null, null, false);
        $userId = Auth::id();
        $personalsLeft = Binary::where('parent_id', Auth::id())->where('side', 'Left')->get();
        $personalsRight = Binary::where('parent_id', Auth::id())->where('side', 'Right')->get();
        $personalCountLeft = 0;
        $personalCountRight = 0;

        foreach($personalsLeft as $ld){
          $user = User::where('id', $ld->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountLeft++;
        }
        foreach($personalsRight as $rd){
          $user = User::where('id', $rd->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountRight++;
        }

        $dailyLimit = 0;
        $thisUser = User::where('id', Auth::id())->first();
        if($personalCountLeft > 0 && $personalCountRight > 0 && $thisUser->status == 'Active') $qualified = true;
        else $qualified = false;
        if($personalCountLeft > 4 && $personalCountRight > 4) {
            $dailyLimit = 4500; // 30 Vol
        }else{
            $dailyLimit = 3000; // 20 Vol
        }

        $allUsers = User::all();

        $placementQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $placementParents = [];

        foreach($placementQuery as $parent){
            $placementParents[$parent->id]['id'] = $parent->id;
            $placementParents[$parent->id]['name'] = $parent->parent_name;
            $placementParents[$parent->id]['username'] = $parent->username;
            $placementParents[$parent->id]['status'] = $parent->status;
            $placementParents[$parent->id]['plan_name'] = $parent->name;
            $placementParents[$parent->id]['amount'] = $parent->amount;
            $placementParents[$parent->id]['left_child'] = $parent->left_child;
            $placementParents[$parent->id]['right_child'] = $parent->right_child;
        }

        $outerLeftPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Left');
        $outerRightPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Right');

        if($outerRightPlacementData['placement_id'] !== NULL){
            $rightPlacementUser = base64_encode($outerRightPlacementData['placement_id']);
        }else{
            $rightPlacementUser = NULL;
        }

        if($outerLeftPlacementData['placement_id'] !== NULL){
            $leftPlacementUser = base64_encode($outerLeftPlacementData['placement_id']);
        }else{
            $leftPlacementUser = NULL;
        }

        return view('binarytree.binarytree', ['downlines' => $downline, 'userId' => $userId, 'qualified' => $qualified, 'dailyLimit' => $dailyLimit, 'users' => $allUsers, 'rightPlacementUser' => $rightPlacementUser, 'leftPlacementUser' => $leftPlacementUser]);
    }

    public function searchByUsername(Request $request){
        $username = $request->username;
        $userbyUsername = User::where('username', $username)->first();
        $userInTree = false;

        if($userbyUsername == null){
            return view('error.display', [
                'status' => 'Error',
                'message' => 'This user does not exist.'
            ]);
        }
        $userId = $userbyUsername->id;
        $thisUser = User::where('username', $username)->first();
        $allChildren[] = $this->getChildrenIds(Auth::user()->id);
        if(in_array($thisUser->id, $allChildren[0])) $userInTree = True;
        else{
          foreach($allChildren[0] as $child){
            if(is_array($child)){
              $userInTree = $this->checkInnerArray($child, $thisUser);
              if($userInTree) break;
            }
          }
        }

        if($userInTree == false){
          return view('error.display', [
              'status' => 'Error',
              'message' => 'This user is not in your tree.'
          ]);
        }

        $testQuery = DB::select("SELECT p.id, p.name as parent_name,p.last_renewal as last_renewal, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $parents = [];

        foreach($testQuery as $parent){
            $parents[$parent->id]['id'] = $parent->id;
            $parents[$parent->id]['name'] = $parent->parent_name;
            $parents[$parent->id]['username'] = $parent->username;
            $parents[$parent->id]['status'] = $parent->status;
            $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
            $parents[$parent->id]['plan_name'] = $parent->name;
            $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
            $parents[$parent->id]['left_child'] = $parent->left_child;
            $parents[$parent->id]['right_child'] = $parent->right_child;
        }

        $downline = $this->getBinaryDownline($parents, $userId, null, null, false);
        $userId = Auth::id();
        $personalsLeft = Binary::where('parent_id', Auth::id())->where('side', 'Left')->get();
        $personalsRight = Binary::where('parent_id', Auth::id())->where('side', 'Right')->get();
        $personalCountLeft = 0;
        $personalCountRight = 0;

        foreach($personalsLeft as $ld){
          $user = User::where('id', $ld->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountLeft++;
        }
        foreach($personalsRight as $rd){
          $user = User::where('id', $rd->child_id)->first();
          if($user->status == 'Active' && $user->plan == 1) $personalCountRight++;
        }

        $dailyLimit = 0;
        $thisUser = User::where('id', Auth::id())->first();
        if($personalCountLeft > 0 && $personalCountRight > 0 && $thisUser->status == 'Active') $qualified = true;
        else $qualified = false;
        if($personalCountLeft > 4 && $personalCountRight > 4) {
            $dailyLimit = 4500; // 30 Vol
        }else{
            $dailyLimit = 3000; // 20 Vol
        }

        $allUsers = User::all();

        $placementQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $placementParents = [];

        foreach($placementQuery as $parent){
            $placementParents[$parent->id]['id'] = $parent->id;
            $placementParents[$parent->id]['name'] = $parent->parent_name;
            $placementParents[$parent->id]['username'] = $parent->username;
            $placementParents[$parent->id]['status'] = $parent->status;
            $placementParents[$parent->id]['plan_name'] = $parent->name;
            $placementParents[$parent->id]['amount'] = $parent->amount;
            $placementParents[$parent->id]['left_child'] = $parent->left_child;
            $placementParents[$parent->id]['right_child'] = $parent->right_child;
        }

        $outerLeftPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Left');
        $outerRightPlacementData = $this->getOuterPlacement($placementParents, Auth::id(), 'Right');

        if($outerRightPlacementData['placement_id'] !== NULL){
            $rightPlacementUser = base64_encode($outerRightPlacementData['placement_id']);
        }else{
            $rightPlacementUser = NULL;
        }

        if($outerLeftPlacementData['placement_id'] !== NULL){
            $leftPlacementUser = base64_encode($outerLeftPlacementData['placement_id']);
        }else{
            $leftPlacementUser = NULL;
        }

        return view('binarytree.binarytree', ['downlines' => $downline, 'userId' => $userId, 'qualified' => $qualified, 'dailyLimit' => $dailyLimit, 'users' => $allUsers, 'rightPlacementUser' => $rightPlacementUser, 'leftPlacementUser' => $leftPlacementUser]);
    }

//    public function placeUser(Request $request){
//      $userToPlace = $request->userId;
//      $side = $request->side;
//      $placeUnder = User::where('username', $request->placementId)->first();
//
//      if($placeUnder){
//
//        if($placeUnder->id == $userToPlace){
//          return response()->json('Error - Trying to place user within user, this will break the binary link.');
//        }
//
//        $placementCount = Binary::where('placement_id', $placeUnder->id)->where('side', $side)->count();
//        // $sideCount = Binary::where('placement_id', $placeUnder->id)->where('side', $side)->count();
//        // return response()->json(['count' => $sideCount, 'side' => $side]);
//        if($placementCount == 0){
//          if(Auth::user()->admin == 1){
//            $placee = Binary::where('child_id', $userToPlace)->first();
//          }else{
//            $placee = Binary::where('child_id', $userToPlace)->where('parent_id', Auth::id())->first();
//          }
//          if($placee){
//            $placee->placement_id = $placeUnder->id;
//            $placee->side = $side;
//            $placee->save();
//            return response()->json('Success');
//          }else{
//            return response()->json('This user is not sponsored by you');
//          }
//        }else{
//          return response()->json('This User Already Has This Position Occupied.');
//        }
//      }else{
//        return response()->json('Please make sure the username of the user you would like to place your referral under is correct');
//      }
//    }
}
