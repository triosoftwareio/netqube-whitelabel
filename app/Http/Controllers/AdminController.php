<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use Auth;
use Hash;
use App\Plan;
use App\Wallet;
use App\Upgrade;
use Carbon\Carbon;
use App\Binary;
use Image;
use App\Update;
use App\Summary;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSummary;
use App\Mail\MailUpdate;
use App\Mail\NewPayout;
use App\Withdrawl;
use App\Mail\MailUsers;

class AdminController extends Controller
{

    public function index(){
      return view('admin.index');
    }

    public function mailForm(){
      $summaries = Summary::orderBy('id','desc')->take(5)->get();
      $updates = Update::orderBy('id', 'desc')->take(5)->get();
      return view('admin.mailform', [
        'summaries' => $summaries,
        'updates' => $updates
      ]);
    }

    public function emailUsersForm(){
      return view('admin.mailAllUsers');
    }

    public function mailAllUsers(Request $request){
      $users = User::all();
      foreach($users as $user){
          Mail::to($user->email)->send(new MailUsers(['user' => $user, 'content' => $request->input('content'), 'subject' => $request->subject]));
      }
      return redirect('/admin');
    }

    public function mailActiveUsers(Request $request){
      $activeUsers = User::where('status', 'Active')->get();
      foreach($activeUsers as $user){
          Mail::to($user->email)->send(new MailUsers(['user' => $user, 'content' => $request->input('content'), 'subject' => $request->subject]));
      }

      return redirect('/admin');
    }

    public function stats(){
      $numUsers = User::all()->count();
      $activeUserCount = User::where('status','Active')->count();
      $activeUsers = User::where('status', 'Active')->get();
      $customerCount = User::where('status','Active')->where('plan', 1)->count();
      $affiliateCount = $activeUserCount - $customerCount;
      return view('admin.stats', [
        'numUsers' => $numUsers,
        'activeUserCount' => $activeUserCount,
        'activeUsers' => $activeUsers,
        'customerCount' => $customerCount,
        'affiliateCount' => $affiliateCount
      ]);
    }

    public function sendSummary(Request $request){

      if($request->clickedButton == 'preview'){
        // Send mail to justin, scott, amir, joey, tew
        $admins = User::where('status', 'Admin')->get();
        foreach($admins as $user){
            Mail::to($user->email)->send(new MailSummary(['user' => $user, 'file' => $request->summary, 'subject' => $request->title . ' ' . $request->date]));
        }
      } else {
        $users = User::where('status', 'Active')->get();
        foreach($users as $user){
            if($user->plan !== 8){
                Mail::to($user->email)->send(new MailSummary(['user' => $user, 'file' => $request->summary, 'subject' => $request->title . ' ' . $request->date]));
            }
        }
      }
      return redirect('/admin');
    }

    public function sendUpdate(Request $request){

        if($request->clickedButton == 'preview'){
            // Send mail to justin, scott, amir, joey, tew
            $admins = User::where('status', 'Admin')->get();
            foreach($admins as $user){
                Mail::to($user->email)->send(new MailUpdate(['user' => $user, 'file' => $request->summary, 'subject' => $request->title . ' ' . $request->date]));
            }
        } else {
            $users = User::where('status', 'Active')->get();
            foreach($users as $user){
                if($user->plan !== 8){
                    Mail::to($user->email)->send(new MailUpdate(['user' => $user, 'file' => $request->summary, 'subject' => $request->title . ' ' . $request->date]));
                }
            }
        }
        return redirect('/admin');
    }

    // Create duplicate function for sendUpdate but with new Update

    public function uploadFileForm(){

      return view('admin.uploadfile');
    }

    public function uploadSummary(Request $request){
      // Must be absolute path not just relative
      $target_dir = "/var/www/public/netqube/public/uploads/summaries/";
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $local_path = "/uploads/summaries/" . basename($_FILES["fileToUpload"]["name"]);

      // Check if file already exists
      if (file_exists($target_file)) {
          echo "Sorry, file already exists.";
          $uploadOk = 0;
      }
      if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            Summary::create([
              'name' => $_FILES["fileToUpload"]["name"],
              'filePath' => $target_file,
              'localPath' => $local_path
            ]);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
      }
      return redirect('/admin');
    }

    public function uploadUpdate(Request $request){
      // Must be absolute path not just relative
      $target_dir = "/var/www/public/netqube/public/uploads/updates/";
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $local_path = "/uploads/updates/" . basename($_FILES["fileToUpload"]["name"]);

      // Check if file already exists
      if (file_exists($target_file)) {
          echo "Sorry, file already exists.";
          $uploadOk = 0;
      }
      if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            Update::create([
              'name' => $_FILES["fileToUpload"]["name"],
              'filePath' => $target_file,
              'localPath' => $local_path
            ]);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
      }
      return redirect('/admin');
    }

    public function listUsers(){
      if(Auth::user()->status != 'Admin'){
        return response()->json('You are not an admin');
      }else{
        $users = User::paginate(20);
        return view('admin.listusers', ['users' => $users]);
      }
    }

    public function editUser($id){
      if(Auth::user()->status != 'Admin'){
        return response()->json('You are not an administrator');
      }else{
        $user = User::where('id', $id)->first();
        return view('admin.edit', [
          'user' => $user
        ]);
      }
    }

    public function editUserInfo(Request $request, $id){
      $newPassword = Hash::make($request->newpassword);
      $user = User::where('id', $id)->first();
      $user->password = $newPassword;
      $user->save();
      return redirect('/admin');
    }

    public function getBinaryDownline($data, $startId, $lastPaidOutLeft, $lastPaidOutRight, $firstUser){

        if($data[$startId]['status'] === 'Active' && $data[$startId]['plan_name'] !== 'Sales_Rep'){
            $currentVolume = $data[$startId]['volume_amount'];
            $bitcoinAmountToPayout = round((100/$data[$startId]['btc_price']), 5);
        }else{
            $currentVolume = 0;
            $bitcoinAmountToPayout = round(0.00000, 5);
        }

        if(($firstUser == false && $lastPaidOutLeft !== null && $lastPaidOutLeft > $data[$startId]['last_renewal']) || ($firstUser == false && $lastPaidOutRight !== null && $lastPaidOutRight > $data[$startId]['last_renewal']) ){
            $currentVolume = 0;
            $bitcoinAmountToPayout = round(0.00000, 5);
        }

        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'volume_amount' => $currentVolume,
            'btc_amount' => $bitcoinAmountToPayout,
            'left_volume' => 0,
            'right_volume' => 0,
            'btc_left' => 0,
            'btc_right' => 0,
            'left_child' => [],
            'right_child' => []
        );

        if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
            $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child'], $lastPaidOutLeft, $lastPaidOutLeft, false);
            $directDownline['left_child'] = $left_child;
            $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['volume_amount'];
            $directDownline['btc_left']  = $directDownline['btc_left']+$left_child['btc_left']+$left_child['btc_right']+$left_child['btc_amount'];
        }
        if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
            $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child'], $lastPaidOutRight, $lastPaidOutRight, false);
            $directDownline['right_child'] = $right_child;
            $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['volume_amount'];
            $directDownline['btc_right']  = $directDownline['btc_right']+$right_child['btc_right']+$right_child['btc_left']+$right_child['btc_amount'];
        }

        return $directDownline;
    }

    function printTree($downlines, $usersToPay){

        if($downlines['left_volume'] > $downlines['right_volume']){
            if($downlines['right_volume'] > 4500){
                $usersToPay[$downlines['email']] = 4500;
            }else{
                $usersToPay[$downlines['email']] = $downlines['btc_right'];
            }
        }else{
            if($downlines['left_volume'] > 4500){
                $usersToPay[$downlines['email']] = 4500;
            }else{
                $usersToPay[$downlines['email']] = $downlines['btc_left'];
            }
        }

        if(!empty($downlines['left_child'])){
            $nextChildData = $this->printTree($downlines['left_child'], $usersToPay);
            $usersToPay = $nextChildData[1];
        }

        if(!empty($downlines['right_child'])){
            $nextChildData = $this->printTree($downlines['right_child'], $usersToPay);
            $usersToPay = $nextChildData[1];
        }

        return [$downlines, $usersToPay];

    }

    public function calculateCommissions(Request $request){

        if($request->api_key !== '0b597229-4014-4036-92df-40bb0d6b1319'){
            return response()->json('Invalid API Key');
        }

        //Query with parent and children
        $testQuery = DB::select("SELECT p.id,p.last_renewal as last_renewal,p.btcPrice as btc_price, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

        $parents = [];

        //Creating array of parents with children from query data
        foreach($testQuery as $parent){
            $parents[$parent->id]['id'] = $parent->id;
            $parents[$parent->id]['name'] = $parent->parent_name;
            $parents[$parent->id]['username'] = $parent->username;
            $parents[$parent->id]['status'] = $parent->status;
            $parents[$parent->id]['plan_name'] = $parent->name;
            $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
            $parents[$parent->id]['btc_price'] = $parent->btc_price;
            $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
            $parents[$parent->id]['left_child'] = $parent->left_child;
            $parents[$parent->id]['right_child'] = $parent->right_child;
        }

        //Creating array to store users that are to be paid
        $usersToPay = [];


        //getting active users with 2 levels of children for match bonus for calculating commissions
        $usersQualified = User::with('personal.child.personal.child')->where('status', 'Active')->get();

        foreach($usersQualified as $user){
            //Traversing up the tree to check users owed amount
            $downline = $this->getBinaryDownline($parents,$user->id, $user->last_paid_out_left, $user->last_paid_out_right, true);

            //Determining lesser leg
            if(($downline['left_volume'] > $downline['right_volume']) && $downline['btc_right'] > 0.00000){

                $usersToPay[$user->email] = [
                    'team_commission' => $downline['btc_right'],
                ];
                $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_right';
                $usersToPay[$user->email]['usersPaidOn'] = $downline['right_volume']/100;

            }elseif(($downline['right_volume'] > $downline['left_volume']) && $downline['btc_left'] > 0.00000){

                $usersToPay[$user->email] = [
                    'team_commission' => $downline['btc_left'],
                ];
                $usersToPay[$user->email]['usersPaidOn'] = $downline['left_volume']/100;
                $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_left';

            }elseif(($downline['right_volume'] === $downline['left_volume']) && ($downline['btc_left'] > 0.00000 || $downline['btc_right'] > 0.00000)){

                if($downline['btc_left'] > $downline['btc_right']){
                    $usersToPay[$user->email] = [
                        'team_commission' => $downline['btc_right'],
                    ];
                    $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_right';
                    $usersToPay[$user->email]['usersPaidOn'] = $downline['left_volume']/100;
                }else{
                    $usersToPay[$user->email] = [
                        'team_commission' => $downline['btc_left'],
                    ];
                    $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_left';
                    $usersToPay[$user->email]['usersPaidOn'] = $downline['right_volume']/100;
                }

            }else{

                $usersToPay[$user->email] = [
                    'team_commission' => 0.00000,
                ];
                $usersToPay[$user->email]['usersPaidOn'] = 0;
                $usersToPay[$user->email]['payoutSide'] = null;

            }

            $dirUsers = Binary::where('parent_id', $user->id)->get();
            foreach ($dirUsers as $du){
                $directUser = User::where('id', $du->child_id)->first();
                if($user->last_paid_out_direct < $directUser->last_renewal){
                    if($directUser->status == 'Active' && $directUser->plan == 1){
                        if( array_key_exists ( $user->email , $usersToPay )){
                            array_key_exists ( 'direct_commission' , $usersToPay[$user->email] ) ? $usersToPay[$user->email]['direct_commission'] += (100 / $directUser->btcPrice) : $usersToPay[$user->email]['direct_commission'] = (100 / $directUser->btcPrice) ;
                        }else{
                            $usersToPay[$user->email] = ['direct_commission' => (100 / $directUser->btcPrice)];
                        }
                    }
                }
            }

            //Adding user plan for future qualification checks & adding arrays to store user match
            $usersToPay[$user->email]['plan'] = $parents[$user->id]['plan_name'];
            $leftMatch = [];
            $rightMatch = [];
            $leftActive = [];
            $rightActive = [];


            //Gathering data to check if user qualifies for match bonus
            foreach($user->personal as $personal){
                if($personal->side == 'Left' && $personal->child->status == 'Active' && $personal->child->plan == 1){
                    $childLeft = 0;
                    $childRight = 0;
                    foreach($personal->child->personal as $childsChildren){
                        if($childsChildren->side == 'Left' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childLeft++;
                        }elseif($childsChildren->side == 'Right' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childRight++;
                        }
                    }
                    if($childLeft > 0 && $childRight > 0){
                        array_push($leftMatch, $personal->child->email);
                    }
                    array_push($leftActive, $personal->child->email);
                }else if($personal->side == 'Right' && $personal->child->status == 'Active' && $personal->child->plan == 1){
                    $childLeft = 0;
                    $childRight = 0;
                    foreach($personal->child->personal as $childsChildren){
                        if($childsChildren->side == 'Left' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childLeft++;
                        }elseif($childsChildren->side == 'Right' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childRight++;
                        }
                    }
                    if($childLeft > 0 && $childRight > 0){
                        array_push($rightMatch, $personal->child->email);
                    }
                    array_push($rightActive, $personal->child->email);
                }
            }

            //Adding match bonus and other data needed for payouts
            $usersToPay[$user->email]['user_id'] = $user->id;
            $usersToPay[$user->email]['btc_address'] = $user->wallet_address;
            $usersToPay[$user->email]['left_match'] = $leftMatch;
            $usersToPay[$user->email]['right_match'] = $rightMatch;
            $usersToPay[$user->email]['left_active'] = $leftActive;
            $usersToPay[$user->email]['right_active'] = $rightActive;
        }

        //Calculating users match bonus
        foreach($usersToPay as $key => $user){
            switch ($user) {
                case count($user['left_match']) > 2 && count($user['right_match']) > 2:
                $usersToPay[$key]['match_bonus'] = 0.000000;
                ($usersToPay[$user['left_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['left_match'][1]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][1]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['left_match'][2]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][2]]['team_commission']*0.10000 : '' ;
                ($usersToPay[$user['right_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['right_match'][1]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][1]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['right_match'][2]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][2]]['team_commission']*0.10000 : '' ;
                    break;
                case count($user['left_match']) > 1 && count($user['right_match']) > 1:
                $usersToPay[$key]['match_bonus'] = 0.000000;
                ($usersToPay[$user['left_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['left_match'][1]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][1]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['right_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['right_match'][1]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][1]]['team_commission']*0.20000 : '' ;

                    break;
                case count($user['left_match']) > 0 && count($user['right_match']) > 0:
                $usersToPay[$key]['match_bonus'] = 0.000000;
                ($usersToPay[$user['left_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000 : '' ;
                ($usersToPay[$user['right_match'][0]]['btc_address'] !== null) ? $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000 : '' ;

                    break;
                default:
                    $usersToPay[$key]['match_bonus'] = 0.00000;
            }
        }

        //User Paid out array for die dump testing
        $usersPaidOut = [];

        foreach($usersToPay as $key => $affiliate){

            $totalActiveChildren = count($affiliate['left_active'])+count($affiliate['right_active']);
            //Block io credentials
            $apiKey = "2f8f-2f0b-ec72-74b6";
            $pin = "Scott1994Amir1993";
            $version = 2; // API version
            $block_io = new \BlockIo($apiKey, $pin, $version);

            //Team commissions before calculating deductions
            $amountBeingPaidOutTeamCommission = $affiliate['team_commission'];
            $bonusAmount = $affiliate['match_bonus'];
            $qualifiedForPayout = false;

            if(!array_key_exists('direct_commission', $affiliate)){
                $affiliate['direct_commission'] = 0.000000;
            }

            $amountBeingPaidOutDirectCommission = $affiliate['direct_commission'];
            //Determining what the users daily maximum is
            if(count($affiliate['left_active']) > 4 && count($affiliate['right_active']) > 4){
                $dailyMax = 45;
                $qualifiedForPayout = true;
            }elseif($affiliate['plan'] != 'Sales_Rep' && count($affiliate['left_active']) > 0 && count($affiliate['right_active']) > 0){
                $dailyMax = 30;
                $qualifiedForPayout = true;
            }elseif($affiliate['plan'] == 'Sales_Rep' && count($affiliate['left_active']) > 0 && count($affiliate['right_active']) > 0 && $totalActiveChildren > 2){
                $dailyMax = 30;
                $qualifiedForPayout = true;
            }else{
                $dailyMax = 0;
            }

            if($affiliate['direct_commission'] > 0.000000){
                $qualifiedForPayout = true;
            }

            //Checking if user qualifies and has mad commission
            if(($qualifiedForPayout == true && $affiliate['btc_address'] !== null && $affiliate['btc_address'] !== '' && ($amountBeingPaidOutTeamCommission > 0.00000 || $bonusAmount > 0.00000)) || ($affiliate['direct_commission'] > 0.00000 && $affiliate['btc_address'] !== null && $affiliate['btc_address'] !== '')){

                //Seeing if user has hit daily maximum
                if($affiliate['usersPaidOn'] >= $dailyMax){
                    //Getting live price of bitcoin for daily max deduction
                    $client = new \GuzzleHttp\Client();
                    $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
                    $jsonBtcResponse = json_decode($btcResponse->getBody());
                    $currentBtcPrice = $jsonBtcResponse[0]->price_usd;

                    $amountBeingPaidOutTeamCommission = ($dailyMax*100)/$currentBtcPrice;
                }

                //Getting network fee from block io
                $amountWithoutNetworkFee = round(($amountBeingPaidOutTeamCommission+$bonusAmount+$amountBeingPaidOutDirectCommission), 5);
                $estNetworkFeeResponse = $block_io->get_network_fee_estimate(array('to_address' => $affiliate['btc_address'], 'amount' => $amountWithoutNetworkFee));

                if($estNetworkFeeResponse->status === 'success'){

                    //Amount sending minus netowkr fee
                    $estNetworkFee = $estNetworkFeeResponse->data->estimated_network_fee;
                    $finalAmountToSend = $amountWithoutNetworkFee - $estNetworkFee;

                    //Users paid out array for die dump testing and confirming logic is correct
                    $usersPaidOut[$key] = [
                        'user_id' => $affiliate['user_id'],
                        'bonus_amount_paid' => $bonusAmount,
                        'team_commission_paid' => $amountBeingPaidOutTeamCommission,
                        'daily_max' => $dailyMax,
                        'wallet_paid_to_address' => $affiliate['btc_address'],
                        'network_fee' => $estNetworkFee,
                        'final_amount_sent' => $finalAmountToSend,
//                        'side_paid_on' => $affiliate['payoutSide'],
                        'direct_commission_paid' => $amountBeingPaidOutDirectCommission
                    ];

                    // $payout = $block_io->withdraw(array('amount' => $finalAmountToSend, 'to_addresses' => $affiliate['btc_address']));

                    //Test dummy payout response object to test volume flushing
//                    $payout = (object)[
//                        'status' => 'success'
//                    ];

                    // if($payout->status === 'success'){
                    //
                    //     //Volume Flush
                    //     $user = User::where('id', $affiliate['user_id'])->first();
                    //
                    //     if($affiliate['payoutSide'] === 'last_paid_out_right'){
                    //         $user->last_paid_out_right = Carbon::now();
                    //     }elseif($affiliate['payoutSide'] === 'last_paid_out_left'){
                    //         $user->last_paid_out_left = Carbon::now();
                    //     }
                    //
                    //     if($affiliate['direct_commission'] > 0.000000){
                    //         $user->last_paid_out_direct = Carbon::now();
                    //     }
                    //
                    //     $user->save();
                    //
                    //     //Save Withdrawl
                    //     $newWithdrawl = new Withdrawl;
                    //     $newWithdrawl->user_id = $affiliate['user_id'];
                    //     $newWithdrawl->amount = $finalAmountToSend;
                    //     $newWithdrawl->bonus_amount = $bonusAmount;
                    //     $newWithdrawl->network_fee = $estNetworkFee;
                    //     $newWithdrawl->wallet_paid_to_address = $affiliate['btc_address'];
                    //     $newWithdrawl->direct_amount = $amountBeingPaidOutDirectCommission;
                    //     $newWithdrawl->status = 'Success';
                    //     $newWithdrawl->save();
                    //
                    //     Mail::to($key)->send(new NewPayout(['payoutData' => null, 'admin' => false]));
                    // }else{
                    //     //Save withdrawl Error
                    //     $newWithdrawlError = new Withdrawl;
                    //     $newWithdrawlError->user_id = $affiliate['user_id'];
                    //     $newWithdrawlError->amount = $finalAmountToSend;
                    //     $newWithdrawlError->bonus_amount = $bonusAmount;
                    //     $newWithdrawlError->network_fee = $estNetworkFee;
                    //     $newWithdrawlError->wallet_paid_to_address = $affiliate['btc_address'];
                    //     $newWithdrawlError->direct_amount = $amountBeingPaidOutDirectCommission;
                    //     $newWithdrawlError->status = 'Fail';
                    //     $newWithdrawlError->error_message = $estNetworkFeeResponse->data->error_message;
                    //     $newWithdrawlError->save();
                    // }
                }else{
                    //Save withdrawl Error On Estimate network fee
                    // $newWithdrawlError = new Withdrawl;
                    // $newWithdrawlError->user_id = $affiliate['user_id'];
                    // $newWithdrawlError->amount = $amountWithoutNetworkFee;
                    // $newWithdrawlError->bonus_amount = $bonusAmount;
                    // $newWithdrawlError->wallet_paid_to_address = $affiliate['btc_address'];
                    // $newWithdrawlError->status = 'Fail';
                    // $newWithdrawlError->error_message = $estNetworkFeeResponse->data->error_message;
                    // $newWithdrawlError->save();
                }
            }

        }
        dd($usersToPay);
        Mail::to('amir@trio.software')->send(new NewPayout(['payoutData' => $usersPaidOut, 'admin' => true]));
        Mail::to('sphoun@trio.software')->send(new NewPayout(['payoutData' => $usersPaidOut, 'admin' => true]));
        Mail::to('joseph@trio.software')->send(new NewPayout(['payoutData' => $usersPaidOut, 'admin' => true]));
        Mail::to('justindavis214@yahoo.com')->send(new NewPayout(['payoutData' => $usersPaidOut, 'admin' => true]));

        return response()->json('Succesfully Paid');
    }

}
