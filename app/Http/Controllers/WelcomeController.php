<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class WelcomeController extends Controller
{

  public function userAgreement(){
    return view('legal.userAgreement');
  }

  public function privacyPolicy(){
    return view('legal.privacyPolicy');
  }

  public function affiliateAgreement(){
    return view('legal.affiliateAgreement');
  }

  public function enterSponsor(){
    return view('sponsor.index');
  }

  public function compPlan(){
    return view('legal.compPlan');
  }
}
