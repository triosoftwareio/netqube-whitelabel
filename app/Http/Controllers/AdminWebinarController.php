<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alert;
use Auth;
use App\User;
use App\Subscription;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use App\Webinar;
use App\UploadedWebinar;
use App\LatestUpdate;
use App\UpcomingUpdate;

class AdminWebinarController extends Controller
{
    public function removeWebinar($id){
      $webinar = Webinar::where('id', $id)->first();
      $webinar->delete();

      return redirect('/webinars');
    }

    public function removePrevWebinar($id){
      $webinar = UploadedWebinar::where('id', $id)->first();
      $webinar->delete();

      return redirect('/webinars');
    }

    public function editWebinar($id){
      $webinar = Webinar::where('id', $id)->first();

      return view('admin.editwebinar', [
        'webinar' => $webinar
      ]);
    }

    public function editPreviousWebinar($id){
      $webinar = UploadedWebinar::where('id', $id)->first();

      return view('admin.editprevwebinar', [
        'webinar' => $webinar
      ]);
    }

    public function updateWebinarInfo($id, Request $request){

      $webinar = Webinar::where('id', $id)->first();
      $webinar->name = $request->webinarname;
      $webinar->prettyTime = $request->webinartime;
      $webinar->prettyDate = $request->webinardate;
      $webinar->link = $request->webinarlink;
      $webinar->save();

      return response()->json('Successfully updated webinar');
    }

    public function updatePrevWebinarInfo($id, Request $request){

      $webinar = UploadedWebinar::where('id', $id)->first();
      $webinar->name = $request->webinarname;
      $webinar->date = $request->webinardate;
      $webinar->link = $request->webinarlink;
      $webinar->save();

      return response()->json('Successfully updated previous webinar');
    }

    public function uploadWebinar(Request $request){
      UploadedWebinar::create([
        'name' => $request['webinarname'],
        'date' => $request['webinardate'],
        'link' => $request['webinarlink'],
      ]);

      return redirect('/home');
    }

    public function addWebinar(Request $request){
      Webinar::create([
        'name' => $request['webinarname'],
        'prettyDate' => $request['webinardate'],
        'prettyTime' => $request['webinartime'],
        'link' => $request['webinarlink'],
        'language' => 'English'
      ]);
      return redirect('/home');
    }
}
