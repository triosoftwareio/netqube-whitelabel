<?php
namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Wallet;
use App\Payment;
use App\Plan;
use DB;
use App\Binary;
use App\Traits\CaptchaTrait;
use App\Mail\WelcomeUser;
use App\Mail\NewSignup;
use Illuminate\Support\Facades\Mail;
use Meng\AsyncSoap\Guzzle\Factory;
use GuzzleHttp\Client;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     * @param SoapWrapper $soapWrapper
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
   * Show the application registration form.
   *
   * @return \Illuminate\Http\Response
   */
    public function showRegistrationForm(){
      $countryCodes = array(
         'US' => 'United States',
         'CA' => 'Canada',
         'IN' => 'India',
         'AF' => 'Afghanistan',
         'AL' => 'Albania',
         'DZ' => 'Algeria',
         'AS' => 'American Samoa',
         'AD' => 'Andorra',
         'AO' => 'Angola',
         'AI' => 'Anguilla',
         'AQ' => 'Antarctica',
         'AG' => 'Antigua And Barbuda',
         'AR' => 'Argentina',
         'AM' => 'Armenia',
         'AW' => 'Aruba',
         'AU' => 'Australia',
         'AT' => 'Austria',
         'AZ' => 'Azerbaijan',
         'BS' => 'Bahamas, The',
         'BH' => 'Bahrain',
         'BD' => 'Bangladesh',
         'BB' => 'Barbados',
         'BY' => 'Belarus',
         'BE' => 'Belgium',
         'BZ' => 'Belize',
         'BJ' => 'Benin',
         'BM' => 'Bermuda',
         'BT' => 'Bhutan',
         'BO' => 'Bolivia',
         'BA' => 'Bosnia And Herzegovina',
         'BW' => 'Botswana',
         'BV' => 'Bouvet Island',
         'BR' => 'Brazil',
         'IO' => 'British Indian Ocean Territory',
         'BN' => 'Brunei',
         'BG' => 'Bulgaria',
         'BF' => 'Burkina Faso',
         'MM' => 'Burma',
         'BI' => 'Burundi',
         'KH' => 'Cambodia',
         'CM' => 'Cameroon',
         'CV' => 'Cape Verde',
         'KY' => 'Cayman Islands',
         'CF' => 'Central African Republic',
         'TD' => 'Chad',
         'CL' => 'Chile',
         'CN' => 'China',
         'CX' => 'Christmas Island',
         'CC' => 'Cocos (keeling) Islands',
         'CO' => 'Colombia',
         'KM' => 'Comoros',
         'CG' => 'Congo (brazzaville) ',
         'CD' => 'Congo (kinshasa)',
         'CK' => 'Cook Islands',
         'CR' => 'Costa Rica',
         'CI' => 'CÔte D’ivoire',
         'HR' => 'Croatia',
         'CU' => 'Cuba',
         'CW' => 'CuraÇao',
         'CY' => 'Cyprus',
         'CZ' => 'Czech Republic',
         'DK' => 'Denmark',
         'DJ' => 'Djibouti',
         'DM' => 'Dominica',
         'DO' => 'Dominican Republic',
         'EC' => 'Ecuador',
         'EG' => 'Egypt',
         'SV' => 'El Salvador',
         'GQ' => 'Equatorial Guinea',
         'ER' => 'Eritrea',
         'EE' => 'Estonia',
         'ET' => 'Ethiopia',
         'FK' => 'Falkland Islands (islas Malvinas)',
         'FO' => 'Faroe Islands',
         'FJ' => 'Fiji',
         'FI' => 'Finland',
         'FR' => 'France',
         'GF' => 'French Guiana',
         'PF' => 'French Polynesia',
         'TF' => 'French Southern And Antarctic Lands',
         'GA' => 'Gabon',
         'GM' => 'Gambia, The',
         'GE' => 'Georgia',
         'DE' => 'Germany',
         'GH' => 'Ghana',
         'GI' => 'Gibraltar',
         'GR' => 'Greece',
         'GL' => 'Greenland',
         'GD' => 'Grenada',
         'GP' => 'Guadeloupe',
         'GU' => 'Guam',
         'GT' => 'Guatemala',
         'GG' => 'Guernsey',
         'GN' => 'Guinea',
         'GW' => 'Guinea-bissau',
         'GY' => 'Guyana',
         'HT' => 'Haiti',
         'HM' => 'Heard Island And Mcdonald Islands',
         'HN' => 'Honduras',
         'HK' => 'Hong Kong',
         'HU' => 'Hungary',
         'IS' => 'Iceland',
         //'IN' => 'India',
         'ID' => 'Indonesia',
         'IR' => 'Iran',
         'IQ' => 'Iraq',
         'IE' => 'Ireland',
         'IM' => 'Isle Of Man',
         'IL' => 'Israel',
         'IT' => 'Italy',
         'JM' => 'Jamaica',
         'JP' => 'Japan',
         'JE' => 'Jersey',
         'JO' => 'Jordan',
         'KZ' => 'Kazakhstan',
         'KE' => 'Kenya',
         'KI' => 'Kiribati',
         'KP' => 'Korea, North',
         'KR' => 'Korea, South',
         'KW' => 'Kuwait',
         'KG' => 'Kyrgyzstan',
         'LA' => 'Laos',
         'LV' => 'Latvia',
         'LB' => 'Lebanon',
         'LS' => 'Lesotho',
         'LR' => 'Liberia',
         'LY' => 'Libya',
         'LI' => 'Liechtenstein',
         'LT' => 'Lithuania',
         'LU' => 'Luxembourg',
         'MO' => 'Macau',
         'MK' => 'Macedonia',
         'MG' => 'Madagascar',
         'MW' => 'Malawi',
         'MY' => 'Malaysia',
         'MV' => 'Maldives',
         'ML' => 'Mali',
         'MT' => 'Malta',
         'MH' => 'Marshall Islands',
         'MQ' => 'Martinique',
         'MR' => 'Mauritania',
         'MU' => 'Mauritius',
         'YT' => 'Mayotte',
         'MX' => 'Mexico',
         'FM' => 'Micronesia, Federated States Of',
         'MD' => 'Moldova',
         'MC' => 'Monaco',
         'MN' => 'Mongolia',
         'ME' => 'Montenegro',
         'MS' => 'Montserrat',
         'MA' => 'Morocco',
         'MZ' => 'Mozambique',
         'NA' => 'Namibia',
         'NR' => 'Nauru',
         'NP' => 'Nepal',
         'NL' => 'Netherlands',
         'NC' => 'New Caledonia',
         'NZ' => 'New Zealand',
         'NI' => 'Nicaragua',
         'NE' => 'Niger',
         'NG' => 'Nigeria',
         'NU' => 'Niue',
         'NF' => 'Norfolk Island',
         'MP' => 'Northern Mariana Islands',
         'NO' => 'Norway',
         'OM' => 'Oman',
         'PK' => 'Pakistan',
         'PW' => 'Palau',
         'PA' => 'Panama',
         'PG' => 'Papua New Guinea',
         'PY' => 'Paraguay',
         'PE' => 'Peru',
         'PH' => 'Philippines',
         'PN' => 'Pitcairn Islands',
         'PL' => 'Poland',
         'PT' => 'Portugal',
         'PR' => 'Puerto Rico',
         'QA' => 'Qatar',
         'RE' => 'Reunion',
         'RO' => 'Romania',
         'RU' => 'Russia',
         'RW' => 'Rwanda',
         'BL' => 'Saint Barthelemy',
         'SH' => 'Saint Helena, Ascension, And Tristan Da Cunha',
         'KN' => 'Saint Kitts And Nevis',
         'LC' => 'Saint Lucia',
         'MF' => 'Saint Martin',
         'PM' => 'Saint Pierre And Miquelon',
         'VC' => 'Saint Vincent And The Grenadines',
         'WS' => 'Samoa',
         'SM' => 'San Marino',
         'ST' => 'Sao Tome And Principe',
         'SA' => 'Saudi Arabia',
         'SN' => 'Senegal',
         'RS' => 'Serbia',
         'SC' => 'Seychelles',
         'SL' => 'Sierra Leone',
         'SG' => 'Singapore',
         'SX' => 'Sint Maarten',
         'SK' => 'Slovakia',
         'SI' => 'Slovenia',
         'SB' => 'Solomon Islands',
         'SO' => 'Somalia',
         'ZA' => 'South Africa',
         'GS' => 'South Georgia And South Sandwich Islands',
         'SS' => 'South Sudan',
         'ES' => 'Spain',
         'LK' => 'Sri Lanka',
         'SD' => 'Sudan',
         'SR' => 'Suriname',
         'SZ' => 'Swaziland',
         'SE' => 'Sweden',
         'CH' => 'Switzerland',
         'SY' => 'Syria',
         'TW' => 'Taiwan',
         'TJ' => 'Tajikistan',
         'TZ' => 'Tanzania',
         'TH' => 'Thailand',
         'TL' => 'Timor-leste',
         'TG' => 'Togo',
         'TK' => 'Tokelau',
         'TO' => 'Tonga',
         'TT' => 'Trinidad And Tobago',
         'TN' => 'Tunisia',
         'TR' => 'Turkey',
         'TM' => 'Turkmenistan',
         'TC' => 'Turks And Caicos Islands',
         'TV' => 'Tuvalu',
         'UG' => 'Uganda',
         'UA' => 'Ukraine',
         'AE' => 'United Arab Emirates',
         'GB' => 'United Kingdom',
         //'US' => 'United States', //added at the top
         'UY' => 'Uruguay',
         'UZ' => 'Uzbekistan',
         'VU' => 'Vanuatu',
         'VA' => 'Vatican City',
         'VE' => 'Venezuela',
         'VN' => 'Vietnam',
         'VG' => 'Virgin Islands, British',
         'VI' => 'Virgin Islands, United States ',
         'WF' => 'Wallis And Futuna',
         'EH' => 'Western Sahara',
         'YE' => 'Yemen',
         'ZM' => 'Zambia',
         'ZW' => 'Zimbabwe');

         if($_GET['sponsor'] == "") return redirect('/enter/sponsor');

         $client = new \GuzzleHttp\Client();
         $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');

         $jsonBtcResponse = json_decode($btcResponse->getBody());

         $btcPrice = $jsonBtcResponse[0]->price_usd;

         if($_GET['package'] == 'Pro_Semi') $price = 600;
         elseif($_GET['package'] == 'Sales_Rep') $price = 97;
         elseif($_GET['package'] == 'Pro_Monthly') $price = 100;
         else $price = 1000;
         // if($_GET['package'] == 'BasicAnnual') $price = 857;
         // if($_GET['package'] == 'ProSemi') $price = 847;
         // if($_GET['package'] == 'ProAnnual') $price = 1497;
         // if($_GET['package'] == 'SignalSemi') $price = 497;
         // if($_GET['package'] == 'SignalAnnual') $price = 887;

         // phpinfo();
         $costBtc = round(($price / $btcPrice), 8);

         // Pass values to create method by setting hidden values in form
         // Add values to db on creation
         // Create only that address
         // $2 to create new wallet for new payment

         return view('auth.register', [
           'btcPrice' => $btcPrice,
           'costBtc' => $costBtc,
           'countryCodes' => $countryCodes
         ]);
    }

    public function getBinaryDownline($data, $startId, $side){
        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'amount' => $data[$startId]['amount'],
            'left_volume' => 0,
            'right_volume' => 0,
            'left_child' => [],
            'right_child' => [],
            'placement_id' => NULL
        );

        if($side == 'Left'){
            if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
                $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child'], $side);
                $directDownline['left_child'] = $left_child;
                $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['amount'];
                $directDownline['placement_id'] = $left_child['placement_id'];
            }else{
                    $directDownline['placement_id'] = $startId;
            }
        }else{
            if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
                $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child'], $side);
                $directDownline['right_child'] = $right_child;
                $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['amount'];
                $directDownline['placement_id'] = $right_child['placement_id'];
            }else{
                $directDownline['placement_id'] = $startId;
            }
        }




        return $directDownline;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['captcha'] = CaptchaTrait::captchaCheck();
        return Validator::make($data, [
            'username' => 'required|string|max:20|unique:users',
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'sponsorId' => 'required|string',
            'g-recaptcha-response'  => 'required',
            'captcha'               => 'required|min:1',
            'country' => 'required|string',
            'postalCode' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    protected function create(array $data)
    {
      // $sponsorUser = User::where('username', $data['sponsorId'])->first();

      //init wallet api        / Check if ltc or btc was clicked
      $version = 2; // API version
      $pin = "Scott1994Amir1993";
      if($data['clickedButton'] == 'btc') $apiKey = "2f8f-2f0b-ec72-74b6";

      $block_io = new \BlockIo($apiKey, $pin, $version);
      //create wallet address
      $rndString = substr(md5(uniqid(mt_rand(), true)), 0, 8);
      $newAddressInfo = $block_io->get_new_address(array('label' => $data['email'].'.'.$rndString));

      if($newAddressInfo->status == "success"){

        $address = $newAddressInfo->data->address;

        $dbWallet = new Wallet;
        if($data['clickedButton'] == 'btc') $dbWallet->address = $address;

        $dbWallet->label = $data['email'].date('Y-m-d H:i:s');
        $dbWallet->save();

        $package = Plan::where('name', $data['package'])->first();
        $packageId = $package->id;

          $sponsorUser = User::where('username', $data['sponsorId'])->first();
          $sponsorSide = $sponsorUser->default_position;
          $testQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

          $parents = [];

          foreach($testQuery as $parent){
              $parents[$parent->id]['id'] = $parent->id;
              $parents[$parent->id]['name'] = $parent->parent_name;
              $parents[$parent->id]['username'] = $parent->username;
              $parents[$parent->id]['status'] = $parent->status;
              $parents[$parent->id]['plan_name'] = $parent->name;
              $parents[$parent->id]['amount'] = $parent->amount;
              $parents[$parent->id]['left_child'] = $parent->left_child;
              $parents[$parent->id]['right_child'] = $parent->right_child;
          }

          $downline = $this->getBinaryDownline($parents, $sponsorUser->id, $sponsorSide);

          if($data['clickedButton'] == 'btc'){
            $name = $data['firstName'] . ' ' . $data['lastName'];
            $today = Carbon::today();
            if($packageId == 1) $days = 183;
            else if($packageId == 2) $days = 30;
            else if($packageId == 3 || $packageId == 8) $days = 365;
            $expiryDate = $today->addDays($days);
            $mySqlExpiryDate = $expiryDate->toDateTimeString();
            // dd($mySqlExpiryDate);

            $user = User::create([
                'username' => $data['username'],
                'first_name' => $data['firstName'],
                'last_name' => $data['lastName'],
                'name' => $name,
                'email' => $data['email'],
                'plan' => $packageId,
                'status' => 'Inactive',
                'avatar' => 'ttradericon.png',
                'default_position' => 'Left',
                'password' => bcrypt($data['password']),
                'payment_address' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:'.$address.'?amount='.$data['costAmt'],
                'btcPrice' => $data['btcPrice'],
                'addressType' => 'btc',
                'country' => $data['country'],
                'postal_code' => $data['postalCode'],
                'phone_number' => $data['phoneNum'],
                'city' => $data['city'],
                'address' => $data['address'],
                'sponsor_name' => $data['sponsorId'],
                'trader_demo' => 1,
                'plan' => $packageId,
                'expiry_date' => $mySqlExpiryDate
            ]);
            $user->api_key = uniqid($user->id, true);
            $user->save();
          }

        if($sponsorSide == 'Left'){
            if(empty($downline['left_child'])){
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Left';
                $sponsor->placement_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }elseif(!empty($downline['left_child']['placement_id'])){
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Left';
                $sponsor->placement_id = $downline['left_child']['placement_id'];
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }else{
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Left';
                $sponsor->placement_id = NULL;
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }
        }else{
            if(empty($downline['right_child'])){
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Right';
                $sponsor->placement_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }elseif(!empty($downline['right_child']['placement_id'])){
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Right';
                $sponsor->placement_id = $downline['right_child']['placement_id'];
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }else{
                $sponsor = new Binary;
                $sponsor->parent_id = User::where('username', $data['sponsorId'])->first()->id;
                $sponsor->side = 'Right';
                $sponsor->placement_id = NULL;
                $sponsor->child_id = $user->id;
                $sponsor->save();
            }
        }

        $payment = new Payment;
        $payment->wallet_id = $dbWallet->id;
        $payment->user_id = $user->id;
        if($data['clickedButton'] == 'btc') $payment->costBtc = $data['costAmt'];
        $payment->amount = $package->amount;
        $payment->save();

        Mail::to($data['email'])->send(new WelcomeUser(['user' => $data, 'subject' => 'Welcome To Trio Trader']));
        Mail::to($sponsorUser->email)->send(new NewSignup(['user' => $sponsorUser, 'sponsoredUser' => $data, 'subject' => 'Congrats! You Have Signed Up A New User']));

        return $user;
      }else{
          return NULL;
      }

    }
}
