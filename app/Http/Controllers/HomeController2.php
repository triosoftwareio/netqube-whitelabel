<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Wallet;
use Auth;
use App\User;
use App\Plan;
use App\Withdrawl;
use App\SupportTicket;
use App\Binary;
use App\Update;
use App\Summary;
use Carbon\Carbon;
use DB;
use App\Webinar;
use App\UploadedWebinar;
use App\Course;
use App\UserCompletedCourses;
use App\UserCompletedVideos;
use App\Video;
use App\Exchange;

class HomeController2 extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getBinaryDownline($data, $startId){
        $thisUser = User::where('id', $startId)->first();
        if($thisUser->status == 'Active' && $thisUser->plan == 1) $volume_amount = 150;
        else $volume_amount = 0;
        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'volume_amount' => $volume_amount,
            'left_volume' => 0,
            'right_volume' => 0,
            'left_child' => [],
            'right_child' => []
        );

        if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
            $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child']);
            $directDownline['left_child'] = $left_child;
            $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['volume_amount'];
        }
        if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
            $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child']);
            $directDownline['right_child'] = $right_child;
            $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['volume_amount'];
        }

        return $directDownline;
    }

    public function getFullDownline($userId){

      $currentUser = User::with('binaryPlan')->where('id', $userId)->first();
      $amount = $currentUser->binaryPlan->amount;

      $directDownline = array(
        'id' => $userId,
        'username' => $currentUser->username,
        'name' => $currentUser->email,
        'status' => $currentUser->status,
        'package' => $amount,
        'leftVolume' => 0,
        'rightVolume' => 0,
        'children' => []
      );

      $children = Binary::where('placement_id', $userId)->get();

      $count = 0;
      foreach($children as $child){
        $count++;
        $nextChildren = $this->getFullDownline($child->child_id);
        $directDownline['children'][$child->child_id] = $nextChildren;

        if($count == 1){
          $directDownline['leftVolume'] = $directDownline['leftVolume']+$nextChildren['package']+$nextChildren['leftVolume']+$nextChildren['rightVolume'];
        }else{
          $directDownline['rightVolume'] = $directDownline['rightVolume']+$nextChildren['package']+$nextChildren['rightVolume']+$nextChildren['leftVolume'];
        }
      }

      return $directDownline;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       $user = User::where('id', Auth::id())->first();
       $userId = $user->id;

       $exchanges = Exchange::all();
       $courses = Course::count();
       $videos = Video::count();
       $completedCourses = UserCompletedCourses::where('userId', $userId)->count();
       $completedVideos = UserCompletedVideos::where('userId', $userId)->count();
       $userCompletedVideosList = UserCompletedVideos::where('userId', Auth::id())->get();

       $coursesList = Course::with('video')->get();
       $videoList = Video::all();

         $balance = Auth::user()->balance;
         $payments = Payment::where('user_id', Auth::id())->where('status', 'Complete')->get();
         $plan = Plan::where('id', Auth::user()->plan)->first();
         $updates = Update::selectRaw('year(created_at) year, monthname(created_at) month, count(*) num')
            ->groupBy('year', 'month')
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get()
            ->toArray();
          $summaries = Summary::selectRaw('year(created_at) year, monthname(created_at) month, count(*) num')
            ->groupBy('year', 'month')
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get()
            ->toArray();
          $usersRecruited = Binary::where('parent_id', Auth::id())->count();
         //  $downline = $this->getFullDownline(Auth::id());
         // $percentage = Auth::user()->balance/($plan->amount*2)*100;
         $percentage = Auth::user()->percentage;
         $withdrawls = Withdrawl::where('user_id', Auth::id())->get();

         $webinars = Webinar::orderBy('id', 'desc')->take(2)->get();
         $prevWebinars = UploadedWebinar::orderBy('id', 'desc')->take(2)->get();

         $client = new \GuzzleHttp\Client();
         $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
         // $ltcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/litecoin/');
         // http://billionbitclub.com:8081/signals/
         // https://cryptotrader.trio.software:8081/signals/
         // $response = $client->request('GET', 'http://billionbitclub.com:8081/signals/'.$user->default_exchange.'/get');
         $response = $client->request('GET', 'https://cryptotrader.trio.software/api/v1.0/signals/Bittrex/get');


         $jsonResponse = json_decode($response->getBody());

         $currentUser = User::where('id', Auth::id())->first();

         $now = Carbon::now();

         if($currentUser->status == 'Inactive'){
           $inactive = true;
           $payment = Payment::where('user_id', Auth::id())->where('status', 'Pending')->first();
           if($payment){
             $wallet = Wallet::where('id', $payment->wallet_id)->first();

             $lastTime = new Carbon($currentUser->last_price_time);
             $timePassed = $now->diffInSeconds($lastTime);

             if($timePassed >= 1800 || $currentUser->last_price_time == NULL){
               $jsonBtcResponse = json_decode($btcResponse->getBody());
               // $jsonLtcResponse = json_decode($ltcResponse->getBody());
               $currentBtcPrice = $jsonBtcResponse[0]->price_usd;
               // $currentLtcPrice = $jsonLtcResponse[0]->price_usd;
               $currentUser->last_price_time = $now;
             } else {
               //price stays the same
               $currentBtcPrice = $currentUser->btcPrice;
               // $currentLtcPrice = $currentUser->ltcPrice;
             }
             //save price
             $currentUser->btcPrice = $currentBtcPrice;
             $currentUser->save();

             $lastTime = new Carbon($currentUser->last_price_time);
             $endTime = $lastTime->addMinutes(30)->toRfc2822String();
             // $currentUser->ltcPrice = $currentLtcPrice;

             //init wallet api (BTC)
             $version = 2; // API version
             $pin = "Scott1994Amir1993";
             $apiKey = "2f8f-2f0b-ec72-74b6";

             $block_io = new \BlockIo($apiKey, $pin, $version);
             $blockBalance = $block_io->get_address_balance(array('addresses' => $wallet->address));
             $pendingBalance = floatval($blockBalance->data->balances[0]->pending_received_balance);
             $receivedBalance = floatval($blockBalance->data->balances[0]->available_balance);
             $totalBalance = $pendingBalance + $receivedBalance;
             // https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:3NKsP8j7V1J49RfntdSYq8LYWckQeU56u5?amount=0.057336

             $package = floatval(User::with('binaryPlan')->where('id', Auth::id())->first()->binaryPlan->amount);
             if(Auth::user()->addressType == 'btc'){
               $package = round(($package / $currentBtcPrice),8);
               $payment->costBtc = $package;
               $difference = $package-$totalBalance;
               $thisLink = strstr(Auth::user()->payment_address, "bitcoin:", true);
               $thisAddress = strstr(substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "bitcoin:") + 8), '?amount=', true);
               $newAddress = $thisLink . "bitcoin:" . $thisAddress . "?amount=" . $difference;
               $currentUser->payment_address = $newAddress;
             }
             // else {
             //   $package = round(($package / $currentLtcPrice),8);
             //   $payment->costLtc = $package;
             //   $apiKey = "8e60-1313-5279-43b6";
             //   $thisLink = strstr(Auth::user()->payment_address, "litecoin:", true);
             //   $thisAddress = strstr(substr(Auth::user()->payment_address_ltc, strpos(Auth::user()->payment_address_ltc, "litecoin:") + 8), '?amount=', true);
             //   $newAddress = $thisLink . "litecoin:" . $thisAddress . "?amount=" . $package;
             //   $currentUser->payment_address_ltc = $newAddress;
             // }

             // if(isset($blockBalance->data->balances)){}
             // else $blockBalance = $block_io->get_address_balance(array('addresses' => $wallet->ltc_address));

             if($pendingBalance >= $package || $receivedBalance >= $package || $totalBalance >= $package){
               $currentUser->status = 'Active';
               $payment->status = 'Complete';
               $currentUser->last_renewal = $now;
               $currentUser->last_paid_out_direct = $now;
             }
             $currentUser->save();
             $payment->save();
           }
         }else{
           $inactive = false;
           $endTime = 0;
           $pendingBalance = 0;
           $receivedBalance = 0;
         }

         $recentUsers = User::with('binaryPlan')->orderBy('created_at', 'DESC')->take(5)->get();
         $recentWithdrawls = Withdrawl::with('user')->orderBy('created_at', 'DESC')->take(10)->get();
         $ticketCount = SupportTicket::count();


         $testQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

         $parents = [];

         foreach($testQuery as $parent){
             $parents[$parent->id]['id'] = $parent->id;
             $parents[$parent->id]['name'] = $parent->parent_name;
             $parents[$parent->id]['username'] = $parent->username;
             $parents[$parent->id]['status'] = $parent->status;
             $parents[$parent->id]['plan_name'] = $parent->name;
             $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
             $parents[$parent->id]['left_child'] = $parent->left_child;
             $parents[$parent->id]['right_child'] = $parent->right_child;
         }

         $downline = $this->getBinaryDownline($parents, Auth::id());
         if($downline['left_volume'] > $downline['right_volume']){
             $lesserVolume = $downline['right_volume'];
         }else{
             $lesserVolume = $downline['left_volume'];
         }

         $return = [
           'inactive' => $inactive,
           'balance' => $balance,
           'payments' => $payments,
           'planName' => $plan->name,
           'dailyMax' => $plan->amount*2,
           'usersRecruited' => $usersRecruited,
           'percentage' => $percentage,
           'withdrawls' => $withdrawls,
           'ticketCount' => $ticketCount,
           'recentWithdrawls' => $recentWithdrawls,
           'recentUsers' => $recentUsers,
           'totalVolume' => $downline['left_volume']+$downline['right_volume'],
           'lesserLegVolume' => $lesserVolume,
           'updates' => $updates,
           'signals' => $jsonResponse->signals,
           'summaries' => $summaries,
           'webinars' => $webinars,
           'prevWebinars' => $prevWebinars,
           'courses' => $courses,
           'videos' => $videos,
           'userCompletedCourses' => $completedCourses,
           'userCompletedVideos' => $completedVideos,
           'userCompletedVideosList' => $userCompletedVideosList,
           'coursesList' => $coursesList,
           'videoList' => $videoList,
           'endTime' => $endTime,
           'now' => $now,
           'pendingBalance' => $pendingBalance,
           'receivedBalance' => $receivedBalance,
           'exchanges' => $exchanges,
           'user' => $user
         ];

         return view('newHome', $return);
     }
}
