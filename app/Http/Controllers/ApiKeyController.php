<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use App\ExchangeKey;
use Auth;

class ApiKeyController extends Controller
{

  public function index(){
      $currentKeys = ExchangeKey::where('user_id', Auth::id())->get();
      return view('keys.index', ['poloniex' => $currentKeys->where('exchange', 'poloniex')->first(), 'bittrex' => $currentKeys->where('exchange', 'bittrex')->first(), 'hitbtc' => $currentKeys->where('exchange', 'hitbtc')->first()]);
  }

  public function updateKeys($exchange, Request $request){

      $validatedData = $this->validate($request, [
          'apiKey' => 'required',
          'apiSecret' => 'required',
          'btcRisk' => 'required',
      ]);

      $apiKey = $request->apiKey;
      $apiSecret = $request->apiSecret;
      $btcRisk = $request->btcRisk;

      $exchangeKey = ExchangeKey::where('exchange', $exchange)->where('user_id', Auth::id())->first();

      if($exchangeKey){
          $exchangeKey->api_key = $apiKey;
          $exchangeKey->api_secret = $apiSecret;
          $exchangeKey->risk_amount = $btcRisk;
          $exchangeKey->save();
      }else{
          $newExchangeKey = new ExchangeKey;
          $newExchangeKey->api_key = $apiKey;
          $newExchangeKey->api_secret = $apiSecret;
          $newExchangeKey->exchange = $exchange;
          $newExchangeKey->risk_amount = $btcRisk;
          $newExchangeKey->user_id = Auth::id();
          $newExchangeKey->save();
      }

      $currentKeys = ExchangeKey::where('user_id', Auth::id())->get();

      return view('keys.index', ['success' => true, 'poloniex' => $currentKeys->where('exchange', 'poloniex')->first(), 'bittrex' => $currentKeys->where('exchange', 'bittrex')->first(), 'hitbtc' => $currentKeys->where('exchange', 'hitbtc')->first()]);
  }
}
