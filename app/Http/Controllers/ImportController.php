<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use Auth;
use Hash;
use App\Plan;
use App\Wallet;
use App\Upgrade;
use Carbon\Carbon;
use Twilio\Rest\Client as Client;
use Image;
use App\Binary;
use DB;
use App\Summary;
use App\Update;
use App\Webinar;
use App\UploadedWebinar;
use App\Withdrawl;
use App\UserCompletedVideos;
use App\Video;
use App\Course;
use Illuminate\Support\Collection;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Payment;

class ImportController extends Controller
{

    public function index(){
      return view('payment.selectPlan');
    }

    public function generateWallet($packageName, $price){
      $thisUser = User::where('id', Auth::id())->first();
      $thisPayment = Payment::where('user_id', $thisUser->id)->first();

      if($thisPayment){
        $client = new \GuzzleHttp\Client();
        $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
        $jsonBtcResponse = json_decode($btcResponse->getBody());

        $now = Carbon::now();

        $currentBtcPrice = $jsonBtcResponse[0]->price_usd;
        $thisUser->last_price_time = $now;
        $thisUser->plan = $packageId;
        $thisUser->btcPrice = $currentBtcPrice;
        $costAmt = round(($package->amount / $currentBtcPrice),8);

        $thisUser->payment_address = 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:' . $address . '?amount=' . $costAmt;

        $thisUser->save();

        $thisPayment->wallet_id = $dbWallet->id;
        $thisPayment->user_id = Auth::id();
        $thisPayment->costBtc = $costAmt;
        $thisPayment->amount = $package->amount;
        $thisPayment->save();

        return redirect('/home');
      }else{
        //init wallet api        / Check if ltc or btc was clicked
        $version = 2; // API version
        $pin = "Scott1994Amir1993";
        $apiKey = "2f8f-2f0b-ec72-74b6";

        $block_io = new \BlockIo($apiKey, $pin, $version);
        //create wallet address
        $rndString = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        $newAddressInfo = $block_io->get_new_address(array('label' => $thisUser->email.'.'.$rndString));

        if($newAddressInfo->status == "success"){

          $address = $newAddressInfo->data->address;

          $dbWallet = new Wallet;
          $dbWallet->address = $address;

          $dbWallet->label = $thisUser->email.date('Y-m-d H:i:s');
          $dbWallet->save();

          $package = Plan::where('name', $packageName)->first();
          $packageId = $package->id;

          $client = new \GuzzleHttp\Client();
          $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
          $jsonBtcResponse = json_decode($btcResponse->getBody());

          $now = Carbon::now();

          $currentBtcPrice = $jsonBtcResponse[0]->price_usd;
          $thisUser->last_price_time = $now;
          $thisUser->plan = $packageId;
          $thisUser->btcPrice = $currentBtcPrice;
          $costAmt = round(($package->amount / $currentBtcPrice),8);

          $thisUser->payment_address = 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:' . $address . '?amount=' . $costAmt;

          $thisUser->save();

          $payment = new Payment;
          $payment->wallet_id = $dbWallet->id;
          $payment->user_id = Auth::id();
          $payment->costBtc = $costAmt;
          $payment->amount = $package->amount;
          $payment->save();

          return redirect('/home');
        }
      }
    }

    public function importUsers(){
        $collection = (new FastExcel)->import('USER-Upload-JD.xlsx');

        foreach($collection as $newUserData){
          $sponsorUser = User::where('username', $newUserData['SPONSORID'])->first();
          $sponsorId = $sponsorUser->id;
          $sponsorSide = $newUserData['SIDE'];
          $placementId = User::where('username', $newUserData['PLACEMENTID'])->first()->id;
          $testQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

          $parents = [];

          foreach($testQuery as $parent){
              $parents[$parent->id]['id'] = $parent->id;
              $parents[$parent->id]['name'] = $parent->parent_name;
              $parents[$parent->id]['username'] = $parent->username;
              $parents[$parent->id]['status'] = $parent->status;
              $parents[$parent->id]['plan_name'] = $parent->name;
              $parents[$parent->id]['amount'] = $parent->amount;
              $parents[$parent->id]['left_child'] = $parent->left_child;
              $parents[$parent->id]['right_child'] = $parent->right_child;
          }

          $downline = $this->getBinaryDownline($parents, $sponsorId, $sponsorSide);

          $nameArray = explode(" ", $newUserData['FULLNAME']);
          if(count($nameArray) == 1){
            $firstName = $nameArray[0];
            $lastName = '';
          }
          else if(count($nameArray) == 2){
            $firstName = $nameArray[0];
            $lastName = $nameArray[1];
          }
          else if(count($nameArray) == 3){
            $firstName = $nameArray[0];
            $lastName = $nameArray[2];
          }
          $newUser = User::create([
              'username' => $newUserData['USERNAME'],
              'first_name' => $firstName,
              'last_name' => $lastName,
              'name' => $newUserData['FULLNAME'],
              'email' => $newUserData['EMAIL'],
              'plan' => 1,
              'status' => 'Inactive',
              'avatar' => 'ttradericon.png',
              'default_position' => 'Left',
              'password' => bcrypt($newUserData['PASSWORD']),
              // 'payment_address' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:'.$address.'?amount='.$data['costAmt'],
              // 'btcPrice' => $data['btcPrice'],
              'addressType' => 'btc',
              // 'country' => $data['country'],
              // 'postal_code' => $data['postalCode'],
              // 'phone_number' => $data['phoneNum'],
              // 'city' => $data['city'],
              // 'address' => $data['address'],
              'sponsor_name' => $newUserData['SPONSORID'],
              'trader_demo' => 1,
              'imported' => 1
          ]);
          $newUser->api_key = uniqid($newUser->id, true);
          $newUser->save();

          if($sponsorSide == 'Left' || $sponsorSide == 'left' || $sponsorSide == 'L'){
              if(empty($downline['left_child'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  // $sponsor->placement_id = $sponsorId;
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }elseif(!empty($downline['left_child']['placement_id'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  // $sponsor->placement_id = $downline['left_child']['placement_id'];
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }else{
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  $sponsor->placement_id = NULL;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }
          }else{
              if(empty($downline['right_child'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  // $sponsor->placement_id = $sponsorId;
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }elseif(!empty($downline['right_child']['placement_id'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  // $sponsor->placement_id = $downline['right_child']['placement_id'];
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }else{
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  $sponsor->placement_id = NULL;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }
          }
        } // End foreach user
        return view('testing.output');
    } // End importUsers

    public function importCmdUsers(){
        // $collection = (new FastExcel)->import('USER-Upload-CMD.xlsx');
        $collection = (new FastExcel)->sheet(1)->import('USER-Upload-CMD.xlsx');

        // phpinfo();
        // $myvar = 'arawrar';
        // dd($myvar);

        dd($collection);

        foreach($collection as $newUserData){
          $sponsorUser = User::where('username', $newUserData['SPONSORID'])->first();
          $sponsorId = $sponsorUser->id;
          $sponsorSide = $newUserData['SIDE'];
          $placementId = User::where('username', $newUserData['PLACEMENTID'])->first()->id;
          $testQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

          $parents = [];

          foreach($testQuery as $parent){
              $parents[$parent->id]['id'] = $parent->id;
              $parents[$parent->id]['name'] = $parent->parent_name;
              $parents[$parent->id]['username'] = $parent->username;
              $parents[$parent->id]['status'] = $parent->status;
              $parents[$parent->id]['plan_name'] = $parent->name;
              $parents[$parent->id]['amount'] = $parent->amount;
              $parents[$parent->id]['left_child'] = $parent->left_child;
              $parents[$parent->id]['right_child'] = $parent->right_child;
          }

          $downline = $this->getBinaryDownline($parents, $sponsorId, $sponsorSide);

          $nameArray = explode(" ", $newUserData['FULLNAME']);
          if(count($nameArray) == 1){
            $firstName = $nameArray[0];
            $lastName = '';
          }
          else if(count($nameArray) == 2){
            $firstName = $nameArray[0];
            $lastName = $nameArray[1];
          }
          else if(count($nameArray) == 3){
            $firstName = $nameArray[0];
            $lastName = $nameArray[2];
          }
          $newUser = User::create([
              'username' => $newUserData['USERNAME'],
              'first_name' => $firstName,
              'last_name' => $lastName,
              'name' => $newUserData['FULLNAME'],
              'email' => $newUserData['EMAIL'],
              'plan' => 1,
              'status' => 'Inactive',
              'avatar' => 'ttradericon.png',
              'default_position' => 'Left',
              'password' => bcrypt($newUserData['PASSWORD']),
              // 'payment_address' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:'.$address.'?amount='.$data['costAmt'],
              // 'btcPrice' => $data['btcPrice'],
              'addressType' => 'btc',
              // 'country' => $data['country'],
              // 'postal_code' => $data['postalCode'],
              // 'phone_number' => $data['phoneNum'],
              // 'city' => $data['city'],
              // 'address' => $data['address'],
              'sponsor_name' => $newUserData['SPONSORID'],
              'trader_demo' => 1,
              'imported' => 1
          ]);
          $newUser->api_key = uniqid($newUser->id, true);
          $newUser->save();

          if($sponsorSide == 'Left' || $sponsorSide == 'left' || $sponsorSide == 'L'){
              if(empty($downline['left_child'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  // $sponsor->placement_id = $sponsorId;
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }elseif(!empty($downline['left_child']['placement_id'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  // $sponsor->placement_id = $downline['left_child']['placement_id'];
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }else{
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Left';
                  $sponsor->placement_id = NULL;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }
          }else{
              if(empty($downline['right_child'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  // $sponsor->placement_id = $sponsorId;
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }elseif(!empty($downline['right_child']['placement_id'])){
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  // $sponsor->placement_id = $downline['right_child']['placement_id'];
                  $sponsor->placement_id = $placementId;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }else{
                  $sponsor = new Binary;
                  $sponsor->parent_id = $sponsorId;
                  $sponsor->side = 'Right';
                  $sponsor->placement_id = NULL;
                  $sponsor->child_id = $newUser->id;
                  $sponsor->save();
              }
          }
        } // End foreach user
        return view('testing.output');
    } // End importUsers

    public function getBinaryDownline($data, $startId, $side){
        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'amount' => $data[$startId]['amount'],
            'left_volume' => 0,
            'right_volume' => 0,
            'left_child' => [],
            'right_child' => [],
            'placement_id' => NULL
        );

        if($side == 'Left'){
            if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
                $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child'], $side);
                $directDownline['left_child'] = $left_child;
                $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['amount'];
                $directDownline['placement_id'] = $left_child['placement_id'];
            }else{
                    $directDownline['placement_id'] = $startId;
            }
        }else{
            if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
                $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child'], $side);
                $directDownline['right_child'] = $right_child;
                $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['amount'];
                $directDownline['placement_id'] = $right_child['placement_id'];
            }else{
                $directDownline['placement_id'] = $startId;
            }
        }

        return $directDownline;
    }
}
