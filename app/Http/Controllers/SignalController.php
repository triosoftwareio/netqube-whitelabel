<?php

namespace App\Http\Controllers;

use App\ExchangeKey;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Exchange;

class SignalController extends Controller
{

    public function index($exchange){
        $client = new \GuzzleHttp\Client();
        // $response = $client->request('GET', 'http://billionbitclub.com:8081/signals/'.$exchange.'/get');
        $response = $client->request('GET', 'https://cryptotrader.trio.software/api/v1.0/signals/'.ucwords($exchange).'/get');
        $jsonResponse = json_decode($response->getBody());
        $responseTrades = $client->request('GET', 'https://cryptotrader.trio.software/api/v1.0/trades/'.ucwords($exchange).'/'.Auth::user()->api_key.'/get');
        $jsonResponseTrades = json_decode($responseTrades->getBody());
        $isDemo = Auth::user()->trader_demo;

        $signalStatsFormatted = (object)[];
        $tradesFormatted = (object)[];

        $signalsTakenByDay = (object)[];
        $signalsCopiedByDay = (object)[];
        $profitForTheMonthByDay = (object)[];
        $profitForTheDayByHour = (object)[];

        $profitForTheMonth = 0;
        $profitForTheDay = 0;

        $signalsTaken = 0;
        $signalsCopied = 0;
        $totalSignals = 0;

        foreach($jsonResponse->signalStats as $signalStat){
            $signalStatsFormatted->{$signalStat->_id} = $signalStat;
        }

        foreach($jsonResponseTrades as $trade){
            if(!empty($trade->signal)){

                $datetime = Carbon::createFromTimestamp($trade->timestamp/1000);
                $sevenDaysAgo = Carbon::now()->subDays(7);
                $thirtyDaysAgo = Carbon::now()->subDays(30);
                $oneDayAgo = Carbon::now()->subDay();

                if($datetime > $thirtyDaysAgo && $trade->signal->profit > 0.99){
                    $profitForTheMonth += number_format(1, 2);

                    if(!empty($profitForTheMonthByDay->{$datetime->format('Y-m-d')})) {
                        $profitForTheMonthByDay->{$datetime->format('Y-m-d')} += 1;
                    }else{
                        $profitForTheMonthByDay->{$datetime->format('Y-m-d')} = 1;
                    }

                }

                if($datetime > $oneDayAgo && $trade->signal->profit > 0.99){

                    $profitForTheDay += number_format(1, 2);
                    if(!empty($profitForTheDayByHour->{$datetime->format('Y-m-d')})) {
                        $profitForTheDayByHour->{$datetime->format('H')} += 1;
                    }else{
                        $profitForTheDayByHour->{$datetime->format('H')} = 1;
                    }

                }

                if($datetime > $sevenDaysAgo && $trade->trade_copied == true){

                    if(!empty($signalsCopiedByDay->{$datetime->format('Y-m-d')})){
                        $signalsCopiedByDay->{$datetime->format('Y-m-d')} += 1;
                        $signalsCopied++;
                        $totalSignals++;
                    }else{
                        $signalsCopiedByDay->{$datetime->format('Y-m-d')} = 1;
                        $signalsCopied++;
                        $totalSignals++;
                    }

                }elseif($datetime > $sevenDaysAgo && $trade->trade_copied == false){

                    if(!empty($signalsTakenByDay->{$datetime->format('Y-m-d')})){
                        $signalsTakenByDay->{$datetime->format('Y-m-d')} += 1;
                        $signalsTaken++;
                        $totalSignals++;
                    }else{
                        $signalsTakenByDay->{$datetime->format('Y-m-d')} = 1;
                        $signalsTaken++;
                        $totalSignals++;
                    }

                }

                $tradesFormatted->{$trade->signal->_id} = $trade;
            }

        }

        $key = ExchangeKey::where('exchange', strtolower($exchange))->where('user_id', Auth::id())->first();

        if($key){
            $hasKey = true;
        }else{
            $hasKey = false;
        }

        $payload = [
            'signals' => $jsonResponse->signals,
            'exchange' => $exchange,
            'hasKey' => $hasKey,
            'signalStats' => $signalStatsFormatted,
            'userTrades' => $tradesFormatted,
            'profitForTheDay' => $profitForTheDay,
            'profitForTheMonth' => $profitForTheMonth,
            'profitForTheMonthByDay' => $profitForTheMonthByDay,
            'profitForTheDayByHour' => $profitForTheDayByHour,
            'signalsTaken' => $signalsTaken,
            'signalsCopied' => $signalsCopied,
            'totalSignals' => $totalSignals,
            'signalsCopiedByDay' => $signalsCopiedByDay,
            'signalsTakenByDay' => $signalsTakenByDay,
            'isDemo' => $isDemo
        ];
        return view('signals.index', $payload);
    }

    public function executeSwipe(Request $request){
        $client = new \GuzzleHttp\Client();

        $pair = $request->pair;
        $exchange = $request->exchange;
        $signalId = $request->signalId;
        $percentageAmount = $request->percentageAmount;

        $exchangeKey = ExchangeKey::where('exchange', $exchange)->where('user_id', Auth::id())->first();

        $response = $client->request('POST', 'https://cryptotrader.trio.software/api/v1.0/'.$exchange.'/trade',[
            'form_params' => [
                'userApiKey' => Auth::user()->api_key,
                'exchangeApiKey' => $exchangeKey->api_key,
                'exchangeApiSecret' => $exchangeKey->api_secret,
                'pairName' => $pair,
                'amountToTrade' => $exchangeKey->risk_amount,
                'isDemo' => Auth::user()->trader_demo,
                'signalId' => $signalId,
                'percentageAmount' => $percentageAmount
            ]
        ]);

        $jsonResponse = json_decode($response->getBody());

        return response()->json($jsonResponse);
    }

    public function sellAtMarket(Request $request){
      $client = new \GuzzleHttp\Client();

      $exchange = $request->exchange;
      $sellOrderId = $request->sellOrderId;

      $exchangeKey = ExchangeKey::where('exchange', $exchange)->where('user_id', Auth::id())->first();

      // $response = $client->request('GET', 'https://cryptotrader.trio.software/api/v1.0/signals/'.ucwords($exchange).'/get');
      // $jsonResponse = json_decode($response->getBody());

      // return response()->json($jsonResponse);

      $response = $client->request('POST', 'https://cryptotrader.trio.software/api/v1.0/trades' .$exchange. '/sell',[
        'form_params' => [
          'exchangeApiKey' => $exchangeKey->api_key,
          'exchangeApiSecret' => $exchangeKey->api_secret,
          'userTradeId' => $sellOrderId,
        ]
      ]);

      $jsonResponse = json_decode($response->getBody());

      return response()->json($jsonResponse);
    }

    // Trader Stats
    public function stats($exchange){

        $key = ExchangeKey::where('exchange', strtolower($exchange))->first();

        if($key){
            $hasKey = true;
        }else{
            $hasKey = false;
        }

        $client = new \GuzzleHttp\Client();
        // http://localhost/api/v1.0/trades/
        // https://cryptotrader.trio.software/api/v1.0/trades/
        $responseTrades = $client->request('GET', 'https://cryptotrader.trio.software/api/v1.0/trades/'.ucwords($exchange).'/'.Auth::user()->api_key.'/get');
        $jsonResponseTrades = json_decode($responseTrades->getBody());
        // dd($jsonResponseTrades);

        $tradesFormatted = (object)[];

        $signalsTakenByDay = (object)[];
        $signalsCopiedByDay = (object)[];
        $profitForTheMonthByDay = (object)[];
        $profitForTheDayByHour = (object)[];

        $profitForTheMonth = 0;
        $profitForTheDay = 0;

        $signalsTaken = 0;
        $signalsCopied = 0;
        $totalSignals = 0;
        // dd($jsonResponseTrades);
        foreach($jsonResponseTrades as $trade){
            if(!empty($trade->sell_order_id)){
              if(!empty($trade->signal)){
                  $datetime = Carbon::createFromTimestamp($trade->timestamp/1000);
                  $sevenDaysAgo = Carbon::now()->subDays(7);
                  $thirtyDaysAgo = Carbon::now()->subDays(30);
                  $oneDayAgo = Carbon::now()->subDay();

                  if($datetime > $thirtyDaysAgo && $trade->signal->profit > 0.99){
                      $profitForTheMonth += number_format(1, 2);
                      if(!empty($profitForTheMonthByDay->{$datetime->format('Y-m-d')})) {
                          $profitForTheMonthByDay->{$datetime->format('Y-m-d')} += 1;
                      }else{
                          $profitForTheMonthByDay->{$datetime->format('Y-m-d')} = 1;
                      }

                  }

                  if($datetime > $oneDayAgo && $trade->signal->profit > 0.99){

                      $profitForTheDay += number_format(1, 2);
                      if(!empty($profitForTheDayByHour->{$datetime->format('Y-m-d')})) {
                          $profitForTheDayByHour->{$datetime->format('H')} += 1;
                      }else{
                          $profitForTheDayByHour->{$datetime->format('H')} = 1;
                      }

                  }

                  if($datetime > $sevenDaysAgo && $trade->trade_copied == true){

                      if(!empty($signalsCopiedByDay->{$datetime->format('Y-m-d')})){
                          $signalsCopiedByDay->{$datetime->format('Y-m-d')} += 1;
                          $signalsCopied++;
                          $totalSignals++;
                      }else{
                          $signalsCopiedByDay->{$datetime->format('Y-m-d')} = 1;
                          $signalsCopied++;
                          $totalSignals++;
                      }

                  }elseif($datetime > $sevenDaysAgo && $trade->trade_copied == false){

                      if(!empty($signalsTakenByDay->{$datetime->format('Y-m-d')})){
                          $signalsTakenByDay->{$datetime->format('Y-m-d')} += 1;
                          $signalsTaken++;
                          $totalSignals++;
                      }else{
                          $signalsTakenByDay->{$datetime->format('Y-m-d')} = 1;
                          $signalsTaken++;
                          $totalSignals++;
                      }

                  }
                  $tradesFormatted->{$trade->_id} = $trade;
              }
            }
        }
        $exchanges = Exchange::all();

        return view('signals.trades', [
            'trades' => $tradesFormatted,
            'hasKey' => $hasKey,
            'profitForTheDay' => $profitForTheDay,
            'profitForTheMonth' => $profitForTheMonth,
            'profitForTheMonthByDay' => $profitForTheMonthByDay,
            'profitForTheDayByHour' => $profitForTheDayByHour,
            'signalsTaken' => $signalsTaken,
            'signalsCopied' => $signalsCopied,
            'totalSignals' => $totalSignals,
            'signalsCopiedByDay' => $signalsCopiedByDay,
            'signalsTakenByDay' => $signalsTakenByDay,
            'exchanges' => $exchanges
        ]);
    }

    public function demoSwitch($on){
        $user = Auth::user();
        if($on == 'true'){
            $user->trader_demo = 1;
            $user->save();
            return response()->json('Success Demo Mode Turned ON');
        }else{
            $user->trader_demo = 0;
            $user->save();
            return response()->json('Success Demo Mode Turned OFF');
        }
    }

}
