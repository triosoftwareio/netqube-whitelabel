<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use App\ApiPayment;

class ApiController extends Controller
{
    public function recordPayment(Request $request){
      $rules = [
          'apiKey' => 'required',
          'apiSecret' => 'required',
          'username' => 'required',
          'length' => 'required',
          'amount' => 'required',
          'userId' => 'required'
      ];

      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
          return response()->json(['status' => 'Failed','data' => $validator->messages()]);
      }

      $apiKey = $request->apiKey;
      $apiSecret = $request->apiSecret;

      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
          $payment = ApiPayment::create([
            'apiKey' => $apiKey,
            'apiSecret' => $apiSecret,
            'username' => $request->username,
            'length' => $request->length,
            'amount' => $request->amount,
            'userId' => $request->userId
          ]);

          return response()->json(['status' => 'Success']);
      }else{
          return response()->json('Invalid Key');
      }
    }

    public function getInactiveUsers(Request $request){
      $apiKey = $request->header('apikey');
      $apiSecret = $request->header('apisecret');

        if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
          $users = User::where('status', 'Inactive')->get();
          return response()->json($users);
        }
        else return response()->json('Invalid Key');
    }

    public function getAllUsers(Request $request){
      $apiKey = $request->header('apikey');
      $apiSecret = $request->header('apisecret');

      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
        $users = User::all();
        return response()->json($users);
      }
      else return response()->json('Invalid Key');
    }

    public function getActiveUsers(Request $request){
      $apiKey = $request->header('apikey');
      $apiSecret = $request->header('apisecret');

      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
        $users = User::where('status', 'Active')->get();
        return response()->json($users);
      }
      else return response()->json('Invalid Key');
    }

    public function getUserById(Request $request){
      $apiKey = $request->header('apikey');
      $apiSecret = $request->header('apisecret');
      $userId = $request->header('userid');

      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
        $user = User::where('id', $userId)->first();
        return response()->json($user);
      }
      else return response()->json('Invalid Key');
    }

    public function getUserByUsername(Request $request){
      $apiKey = $request->header('apikey');
      $apiSecret = $request->header('apisecret');
      $username = $request->header('username');

      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
        $user = User::where('username', $username)->first();
        return response()->json($user);
      }
      else return response()->json('Invalid Key');
    }

    public function addNewUser(Request $request){
        $rules = [
            'apiKey' => 'required',
            'apiSecret' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|unique:users,email',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'sponsor' => 'required',
            'phoneNum' => 'nullable',
            'expiryDate' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'Failed','data' => $validator->messages()]);
        }

        $apiKey = $request->apiKey;
        $apiSecret = $request->apiSecret;

        $name = $request->firstName . ' ' . $request->lastName;
        if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
            $user = User::create([
                'first_name' => $request->firstName,
                'last_name' => $request->lastName,
                'name' => $name,
                'email' => $request->email,
                'username' => $request->username,
                'sponsor_name' => $request->sponsor,
                'phone_number' => $request->phoneNum,
                'status' => 'Active',
                'password' => bcrypt($request->password),
                'plan' => 1,
                'fromApi' => 1,
                'expiry_date' => $request->expiryDate
            ]);
            $user->api_key = uniqid($user->id, true);
            $user->save();

            return response()->json(['status' => 'Success', 'userId' => $user->id]);
        }else{
            return response()->json('Invalid Key');
        }
    }

    public function editUser(Request $request){
      $rules = [
        'apiKey' => 'required',
        'apiSecret' => 'required',
        'username' => 'required',
        'status' => 'required',
        'expiryDate' => 'required',
        'email' => 'required',
        'userId' => 'required'
      ];
      // change to use primary key

      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
          return response()->json(['status' => 'Failed','data' => $validator->messages()]);
      }

      $apiKey = $request->apiKey;
      $apiSecret = $request->apiSecret;
      if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
          $userId = $request->userId;
          $user = User::where('id', $userId)->first();
          if($user){
              $user->status = $request->username;
              $user->status = $request->email;
              $user->status = $request->status;
              $user->expiry_date = $request->expiryDate;
              $user->save();
              $string = 'Success. UserId ' . $userId . ' ' . $user->name . ' has been successfully updated';
              return response()->json($string);
          }else{
              return response()->json('User Not Found');
          }
      }else{
          return response()->json('Invalid Key');
      }
    }

    public function resetPassword(Request $request){
        $apiKey = $request->apiKey;
        $apiSecret = $request->apiSecret;
        if($apiKey == 'EfaXs6xDuAes3Cr6uxPHujEA' && $apiSecret == 'UQ$!w*K+unPm^x2aLQ@SLA%a'){
            $userId = $request->username;
            $passwordPlainText = $request->passwordPlainText;
            $user = User::where('username', $userId)->first();
            if($user){
                $user->password = bcrypt($passwordPlainText);
                $user->save();
                return response()->json('Success password changed');
            }else{
                return response()->json('User Not Found');
            }
        }else{
            return response()->json('Invalid Key');
        }
    }
}
