<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use Auth;
use Hash;
use App\Plan;
use App\Wallet;
use App\Upgrade;
use Carbon\Carbon;
use Twilio\Rest\Client as Client;
use Image;
use App\Binary;
use DB;
use App\Summary;
use App\Update;
use App\Webinar;
use App\UploadedWebinar;
use App\Withdrawl;
use App\UserCompletedVideos;
use App\Video;
use App\Course;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    public function settings(){
      $user = User::where('id', Auth::id())->first();
      $referrals = User::where('sponsor_name', $user->username)->get();
      $referralCount = count($referrals);
      $referralsActive = count(User::where('sponsor_name', $user->username)->where('status', 'Active')->get());
      $coursesList = Course::with('video')->get();
      $videoList = Video::all();

      if($user->status == 'Admin'){
        $commissionsShown = Withdrawl::all();
        $commissions = Withdrawl::all();
      } else {
        $commissionsShown = Withdrawl::where('user_id', $user->id)->orderBy('id','desc')->take(14);
        $commissions = Withdrawl::where('user_id', $user->id)->get();
      }
      $totalPaid = 0;
      foreach($commissions as $commission){
        $totalPaid += $commission->amount;
      }
      $totalPaid = round($totalPaid, 3);

      $completedVideoCount = UserCompletedVideos::where('userId', $user->id)->count();
      $completedVideos = UserCompletedVideos::where('userId', $user->id)->get()
        ->groupBy(function($date){
          return Carbon::parse($date->created_at)->format('d');
        });
        // dd($completedVideos);

      return view('settings.settings', [
        'user' => $user,
        'referralCount' => $referralCount,
        'referralsActive' => $referralsActive,
        'referralUsers' => $referrals,
        'totalPaid' => $totalPaid,
        'completedVideos' => $completedVideos,
        'coursesList' => $coursesList,
        'videoList' => $videoList,
        'completedVideoCount' => $completedVideoCount
      ]);
    }

    public function changeSignals(Request $request){
      $user = User::where('id', Auth::id())->first();
      $user->default_exchange = $request->exchange;
      $user->save();
      return redirect('home');
    }

    public function updatePassword(Request $request){
           $current_password = Auth::User()->password;
           if(Hash::check($request->oldPassword, $current_password) && $request->newPassword == $request->newPassConfirm)
           {
             $user_id = Auth::ID();
             $obj_user = User::find($user_id);
             $obj_user->password = Hash::make($request->newPassword);;
             $obj_user->save();
             return view('error.display', ['status' => 'Success', 'message' => 'Success, your password has been successfully updated.']);
           }
           else
           {
             return view('error.display', ['status' => 'Error','message' => 'Please enter correct current password & make sure your new password matches confirmation field']);
           }
    }

    public function updateProfile(Request $request){
      $user = User::where('id', Auth::id())->first();
      // $requestedUsername = $request->username;
      $requestedEmail = $request->email;
      $phoneNum = $request->phone;

      // $checkUser = User::where('username', $requestedUsername)->first();
      $checkUser2 = User::where('email', $requestedEmail)->first();
      // if($checkUser != NULL && ($checkUser->id != $user->id)){
      //   return view('error.display', ['status' => 'Error', 'message' => 'Username is already in use']);
      // }
      if($checkUser2 != NULL && ($checkUser2->id != $user->id)){
        return view('error.display', ['status' => 'Error', 'message' => 'Email is already in use']);
      }
      else{
        // $user->username = $requestedUsername;
        $user->email = $requestedEmail;
        $user->phone_number = $phoneNum;
        $user->name = $request->name;
        $user->save();
        return view('error.display', ['status' => 'Success', 'message' => 'Successfully updated profile information']);
      }
    }

    public function updateAvatar(Request $request){
      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(230,230)->save(public_path('/uploads/avatars/' . $filename));

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
      return redirect('/settings');
    }

    public function transactions(){
      $transactions = Transaction::where('user_id', Auth::id())->get();
      return view('transactions.transactions', ['transactions' => $transactions]);
    }

    public function getInfo(Request $request){
      $userInfo = User::where('username', $request->username)->first();
      if($userInfo){
        return response()->json(['status' => 'Success', 'name' => $userInfo->name]);
      }else{
        return response()->json(['status' => 'Fail']);
      }
    }

    public function archive(){
      $summaries = Summary::latest();
      $updates = Update::latest();

      if($month = request('month')){
        $summaries->whereMonth('created_at', Carbon::parse($month)->month);
        $updates->whereMonth('created_at', Carbon::parse($month)->month);
      }
      if($year = request('year')){
        $summaries->whereYear('created_at', $year);
        $updates->whereYear('created_at', $year);
      }

      $summaries = $summaries->get();
      $updates = $updates->get();

      return view('archive.index', [
        'summaries' => $summaries,
        'updates' => $updates
      ]);
    }

    public function referrals(){
      $user = User::where('id', Auth::id())->first();
      $referralUsers = User::where('sponsor_name', $user->username)->get();

      return view('referrals.index', [
        'user' => $user,
        'referralUsers' => $referralUsers
      ]);
    }

    public function commissionsPage(){
      $thisUser = User::where('id', Auth::id())->first();
      // $thisUser = User::where('id', 10)->first();
      $today = Carbon::today();
      $lastRenewal = new Carbon($thisUser->last_renewal);
      // if($today->toDateString() == $lastRenewal->toDateString()) dd($today);
      // else dd($now);

      $allUsers = User::all();

      //Query with parent and children
      $testQuery = DB::select("SELECT p.id,p.last_renewal as last_renewal,p.btcPrice as btc_price, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, p.email, pl.name, pl.volume_amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

      $parents = [];

      //Creating array of parents with children from query data
      foreach($testQuery as $parent){
          $parents[$parent->id]['id'] = $parent->id;
          $parents[$parent->id]['name'] = $parent->parent_name;
          $parents[$parent->id]['username'] = $parent->username;
          $parents[$parent->id]['status'] = $parent->status;
          $parents[$parent->id]['plan_name'] = $parent->name;
          $parents[$parent->id]['last_renewal'] = $parent->last_renewal;
          $parents[$parent->id]['btc_price'] = $parent->btc_price;
          $parents[$parent->id]['volume_amount'] = $parent->volume_amount;
          $parents[$parent->id]['left_child'] = $parent->left_child;
          $parents[$parent->id]['right_child'] = $parent->right_child;
          $parents[$parent->id]['email'] = $parent->email;
      }

      $qualified = false;
      $qualified2 = false;
      $match1 = false;
      $match2 = false;
      $match3 = false;

      $myDownline = $this->getBinaryDownline($parents, Auth::id(), Auth::user()->last_paid_out_left, Auth::user()->last_paid_out_right, true);

      //Creating array to store users that are to be paid
      $usersToPay = [];

      //getting active users with 2 levels of children for match bonus for calculating commissions
      $usersQualified = User::with('personal.child.personal.child')->where('status', 'Active')->get();

      foreach($usersQualified as $user){
        // if($user->id != $thisUser->id) continue;

          //Traversing up the tree to check users owed amount
          $downline = $this->getBinaryDownline($parents, $user->id, $user->last_paid_out_left, $user->last_paid_out_right, true);

          //Determining lesser leg
          if(($downline['left_volume'] > $downline['right_volume']) && $downline['btc_right'] > 0.00000){

              $usersToPay[$user->email] = [
                  'team_commission' => $downline['btc_right'],
              ];
              $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_right';
              $usersToPay[$user->email]['usersPaidOn'] = $downline['right_volume']/100;

          }elseif(($downline['right_volume'] > $downline['left_volume']) && $downline['btc_left'] > 0.00000){

              $usersToPay[$user->email] = [
                  'team_commission' => $downline['btc_left'],
              ];
              $usersToPay[$user->email]['usersPaidOn'] = $downline['left_volume']/100;
              $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_left';

          }elseif(($downline['right_volume'] === $downline['left_volume']) && ($downline['btc_left'] > 0.00000 || $downline['btc_right'] > 0.00000)){

              if($downline['btc_left'] > $downline['btc_right']){
                  $usersToPay[$user->email] = [
                      'team_commission' => $downline['btc_right'],
                  ];
                  $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_right';
                  $usersToPay[$user->email]['usersPaidOn'] = $downline['left_volume']/100;
              }else{
                  $usersToPay[$user->email] = [
                      'team_commission' => $downline['btc_left'],
                  ];
                  $usersToPay[$user->email]['payoutSide'] = 'last_paid_out_left';
                  $usersToPay[$user->email]['usersPaidOn'] = $downline['right_volume']/100;
              }

          }else{

              $usersToPay[$user->email] = [
                  'team_commission' => 0.00000,
              ];
              $usersToPay[$user->email]['usersPaidOn'] = 0;
              $usersToPay[$user->email]['payoutSide'] = null;

          }

          $dirUsers = Binary::where('parent_id', $user->id)->get();
          if(!array_key_exists('direct_commission', $usersToPay[$user->email])){
              $usersToPay[$user->email]['direct_commission'] = 0.000000;
          }

          foreach ($dirUsers as $du){
              $directUser = User::where('id', $du->child_id)->first();
              if($user->last_paid_out_direct < $directUser->last_renewal){
                  if($directUser->status == 'Active' && $directUser->plan == 1){
                      if( array_key_exists ( $user->email , $usersToPay )){
                          array_key_exists ( 'direct_commission' , $usersToPay[$user->email] ) ? $usersToPay[$user->email]['direct_commission'] += (100 / $directUser->btcPrice) : $usersToPay[$user->email]['direct_commission'] = (100 / $directUser->btcPrice) ;
                      }else{
                          $usersToPay[$user->email] = ['direct_commission' => (100 / $directUser->btcPrice)];
                      }
                  }
              }
          }

          $usersToPay[$user->email]['direct_commission'] = round($usersToPay[$user->email]['direct_commission'],5);

          //Adding user plan for future qualification checks & adding arrays to store user match
          $usersToPay[$user->email]['plan'] = $parents[$user->id]['plan_name'];
          $leftMatch = [];
          $rightMatch = [];
          $leftActive = [];
          $rightActive = [];


          //Gathering data to check if user qualifies for match bonus
          if($user->id == Auth::id()){
            foreach($user->personal as $personal){
                if($personal->side == 'Left' && $personal->child->status == 'Active' && $personal->child->plan == 1){
                    $childLeft = 0;
                    $childRight = 0;
                    foreach($personal->child->personal as $childsChildren){
                        if($childsChildren->side == 'Left' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childLeft++;
                        }elseif($childsChildren->side == 'Right' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childRight++;
                        }
                    }
                    if($childLeft > 0 && $childRight > 0){
                        array_push($leftMatch, $personal->child->email);
                    }
                    array_push($leftActive, $personal->child->email);
                }else if($personal->side == 'Right' && $personal->child->status == 'Active' && $personal->child->plan == 1){
                    $childLeft = 0;
                    $childRight = 0;
                    foreach($personal->child->personal as $childsChildren){
                        if($childsChildren->side == 'Left' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childLeft++;
                        }elseif($childsChildren->side == 'Right' && $childsChildren->child->status == 'Active' && $childsChildren->child->plan == 1){
                            $childRight++;
                        }
                    }
                    if($childLeft > 0 && $childRight > 0){
                        array_push($rightMatch, $personal->child->email);
                    }
                    array_push($rightActive, $personal->child->email);
                }
            }
          }
          //Adding match bonus and other data needed for payouts
          $usersToPay[$user->email]['user_id'] = $user->id;
          $usersToPay[$user->email]['btc_address'] = $user->wallet_address;
          $usersToPay[$user->email]['left_match'] = $leftMatch;
          $usersToPay[$user->email]['right_match'] = $rightMatch;
          $usersToPay[$user->email]['left_active'] = $leftActive;
          $usersToPay[$user->email]['right_active'] = $rightActive;
      }

      // dd($usersToPay);
      // dd($rightMatch);
      $leftVolume = 0;
      $rightVolume = 0;
      if($thisUser->status !== 'Admin'){
        foreach($usersToPay[$thisUser->email]['left_active'] as $directSale){
          $leftUser = User::where('email', $directSale)->first();
          if($leftUser->status == 'Active' && $leftUser->plan != 8) $leftVolume++;
        }
        foreach($usersToPay[$thisUser->email]['right_active'] as $directSale){
          $rightUser = User::where('email', $directSale)->first();
          if($rightUser->status == 'Active' && $rightUser->plan != 8) $rightVolume++;
        }
      }


      if($leftVolume > 2 && ($rightVolume > 2)){
        $match3 = true;
        $match2 = true;
        $match1 = true;
      }
      elseif($leftVolume > 1 && ($rightVolume > 1)){
        $match2 = true;
        $match1 = true;
      }
      elseif($leftVolume > 0 && ($rightVolume > 0)) $match1 = true;

      // Calculating users match bonus
      foreach($usersToPay as $key => $user){
        // dd($key);
        if($user['user_id'] != Auth::id()) continue;
        $usersToPay[$key]['match_bonus'] = 0.000000;
        if(count($user['left_match']) > 2 && count($user['right_match'])){
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][1]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][2]]['team_commission']*0.10000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][1]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][2]]['team_commission']*0.10000;
        }
        elseif(count($user['left_match']) > 1 && count($user['right_match'])){
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][1]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][1]]['team_commission']*0.20000;
        }
        elseif(count($user['left_match']) > 0 && count($user['right_match'])){
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['left_match'][0]]['team_commission']*0.20000;
          $usersToPay[$key]['match_bonus'] += $usersToPay[$user['right_match'][0]]['team_commission']*0.20000;
        }
        else{}
      }

      foreach($usersToPay as $key => $affiliate){
        if(!array_key_exists('direct_commission', $affiliate)){
            $affiliate['direct_commission'] = 0.000000;
        }

        $amountBeingPaidOutDirectCommission = $affiliate['direct_commission'];
      }

      // User qualification levels
      if($thisUser->status == 'Active') $qualified = true;
      if(count($leftActive) > 4 && count($rightActive) > 4 && $thisUser->status == 'Active') $qualified2 = true;


      if($thisUser->status == 'Admin'){
        $commissionsShown = Withdrawl::orderBy('id','DESC')->get();
        $commissions = Withdrawl::orderBy('id','DESC')->get();
        $commissionedUserIds = Withdrawl::select('user_id')->get();
        $commissionedUsers = [];
        foreach($commissionedUserIds as $cuid){
          $comUser = User::where('id', $cuid->user_id)->first();
          $commissionedUsers[] = $comUser;
        }
      } else {
        $commissionsShown = Withdrawl::where('user_id', $thisUser->id)->orderBy('id','desc')->take(14);
        $commissions = Withdrawl::where('user_id', $thisUser->id)->get();
      }
      $totalPaid = 0;
      $teamCommissions = 0;
      $directCommissions = 0;
      $matchCommissions = 0;
      $dirUsers = Binary::where('parent_id', Auth::id())->get();
      foreach($commissions as $commission){
        $totalPaid += $commission->amount + $commission->bonus_amount;
        $teamCommissions += $commission->amount - $commission->direct_amount;
        $matchCommissions += $commission->bonus_amount;
        $directCommissions += $commission->direct_amount;
      }

      if($thisUser->status == 'Admin'){
        return view('commissions.admin', [
          'user' => $thisUser,
          'commissions' => $commissionsShown,
          'totalPaid' => $totalPaid,
          'commissionedUsers' => $commissionedUsers
        ]);
      } else {
        $totalPaid = round($totalPaid,5);
        $teamCommissions = round($teamCommissions,5);
        $matchCommissions = round($matchCommissions,5);
        $directCommissions = round($directCommissions,5);
        return view('commissions.index', [
          'user' => $thisUser,
          'commissions' => $commissionsShown,
          'totalPaid' => $totalPaid,
          'teamCommissions' => $teamCommissions,
          'matchCommissions' => $matchCommissions,
          'directCommissions' => $directCommissions,
          'match1' => $match1,
          'match2' => $match2,
          'match3' => $match3,
          'qualified' => $qualified,
          'qualified2' => $qualified2,
          'usersToPay' => $usersToPay,
          'downline' => $myDownline,
          'users' => $allUsers,
          'directUsers' => $dirUsers,
          'directToday' => $amountBeingPaidOutDirectCommission
        ]);
      }
    }

    public function webinar(){
      $prevWebinars = UploadedWebinar::orderBy('id', 'desc')->take(10)->get();
      $webinars = Webinar::orderBy('id', 'desc')->take(10)->get();

        return view('webinar.index', [
          'webinars' => $webinars,
          'prevWebinars' => $prevWebinars
        ]);
    }

    public function getBinaryDownline($data, $startId, $lastPaidOutLeft, $lastPaidOutRight, $firstUser){

        if($data[$startId]['status'] === 'Active' && $data[$startId]['plan_name'] !== 'Sales_Rep'){
            $currentVolume = $data[$startId]['volume_amount'];
            $bitcoinAmountToPayout = round((100/$data[$startId]['btc_price']), 5);
        }else{
            $currentVolume = 0;
            $bitcoinAmountToPayout = round(0.00000, 5);
        }

        if(($firstUser == false && $lastPaidOutLeft !== null && $lastPaidOutLeft > $data[$startId]['last_renewal']) || ($firstUser == false && $lastPaidOutRight !== null && $lastPaidOutRight > $data[$startId]['last_renewal']) ){
            $currentVolume = 0;
            $bitcoinAmountToPayout = round(0.00000, 5);
        }

        $directDownline = array(
            'id' => $startId,
            'name' => $data[$startId]['name'],
            'username' => $data[$startId]['username'],
            'status' => $data[$startId]['status'],
            'plan_name' => $data[$startId]['plan_name'],
            'volume_amount' => $currentVolume,
            'btc_amount' => $bitcoinAmountToPayout,
            'last_renewal' => $data[$startId]['last_renewal'],
            'email' => $data[$startId]['email'],
            'left_volume' => 0,
            'right_volume' => 0,
            'btc_left' => 0,
            'btc_right' => 0,
            'left_child' => [],
            'right_child' => []
        );

        if(array_key_exists($data[$startId]['left_child'], $data) && $data[$data[$startId]['left_child']] !== NULL){
            $left_child = $this->getBinaryDownline($data, $data[$startId]['left_child'], $lastPaidOutLeft, $lastPaidOutLeft, false);
            $directDownline['left_child'] = $left_child;
            $directDownline['left_volume'] = $directDownline['left_volume']+$left_child['left_volume']+$left_child['right_volume']+$left_child['volume_amount'];
            $directDownline['btc_left']  = $directDownline['btc_left']+$left_child['btc_left']+$left_child['btc_right']+$left_child['btc_amount'];
        }
        if(array_key_exists($data[$startId]['right_child'], $data) && $data[$data[$startId]['right_child']] !== NULL){
            $right_child = $this->getBinaryDownline($data, $data[$startId]['right_child'], $lastPaidOutRight, $lastPaidOutRight, false);
            $directDownline['right_child'] = $right_child;
            $directDownline['right_volume'] = $directDownline['right_volume']+$right_child['right_volume']+$right_child['left_volume']+$right_child['volume_amount'];
            $directDownline['btc_right']  = $directDownline['btc_right']+$right_child['btc_right']+$right_child['btc_left']+$right_child['btc_amount'];
        }

        return $directDownline;
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function test(){

      $sponsorUsers = User::where('id', '>' , 6699)->get();
      foreach($sponsorUsers as $parentUser){
          for($i = 0; $i < 10; $i++){

              $testQuery = DB::select("SELECT p.id, p.name as parent_name, l.left_child AS left_child, r.right_child AS right_child, p.status, p.username, pl.name, pl.amount FROM users p LEFT OUTER JOIN(SELECT l.placement_id AS parent, GROUP_CONCAT(l.child_id) AS left_child FROM `binarytree` l WHERE l.side = 'Left' GROUP BY l.placement_id) l ON l.parent = p.id LEFT OUTER JOIN(SELECT r.placement_id AS parent, GROUP_CONCAT(r.child_id) AS right_child FROM `binarytree` r WHERE r.side = 'Right' GROUP BY r.placement_id) r ON r.parent = p.id LEFT OUTER JOIN plans pl ON pl.id = p.plan GROUP BY p.id");

              $parents = [];

              foreach($testQuery as $parent){
                  $parents[$parent->id]['id'] = $parent->id;
                  $parents[$parent->id]['name'] = $parent->parent_name;
                  $parents[$parent->id]['username'] = $parent->username;
                  $parents[$parent->id]['status'] = $parent->status;
                  $parents[$parent->id]['plan_name'] = $parent->name;
                  $parents[$parent->id]['amount'] = $parent->amount;
                  $parents[$parent->id]['left_child'] = $parent->left_child;
                  $parents[$parent->id]['right_child'] = $parent->right_child;
              }

              $value = rand(0,1);
              $sponsorSide = $parentUser->default_position;
              $name = $this->generateRandomString(10). $i;
              $email = $this->generateRandomString(10) . $i . '@fake.com';
              if($value == 0){
                  $user = User::create([
                      'name'=>$name,
                      'username'=>$name,
                      'email'=>$email,
                      'plan'=>1,
                      'btcPrice'=>rand(7500,8500),
                      'ltcPrice'=>rand(100,150),
                      'status'=>'Inactive'
                  ]);
              } else {
                  $user = User::create([
                      'name'=>$name,
                      'username'=>$name,
                      'email'=>$email,
                      'plan'=>1,
                      'btcPrice'=>rand(7500,8500),
                      'ltcPrice'=>rand(100,150),
                      'status'=>'Active'
                  ]);
              }
              $downline = $this->getBinaryDownline($parents, $parentUser->id, $sponsorSide);
              if($sponsorSide == 'Left'){
                  if(empty($downline['left_child'])){
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Left';
                      $sponsor->placement_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }elseif(!empty($downline['left_child']['placement_id'])){
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Left';
                      $sponsor->placement_id = $downline['left_child']['placement_id'];
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }else{
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Left';
                      $sponsor->placement_id = NULL;
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }
              }else{
                  if(empty($downline['right_child'])){
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Right';
                      $sponsor->placement_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }elseif(!empty($downline['right_child']['placement_id'])){
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Right';
                      $sponsor->placement_id = $downline['right_child']['placement_id'];
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }else{
                      $sponsor = new Binary;
                      $sponsor->parent_id = User::where('username', $parentUser->username)->first()->id;
                      $sponsor->side = 'Right';
                      $sponsor->placement_id = NULL;
                      $sponsor->child_id = $user->id;
                      $sponsor->save();
                  }
              }
          } // end for
      }
    }

    public function upgrade(){
      $currentPackage = User::with('binaryPlan')->where('id', Auth::id())->first()->binaryPlan->amount;
      $plans = Plan::where('amount', '>', $currentPackage)->get();
      $upgrade = Upgrade::where('user_id', Auth::id())->where('status', 'Pending')->first();
      if($upgrade){
        $pending = true;
        $wallet = Wallet::where('id', $upgrade->wallet_id)->first();

        //init wallet api
        $apiKey = "2f8f-2f0b-ec72-74b6";
        $version = 2; // API version
        $pin = "Scott1994Amir1993";
        // dd(phpinfo());

        $client = new \GuzzleHttp\Client();
        $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
        $jsonBtcResponse = json_decode($btcResponse->getBody());
        $currentUser = User::where('id', Auth::id())->first();

        $now = Carbon::now();
        $lastTime = new Carbon($currentUser->last_price_time);
        $timePassed = $now->diffInSeconds($lastTime);

        if($timePassed >= 1800 || $lastTime == $now){
          $currentBtcPrice = $jsonBtcResponse[0]->price_usd;
          $currentUser->last_price_time = $now;
        } else {
          //price stays the same
          $currentBtcPrice = $currentUser->btcPrice;
        }
        //save price
        $currentUser->btcPrice = $currentBtcPrice;

        $costBtc = round(($upgrade->amount / $currentBtcPrice),7);
        $upgrade->costBtc = $costBtc;

        $block_io = new \BlockIo($apiKey, $pin, $version);
        $blockBalance = $block_io->get_address_balance(array('addresses' => $wallet->address));
        $pendingBalance = floatval($blockBalance->data->balances[0]->pending_received_balance);
        $receivedBalance = floatval($blockBalance->data->balances[0]->available_balance);
        $totalBalance = $pendingBalance + $receivedBalance;

        $package = Plan::where('amount', $upgrade->amount + $currentPackage)->first();

        $thisLink = strstr(Auth::user()->payment_address, "bitcoin:", true);
        $thisAddress = strstr(substr(Auth::user()->payment_address, strpos(Auth::user()->payment_address, "bitcoin:") + 8), '?amount=', true);
        $newAddress = $thisLink . "bitcoin:" . $thisAddress . "?amount=" . $costBtc;
        $currentUser->payment_address = $newAddress;


        if($pendingBalance >= $costBtc || $receivedBalance >= $costBtc || $totalBalance >= $costBtc){
          $currentUser->status = 'Active';
          $currentUser->plan = $package->id;
          $upgrade->status = 'Complete';
          $currentUser->last_renewal = $now;
          $currentUser->save();
          $upgrade->save();
          return view('error.display', ['status' => 'Success', 'message' => 'You have succesfully upgraded.']);
        }
        $currentUser->save();
        $upgrade->save();
        return view('upgrade.display', [
          'plans' => $plans,
          'currentPackage' => $currentPackage,
          'amount' => $upgrade->amount,
          'pending' => $pending,
          'currentBtcPrice' => $currentBtcPrice,
          'costBtc' => $costBtc
        ]);
      }else{
        $pending = false;
        return view('upgrade.upgrade', ['plans' => $plans, 'currentPackage' => $currentPackage, 'amount' => NULL, 'pending' => $pending]);
      }
    }

    public function createPaymentCode(Request $request){
      $currentPackage = User::with('binaryPlan')->where('id', Auth::id())->where('status', 'Active')->first();
      if($currentPackage){
        $plans = Plan::where('amount', '>', $currentPackage->binaryPlan->amount)->get();
        $newPlan = $plans->where('id', $request->plan)->first();
        $upgrade = Upgrade::where('user_id', Auth::id())->where('status', 'Pending')->first();

        $client = new \GuzzleHttp\Client();
        $btcResponse = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/bitcoin/');
        $jsonBtcResponse = json_decode($btcResponse->getBody());
        $currentUser = User::where('id', Auth::id())->first();

        $now = Carbon::now();
        $lastTime = new Carbon($currentUser->last_price_time);
        $timePassed = $now->diffInSeconds($lastTime);

        if($timePassed >= 1800){
          $currentBtcPrice = $jsonBtcResponse[0]->price_usd;
          $currentUser->last_price_time = $now;
        } else {
          //price stays the same
          $currentBtcPrice = $currentUser->btcPrice;
        }
        //save price
        $currentUser->btcPrice = $currentBtcPrice;
        $currentUser->save();

        if($upgrade){
          $pending = true;
        }else{
          $pending = false;
        }

        if($newPlan && $pending == false){
          $newPlanAmount = $newPlan->amount;
          $oldAmount = $currentPackage->binaryPlan->amount;
          $amountDue = $newPlanAmount - $oldAmount;
          $costBtc = round(($amountDue / $currentBtcPrice),8);
          //init wallet api
          $apiKey = "2f8f-2f0b-ec72-74b6";
          $version = 2; // API version
          $pin = "Scott1994Amir1993";
          // dd(phpinfo());
          $block_io = new \BlockIo($apiKey, $pin, $version);
          //create wallet address
          $rndString = substr(md5(uniqid(mt_rand(), true)), 0, 8);
          $newAddressInfo = $block_io->get_new_address(array('label' => Auth::user()->email.'.'.$rndString));

          if($newAddressInfo->status == "success"){
            $address = $newAddressInfo->data->address;

            $dbWallet = new Wallet;
            $dbWallet->address = $address;
            $dbWallet->label = Auth::user()->email.date('Y-m-d H:i:s');
            $dbWallet->save();

            $currentPackage->payment_address = 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:'.$address.'?amount='.$costBtc;
            $currentPackage->save();

            $payment = new Upgrade;
            $payment->wallet_id = $dbWallet->id;
            $payment->user_id = Auth::id();
            $payment->amount = $amountDue;
            $payment->status = 'Pending';
            $payment->costBtc = $costBtc;
            $payment->save();

            return redirect('/upgrade');
          }else{
            return view('error.display', ['status' => 'Error', 'message' => 'Sorry, We could not generate payment token.']);
          }
        }else{
          return view('error.display', ['status' => 'Error', 'message' => 'This package does not exist or you currently have a pending upgrade, this issue has been reported to the system administrator.']);
        }
      }else{
        return view('error.display', ['status' => 'Error', 'message' => 'Please pay for your current package first']);
      }
    }

    public function updateUserBranch(Request $request){
      $user_id = Auth::ID();
      $user = User::find($user_id);
      $user->default_position = $request->direction;
      $user->save();
      return redirect('/binary');
    }

}
