<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckCoursePlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->plan == 8 && Auth::user()->status != 'Admin' && Auth::user()->status != 'Free' && Auth::user()->status != 'Active' && Auth::user()->payment_address == NULL) {
            return redirect('/imported/user');
        }

        return $next($request);
    }
}
