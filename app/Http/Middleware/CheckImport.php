<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckImport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->status != 'Admin' && Auth::user()->status != 'Active' && Auth::user()->status != 'Free' && Auth::user()->payment_address == NULL && Auth::user()->imported == 1) return redirect ('/imported/user');
        // else if (Auth::user()->status != 'Admin' && Auth::user()->status != 'Active' && Auth::user()->status != 'Free') return redirect('/home');
        return $next($request);
    }
}
