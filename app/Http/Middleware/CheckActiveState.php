<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\AuthNetSubscription;

class CheckActiveState
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->status != 'Active' && Auth::user()->status != 'Admin' && Auth::user()->status != 'Free') {
            return redirect('/home');
        }

        return $next($request);
    }
}
