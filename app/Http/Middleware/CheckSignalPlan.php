<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSignalPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ((Auth::user()->plan == 1 || Auth::user()->plan == 2 || Auth::user()->plan != 8) && Auth::user()->status != 'Admin') {
            return redirect('/home');
        }

        return $next($request);
    }
}
