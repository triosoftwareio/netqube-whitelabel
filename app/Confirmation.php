<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
  protected $fillable = [
    'user_id', 'confirmation_num', 'amount'
  ];
}
