<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
    protected $table = 'course_type';


    public function course(){
      return $this->hasMany('App\Course', 'type_id', 'id');
    }

}
