<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthNetSubscription extends Model
{
    protected $table = 'auth_net_subscription';
}
