<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('welcome');
});

Route::get('/user_agreement', 'WelcomeController@userAgreement');
Route::get('/privacy_policy', 'WelcomeController@privacyPolicy');
Route::get('/affiliate_agreement', 'WelcomeController@affiliateAgreement');
Route::get('/comp_plan', 'WelcomeController@compPlan');
Route::get('/enter/sponsor', 'WelcomeController@enterSponsor');
Route::get('/admin/commission/calculate', 'AdminController@calculateCommissions');
Route::get('/import/data', 'ImportController@importUsers');
Route::get('/import/cmd/data', 'ImportController@importCmdUsers');

Route::get('/imported/user', 'ImportController@index');
Route::get('/generate/imported/user/wallet/{package}/{price}', 'ImportController@generateWallet');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->middleware('auth');
// Route::get('/testing', 'BinaryController@showDownline');
// Route::get('/cron/daily', 'CronController@addDailyPercentage');
// Route::get('/test', 'HomeController@index')->name('newHome')->middleware('auth', 'twofactor');
// Route::get('/cron/daily/calculate/volume', 'CronController@calculateDailyVolume');

// Route::get('/mail', 'MailController@sendEmails');

// Assuming Auth
Route::group(['middleware' => ['auth']], function(){
  Route::post('/binary/updatedirection', 'UserController@updateUserBranch');
  Route::post('/user/placement', 'BinaryController@placeUser');
});

Route::group(['middleware' => ['auth', 'import']], function () {
  Route::get('/get/sponsor/info', 'UserController@getInfo');
  Route::get('/binary', 'BinaryController@index');
  Route::post('/upgrade/generate' , 'UserController@createPaymentCode');
  Route::get('/binary/get/children', 'BinaryController@getChildren');
  Route::post('/binary/search', 'BinaryController@searchByUsername');
  Route::get('/referrals', 'UserController@referrals');
  Route::post('/binary/upone', 'BinaryController@upOne');
  Route::get('/upgrade', 'UserController@upgrade');
  Route::get('/commissions', 'UserController@commissionsPage');
  Route::get('/home', 'HomeController2@index')->name('home');
});

Route::group(['middleware' => ['auth', 'admin']], function(){
  Route::get('/payment/form', 'MerchantController@displayForm');
  Route::post('/process/payment', 'MerchantController@processForm');
  Route::post('/authnet', 'MerchantController@authNet');
});

Route::group(['middleware' => ['auth', 'active']], function(){
// Route::group(['middleware' => ['auth', 'admin']], function(){
  Route::get('/webinars', 'UserController@webinar');
  Route::get('/settings', 'UserController@settings');
  Route::post('/settings/updatepassword', 'UserController@updatePassword');
  Route::post('/settings/updateprofilepic', 'UserController@updateAvatar');
  Route::post('/settings/updateuserprofile', 'UserController@updateProfile');
  Route::get('/referrals', 'UserController@referrals');
  Route::get('/wallet', 'WalletController@index');
  Route::post('/wallet/address/update', 'WalletController@updateWalletAddress');
  Route::get('/transaction', 'UserController@transactions');
  Route::get('/upgrade', 'UserController@upgrade');
  Route::get('/wallet', 'WalletController@index');
  Route::post('/wallet/address/update', 'WalletController@updateWalletAddress');
  Route::get('/transaction', 'UserController@transactions');
});

Route::group(['middleware' => ['auth', 'courses']], function () {
// Route::group(['middleware' => ['auth', 'admin']], function () {
  Route::get('/signals/{exchange}', 'SignalController@index');
  Route::post('/change/default/exchange', 'UserController@changeSignals');
  Route::get('/settings/api-keys', 'ApiKeyController@index');
  Route::post('/settings/api-keys/{exchange}/update', 'ApiKeyController@updateKeys');
  Route::post('/trade/execute', 'SignalController@executeSwipe');
  Route::post('/trade/sell-at-market', 'SignalController@sellAtMarket');
  Route::get('/stats/{exchange}', 'SignalController@stats');
  Route::get('/swipe-trade/demo/{on}', 'SignalController@demoSwitch');
  Route::get('/courses', 'CourseController@index');
  Route::get('/courses/{id}/{video}', 'CourseController@indexSubCourse');
  Route::get('/course/complete/{id}', 'CourseController@markComplete');
  Route::get('/archive', 'UserController@archive');
});

Route::group(['middleware' => ['auth', 'admin']], function(){
  Route::get('/admin', 'AdminController@index');
  Route::get('/admin/users/list', 'AdminController@listUsers');
  Route::get('/admin/user/edit/{id}', 'AdminController@editUser');
  Route::post('/admin/user/edit/info/{id}', 'AdminController@editUserInfo');
  Route::get('/generate/dummy/accounts/test', 'UserController@test');
  Route::get('/admin/create/mail', 'AdminController@mailForm');
  Route::post('/admin/send/new/summary', 'AdminController@sendSummary');
  Route::get('/admin/upload/file', 'AdminController@uploadFileForm');
  Route::post('/admin/upload/summary', 'AdminController@uploadSummary');
  Route::post('/admin/upload/update', 'AdminController@uploadUpdate');
  Route::post('/admin/send/new/summary', 'AdminController@sendSummary');
  Route::post('/admin/send/new/update', 'AdminController@sendUpdate');
  Route::post('/admin/add/webinar', 'AdminWebinarController@addWebinar');
  Route::post('/admin/upload/webinar', 'AdminWebinarController@uploadWebinar');
  Route::get('/admin/webinar/remove/{id}', 'AdminWebinarController@removeWebinar');
  Route::get('/admin/prevwebinar/remove/{id}', 'AdminWebinarController@removePrevWebinar');
  Route::get('/admin/webinar/edit/{id}', 'AdminWebinarController@editWebinar');
  Route::get('/admin/prevwebinar/edit/{id}', 'AdminWebinarController@editPreviousWebinar');
  Route::post('/admin/webinar/update/{id}', 'AdminWebinarController@updateWebinarInfo');
  Route::post('/admin/prevwebinar/update/{id}', 'AdminWebinarController@updatePrevWebinarInfo');
  Route::get('/admin/stats', 'AdminController@stats');
  Route::get('/admin/email/users', 'AdminController@emailUsersForm');
  Route::post('/admin/send/all/users', 'AdminController@mailAllUsers');
  Route::post('/admin/send/active/users', 'AdminController@mailActiveUsers');
  Route::get('/admin/get/authnet/subscriptions', 'MerchantController@getSubscriptions');
});

// Test routes to see backend without auth -- remember to uncomment auth lines in newApp layout

Route::get('/phpinfo', function(){
  dd(phpinfo());
});
