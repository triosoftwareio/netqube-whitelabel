<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/users/inactive/get', 'ApiController@getInactiveUsers');
Route::get('/users/active/get', 'ApiController@getActiveUsers');
Route::get('/users/all/get', 'ApiController@getAllUsers');
Route::get('/users/id/get', 'ApiController@getUserById');
Route::get('/users/username/get', 'ApiController@getUserByUsername');
Route::post('/users/add', 'ApiController@addNewUser');
Route::post('/users/password/reset', 'ApiController@resetPassword');
Route::post('/users/edit', 'ApiController@editUser');
Route::post('/users/record/payment', 'ApiController@recordPayment');
