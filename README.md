# QubeTrade Trading System RESTful API

This is a readme on using the RESTful API for QubeTrade to add a new user.

## Endpoints

Available API endpoints:

**Add User** - qubetrade.com/api/users/add => HTTP POST Request

The following are displayed as a key => value type

*  firstName (required) => String

*  lastName (required) => String

*  email (required) => String

*  password (required) => String (plain text)

*  sponsor (required) => String (sponsor's USERNAME)

*  phoneNum (required) => String (sponsor's USERNAME)

*  username (required) => String (user's Username (will be used to log in))

*  apiKey (required) => String

*  apiSecret (required) => String

**Reset Password** - qubetrade.com/api/users/password/reset => HTTP POST Request

The following are displayed as a key => value type

*  email (required) => String

*  passwordPlainText (required) => String

*  apiKey (required) => String

*  apiSecret (required) => String

## Keys (Required for each call)

*  apiKey (required) => EfaXs6xDuAes3Cr6uxPHujEA

*  apiSecret (required) => UQ$!w*K+unPm^x2aLQ@SLA%a
